import axios from 'axios';
import swal from 'sweetalert';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_USERS           = 'LIST_USERS';
const VIEW_USER            = 'VIEW_USER';

export function AC_LIST_USERS() {
  return function(dispatch) {
    return axios.get( URL.API.ListUsers,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_USERS, payload:data.data});
        }
      })
  };
}

export function AC_VIEW_USER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewUser,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_USER, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_USER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteUser,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_USERS, payload:data.data});
        }
      })
  };
}
