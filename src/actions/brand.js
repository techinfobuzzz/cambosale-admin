import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_BRANDS           = 'LIST_BRANDS';

export function AC_LIST_BRANDS() {
  return function(dispatch) {
    return axios.get( URL.API.ListBrands,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_BRANDS, payload:data.data});
        }
      })
  };
}

export function AC_ADD_BRAND(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateBrand,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_DELETE_BRAND(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteBrand,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_BRANDS, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}
export function AC_VIEW_BRAND(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewBrand,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: "VIEW_BRAND", payload:data.data});
        }
      })
  };
}

export function AC_EDIT_BRAND(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateBrand,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: "VIEW_BRAND", payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: "HANDLE_CHANGE_BRAND",name:name,value:value})
  };
}
