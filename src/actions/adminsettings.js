import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const VIEW_ADMINSETTINGS            = 'VIEW_ADMINSETTINGS';
const VIEW_SOCIALMEDIAS             = 'VIEW_SOCIALMEDIAS';
const HANDLE_INPUT_CHANGE           = 'HANDLE_INPUT_CHANGE';

export function AC_AUTHORIZATION(formData) {
  return function(dispatch) {
    return axios.post(URL.API.LogIn,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          localStorage.setItem("cambosaletoken",data.token);
          window.location="/"
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_FORGOT_PASSWORD(formData) {
  return function(dispatch) {
    return axios.post(URL.API.ForgotPassword,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          setTimeout(function(){ window.location='/'; },6000)
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_CHANGE_PASSWORD(formData) {
  return function(dispatch) {
    return axios.post(URL.API.ChangePassword,formData)
    .then(({ data }) => {
      if(data.status){
        Success(data.message)
        setTimeout(function(){ window.location='/'; },6000)
      } else {
        Error(data.message)
      }
    })
  }
}

export function AC_VIEW_ADMINSETTINGS(formData) {
  return function(dispatch) {
    return axios.post(URL.API.ViewAdminsettings,formData)
      .then(({ data }) => {
        if(data.status===403){
          localStorage.removeItem("cambosaletoken");
          window.location='/';
        } else if(data.status){
          dispatch({type: VIEW_ADMINSETTINGS, payload:data.data});
        }
    });
  };
}

export function AC_VIEW_SOCIALMEDIAS(formData) {
  return function(dispatch) {
    return axios.post(URL.API.ViewSocialMedia,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: VIEW_SOCIALMEDIAS, payload:data.data});
        }
    });
  };
}

export function AC_ADDEDIT_SOCIALMEDIA(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddEditSocialMedia,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message)
          dispatch({type: VIEW_SOCIALMEDIAS, payload:data.data});
        }
    });
  };
}

export function AC_DELETE_SOCIALMEDIA(formData) {
  return function(dispatch) {
    return axios.post(URL.API.DeleteSocialMedia,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message)
          dispatch({type: VIEW_SOCIALMEDIAS, payload:data.data});
        }
    });
  };
}
export function AC_UPDATE_ADMINSETTINGS(formData) {
  return function(dispatch) {
    return axios.post(URL.API.UpdateAdminSettings,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message)
          dispatch({type: VIEW_ADMINSETTINGS, payload:data.data});
        }
    });
  };
}

export function AC_HANDLE_INPUT_CHANGE(name,value,module) {
  return function(dispatch) {
    dispatch({type: HANDLE_INPUT_CHANGE,name:name,value:value,module:module})
  };
}
