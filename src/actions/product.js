import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_PRODUCTS          = 'LIST_PRODUCTS';
const VIEW_PRODUCT           = 'VIEW_PRODUCT';
const HANDLE_CHANGE          = 'HANDLE_CHANGE';
const ONCHANGE_SUBCATEGORIES = 'ONCHANGE_SUBCATEGORIES';
const ONCHANGE_CATEGORIES    = 'ONCHANGE_CATEGORIES';
const HANDLE_SUBCATEGORIES   = 'HANDLE_SUBCATEGORIES';
const HANDLE_CATEGORIES      = 'HANDLE_CATEGORIES';

export function AC_ADD_PRODUCT(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateProduct,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_PRODUCTS() {
  return function(dispatch) {
    return axios.get( URL.API.ListProducts,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_PRODUCTS, payload:data.data});
        }
      })
  };
}

export function AC_ONCHANGE_SUBCATEGORIES(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategoriesList,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: ONCHANGE_SUBCATEGORIES, payload:data.data});
        } 
      })
  };
}
export function AC_ONCHANGE_CATEGORIES(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategoriesList,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: ONCHANGE_CATEGORIES, payload:data.data});
        } 
      })
  };
}
export function AC_HANDLE_SUBCATEGORIES(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategoriesList,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: HANDLE_SUBCATEGORIES, payload:data.data});
        } 
      })
  };
}

export function AC_HANDLE_CATEGORIES(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategoriesList,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: HANDLE_CATEGORIES, payload:data.data});
        } 
      })
  };
}
export function AC_DELETE_PRODUCT(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteProduct,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_PRODUCTS, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}
export function AC_VIEW_PRODUCT(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewProduct,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_PRODUCT, payload:data.data, subcategories:data.subcategories,categories:data.categories});
        }
      })
  };
}

export function AC_EDIT_PRODUCT(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateProduct,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  // dispatch({type: VIEW_PRODUCT, payload:data.data, subcategories:data.subcategories,categories:data.categories});
        }
      })
  };
}

export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
