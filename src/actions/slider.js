import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_SLIDERS           = 'LIST_SLIDERS';
const VIEW_SLIDER            = 'VIEW_SLIDER';
const HANDLE_CHANGE          = 'HANDLE_CHANGE';

export function AC_ADD_SLIDER(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateSlider,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_SLIDERS() {
  return function(dispatch) {
    return axios.get( URL.API.ListSliders,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_SLIDERS, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_SLIDER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteSlider,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_SLIDERS, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}
export function AC_VIEW_SLIDER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSlider,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_SLIDER, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_SLIDER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateSlider,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_SLIDER, payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
