import axios from 'axios';
// import swal from 'sweetalert';
import URL from '../common/api';
// import { Success , Loader ,Error} from '../common/swal';

const LIST_CURRENCIES          = 'LIST_CURRENCIES';
const VIEW_CURRENCY            = 'VIEW_CURRENCY';
const HANDLE_CHANGE            = 'HANDLE_CHANGE';

export function AC_ADD_CURRENCY(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateCurrency,formData)
      .then(({ data }) => {
        if(data.status){
          alert(data.message);
        } else {
          alert(data.message)
        }
    });
  };
}

export function AC_LIST_CURRENCIES() {
  return function(dispatch) {
    return axios.get( URL.API.ListCurrencies,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_CURRENCIES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_CURRENCY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteCurrency,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: LIST_CURRENCIES, payload:data.data});
        }
      })
  };
}
export function AC_VIEW_CURRENCY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewCurrency,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_CURRENCY, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_CURRENCY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateCurrency,formData)
      .then(({ data }) => {
        if(data.status) {
          alert(data.message);
      	  dispatch({type: VIEW_CURRENCY, payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
