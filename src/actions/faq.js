import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_FAQS           = 'LIST_FAQS';
const VIEW_FAQ            = 'VIEW_FAQ';
const HANDLE_CHANGE       = 'HANDLE_CHANGE';

export function AC_ADD_FAQ(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateFaq,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_FAQS() {
  return function(dispatch) {
    return axios.get( URL.API.ListFaqs,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_FAQS, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_FAQ(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteFaq,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_FAQS, payload:data.data});
        }
      })
  };
}
export function AC_VIEW_FAQ(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewFaq,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_FAQ, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_FAQ(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateFaq,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_FAQ, payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
