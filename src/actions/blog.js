import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_BLOGCATEGORIES           = 'LIST_BLOGCATEGORIES';
const LIST_BLOG                     = 'LIST_BLOG';

export function AC_LIST_BLOGCATEGORY() {
  return function(dispatch) {
    return axios.get( URL.API.ListBlogCategory,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_BLOGCATEGORIES, payload:data.data});
        }
      })
  };
}

export function AC_ADD_BLOGCATEGORY(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateBlogCategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_DELETE_BLOGCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteBlogCategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_BLOGCATEGORIES, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}
export function AC_VIEW_BLOGCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewBlogcategory,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: "VIEW_BLOGCATEGORY", payload:data.data});
        }
      })
  };
}

export function AC_EDIT_BLOGCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateBlogCategory,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: "VIEW_BLOGCATEGORY", payload:data.data});
        }
      })
  };
}


export function AC_LIST_BLOG() {
  return function(dispatch) {
    return axios.get( URL.API.listBlogs,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_BLOG, payload:data.data});
        }
      })
  };
}

export function AC_ADD_BLOG(formData) {
  return function(dispatch) {
    return axios.post(URL.API.addUpdateBlog,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_DELETE_BLOG(formData) {
  return function(dispatch) {
    return axios.post( URL.API.deleteBlog,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_BLOG, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}
export function AC_VIEW_BLOG(formData) {
  return function(dispatch) {
    return axios.post( URL.API.viewBlog,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: "VIEW_BLOG", payload:data.data});
        }
      })
  };
}

export function AC_EDIT_BLOG(formData) {
  return function(dispatch) {
    return axios.post( URL.API.addUpdateBlog,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: "VIEW_BLOG", payload:data.data});
        }
      })
  };
}

export function AC_HANDLE_CHANGE(name,value,module) {
  return function(dispatch) {
    dispatch({type: "HANDLE_CHANGE_BLOG",name:name,value:value,module:module})
  };
}
