import axios from 'axios';
import swal from 'sweetalert';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_SHOPS           = 'LIST_SHOPS';
const VIEW_SHOP            = 'VIEW_SHOP';
const HANDLE_CHANGE          = 'HANDLE_CHANGE';

export function AC_LIST_SHOPS() {
  return function(dispatch) {
    return axios.get( URL.API.ListShops,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_SHOPS, payload:data.data});
        }
      })
  };
}

export function AC_ADD_SHOP(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateShop,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_EDIT_SHOP(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateShop,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_SHOP, payload:data.data});
        }
      })
  };
}
export function AC_VIEW_SHOP(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewShop,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_SHOP, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_SHOP(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteShop,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_SHOPS, payload:data.data});
        }
      })
  };
}

export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}