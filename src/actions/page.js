import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_PAGES           = 'LIST_PAGES';
const VIEW_PAGE            = 'VIEW_PAGE';
const HANDLE_CHANGE        = 'HANDLE_CHANGE';

export function AC_ADD_PAGE(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdatePage,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_PAGES() {
  return function(dispatch) {
    return axios.get( URL.API.ListPages,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_PAGES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_PAGE(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeletePage,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_PAGES, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}
export function AC_VIEW_PAGE(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewPage,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_PAGE, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_PAGE(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdatePage,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_PAGE, payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
