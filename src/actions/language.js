import axios from 'axios';
import swal from 'sweetalert';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_LANGUAGES           = 'LIST_LANGUAGES';
const VIEW_LANGUAGE            = 'VIEW_LANGUAGE';
const HANDLE_CHANGE            = 'HANDLE_CHANGE';

export function AC_ADD_LANGUAGE(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateLanguage,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_LANGUAGES() {
  return function(dispatch) {
    return axios.get( URL.API.ListLanguages,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_LANGUAGES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_LANGUAGE(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteLanguage,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_LANGUAGES, payload:data.data});
        }
      })
  };
}
export function AC_VIEW_LANGUAGE(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewLanguage,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_LANGUAGE, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_LANGUAGE(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateLanguage,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_LANGUAGE, payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
