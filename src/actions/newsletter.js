import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_NEWSLETTERS           = 'LIST_NEWSLETTERS';
const VIEW_NEWSLETTER            = 'VIEW_NEWSLETTER';
const HANDLE_CHANGE              = 'HANDLE_CHANGE';

export function AC_ADD_NEWSLETTER(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateNewsletter,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_NEWSLETTERS() {
  return function(dispatch) {
    return axios.get( URL.API.ListNewsletters,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_NEWSLETTERS, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_NEWSLETTER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteNewsletter,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_NEWSLETTERS, payload:data.data});
        }
      })
  };
}
export function AC_VIEW_NEWSLETTER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewNewsletter,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_NEWSLETTER, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_NEWSLETTER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateNewsletter,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_NEWSLETTER, payload:data.data});
        }
      })
  };
}
export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
