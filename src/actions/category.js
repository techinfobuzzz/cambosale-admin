import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_MAINCATEGORIES         = 'LIST_MAINCATEGORIES';
const VIEW_MAINCATEGORY           = 'VIEW_MAINCATEGORY';
const LIST_SUBCATEGORIES          = 'LIST_SUBCATEGORIES';
const VIEW_SUBCATEGORY            = 'VIEW_SUBCATEGORY';
const LIST_CATEGORIES             = 'LIST_CATEGORIES';
const VIEW_CATEGORY               = 'VIEW_CATEGORY';
const HANDLE_CHANGE               = 'HANDLE_CHANGE';
const ONCHANGE_SUBCATEGORIESLIST  = 'ONCHANGE_SUBCATEGORIESLIST';
const HANDLE_SUBCATEGORIESLIST    = 'HANDLE_SUBCATEGORIESLIST';

export function AC_ADD_MAINCATEGORY(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateMaincategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_MAINCATEGORIES() {
  return function(dispatch) {
    return axios.get( URL.API.ListMaincategories,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_MAINCATEGORIES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_MAINCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteMaincategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_MAINCATEGORIES, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}

export function AC_VIEW_MAINCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewMaincategory,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_MAINCATEGORY, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_MAINCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateMaincategory,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_MAINCATEGORY, payload:data.data});
        }
      })
  };
}

export function AC_ADD_SUB_CATEGORY(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateSubcategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_SUBCATEGORIES() {
  return function(dispatch) {
    return axios.get( URL.API.ListSubcategories,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_SUBCATEGORIES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_SUBCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteSubcategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_SUBCATEGORIES, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}

export function AC_VIEW_SUBCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategory,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_SUBCATEGORY, payload:data.data});
        }
      })
  };
}

export function AC_EDIT_SUBCATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateSubcategory,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_SUBCATEGORY, payload:data.data});
        }
      })
  };
}

export function AC_ADD_CATEGORY(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateCategory,formData)
      .then(({ data }) => {
        if(data.status){
      	   dispatch({type: LIST_CATEGORIES, payload:data.data});
           Success(data.message);
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_ONCHANGE_SUBCATEGORIES(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategoriesList,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: ONCHANGE_SUBCATEGORIESLIST, payload:data.data});
        } 
      })
  };
}

export function AC_HANDLE_SUBCATEGORIES(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSubcategoriesList,formData)
      .then(({ data }) => {
        if(data.status){
          dispatch({type: HANDLE_SUBCATEGORIESLIST, payload:data.data});
        } 
      })
  };
}

export function AC_LIST_CATEGORIES() {
  return function(dispatch) {
    return axios.get( URL.API.ListCategories,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_CATEGORIES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_CATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteCategory,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          dispatch({type: LIST_CATEGORIES, payload:data.data});
        } else {
          Error(data.message)
        }
      })
  };
}

export function AC_VIEW_CATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewCategory,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_CATEGORY, payload:data.data, subcategories:data.subcategories});
        }
      })
  };
}

export function AC_EDIT_CATEGORY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateCategory,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_CATEGORY, payload:data.data, subcategories:data.subcategories});
        } else {
          Error(data.message);
        }
      })
  };
}

export function AC_HANDLE_CHANGE(name,value,module) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value,module:module})
  };
}
