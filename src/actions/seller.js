import axios from 'axios';
import swal from 'sweetalert';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_SELLERS           = 'LIST_SELLERS';
const VIEW_SELLER            = 'VIEW_SELLER';

export function AC_LIST_SELLERS() {
  return function(dispatch) {
    return axios.get( URL.API.ListSellers,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_SELLERS, payload:data.data});
        }
      })
  };
}

export function AC_VIEW_SELLER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewSeller,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_SELLER, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_SELLER(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteSeller,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_SELLERS, payload:data.data});
        }
      })
  };
}
