import axios from 'axios';
import URL from '../common/api';
import { Success , Loader ,Error} from '../common/alert';

const LIST_DISTRICTS           = 'LIST_DISTRICTS';
const VIEW_DISTRICT            = 'VIEW_DISTRICT';
const LIST_CITIES              = 'LIST_CITIES';
const HANDLE_CHANGE            = 'HANDLE_CHANGE';
const TOTALCITIES_LIST         = 'TOTALCITIES_LIST';

export function AC_ADD_DISTRICT(formData) {
  return function(dispatch) {
    return axios.post(URL.API.AddUpdateDistrict,formData)
      .then(({ data }) => {
        if(data.status){
          Success(data.message);
          window.location="/edit-district/"+data.id;
        } else {
          Error(data.message)
        }
    });
  };
}

export function AC_LIST_DISTRICTS() {
  return function(dispatch) {
    return axios.get( URL.API.ListDistricts,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: LIST_DISTRICTS, payload:data.data});
        }
      })
  };
}

export function AC_LIST_CITIES() {
  return function(dispatch) {
    return axios.get( URL.API.ListCities,{})
      .then(({ data }) => {
        if(data.status) {
      	   dispatch({type: TOTALCITIES_LIST, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_DISTRICT(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteDistrict,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_DISTRICTS, payload:data.data});
        }
      })
  };
}

export function AC_VIEW_DISTRICT(formData) {
  return function(dispatch) {
    return axios.post( URL.API.ViewDistrict,formData)
      .then(({ data }) => {
        if(data.status) {
      	  dispatch({type: VIEW_DISTRICT, payload:data.data,cities:data.cities});
        }
      })
  };
}

export function AC_EDIT_DISTRICT(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateDistrict,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: VIEW_DISTRICT, payload:data.data,cities:data.cities});
        }
      })
  };
}

export function AC_ADDEDIT_CITY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.AddUpdateCity,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_CITIES, payload:data.data});
        }
      })
  };
}

export function AC_DELETE_CITY(formData) {
  return function(dispatch) {
    return axios.post( URL.API.DeleteCity,formData)
      .then(({ data }) => {
        if(data.status) {
          Success(data.message);
      	  dispatch({type: LIST_CITIES, payload:data.data});
        }
      })
  };
}

export function AC_HANDLE_CHANGE(name,value) {
  return function(dispatch) {
    dispatch({type: HANDLE_CHANGE,name:name,value:value})
  };
}
