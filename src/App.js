import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import 'react-toastify/dist/ReactToastify.css';
import { AC_VIEW_ADMINSETTINGS } from './actions/adminsettings';

// Dashboard Component
import Dashboard from './components/dashboard/dashboard';

// Admin settings Component
import SiteSettings from './components/adminsettings/sitesettings';
import SocialmediaSettings from './components/adminsettings/socialmediasettings';

// Layout Component
import Header from './components/layouts/header';
import Footer from './components/layouts/footer';
import Sidebar from './components/layouts/sidebar';

// Authorization Component
import Login from './components/authorization/login';
import ChangePassword from './components/authorization/changepassword';
import Forgotpassword from './components/authorization/forgotpassword';

// Language Component
import AddLanguage from './components/language/addlanguage';
import EditLanguage from './components/language/editlanguage';
import ViewLanguage from './components/language/viewlanguage';
import LanguagesList from './components/language/languageslist';

// Currency Component
import AddCurrency from './components/currency/addcurrency';
import ViewCurrency from './components/currency/viewcurrency';
import EditCurrency from './components/currency/editcurrency';
import CurrenciesList from './components/currency/currencieslist';

// FAQ Component
import AddFaq from './components/faq/addfaq';
import ViewFaq from './components/faq/viewfaq';
import EditFaq from './components/faq/editfaq';
import FaqsList from './components/faq/faqslist';

// Page Component
import AddPage from './components/page/addpage';
import PagesList from './components/page/pageslist';
import ViewPage from './components/page/viewpage';
import EditPage from './components/page/editpage';

// Newsletter Component
import AddNewsletter from './components/newsletter/addnewsletter';
import NewslettersList from './components/newsletter/newsletterslist';
import ViewNewsletter from './components/newsletter/viewnewsletter';
import EditNewsletter from './components/newsletter/editnewsletter';

// Slider Component
import AddSlider from './components/slider/addslider';
import SlidersList from './components/slider/sliderslist';
import ViewSlider from './components/slider/viewslider';
import EditSlider from './components/slider/editslider';

// Shop Component
import ShopsList from './components/shop/shopslist';
import ViewShop from './components/shop/viewshop';
import AddShop from './components/shop/addshop';
import EditShop from "./components/shop/editshop";

// Users Component
import UsersList from './components/user/userslist';
import ViewUser from './components/user/viewuser';

// Seller Component
import SellersList from './components/seller/sellerslist';
import ViewSeller from './components/seller/viewseller';

// Category Component
import AddMainCategory from './components/category/addmaincategory';
import MainCategoriesList from './components/category/maincategorieslist';
import ViewMainCategory from './components/category/viewmaincategory';
import EditMainCategory from './components/category/editmaincategory';
import AddSubcategory from './components/category/addsubcategory';
import SubCategoriesList from './components/category/subcategorieslist';
import ViewSubCategory from './components/category/viewsubcategory';
import EditSubCategory from './components/category/editsubcategory';
import AddCategory from './components/category/addcategory';
import CategoriesList from "./components/category/categorieslist";
import ViewCategory from "./components/category/viewcategory";
import EditCategory from "./components/category/editcategory";

// Page Not found Component
import NotFound from './components/notfound/notfound';

// Product Component
import AddProduct from './components/product/addproduct';
import ProductsList from './components/product/productslist';
import ViewProduct from './components/product/viewproduct';
import EditProduct from './components/product/editproduct';

import AddDistrict from "./components/district/adddistrict";
import EditDistrict from "./components/district/editdistrict";
import DistrictsList from "./components/district/districtslist";
import ViewDistrict from "./components/district/viewdistrict";

import AddBrand from "./components/brand/addbrand"
import BrandsList from "./components/brand/brandlist";
import ViewBrand from "./components/brand/viewbrand";
import EditBrand from "./components/brand/editbrand";
import AddBlogCategory from "./components/blog/addblogcategory";
import BlogCategoriesList from "./components/blog/blogcategorieslist";
import ViewBlogCategory from "./components/blog/viewblogcategory";
import EditBlogCategory from "./components/blog/editblogcategory";
import AddBlog from "./components/blog/addblog";
import BlogsList from "./components/blog/blogslist";
import ViewBlog from "./components/blog/viewblog";
import EditBlog from "./components/blog/editblog";

let token = localStorage.getItem("cambosaletoken");

const App = () =>{
  const dispatch = useDispatch()
  useEffect(()=>{
    if(token){
      dispatch(AC_VIEW_ADMINSETTINGS({token:token}));
    }
  },[])
  if(!token) {
    return (
      <Router>
          <Route exact path="/" component={Login}/>
          <Route exact path="/forgot-password" component={Forgotpassword}/>
          <Route exact path="/change-password" component={ChangePassword}/>
          <ToastContainer />
      </Router>
    );
  } else {
    return (
      <div class="wrapper">
        <Router>
            <Header/>
            <Sidebar/>
            <Switch>
              <Route exact path="/" component={Dashboard}/>
              <Route exact path="/site-settings" component={SiteSettings}/>
              <Route exact path="/social-media" component={SocialmediaSettings}/>

              <Route exact path="/add-language" component={AddLanguage}/>
              <Route exact path="/edit-language/:id" component={EditLanguage}/>
              <Route exact path="/view-language/:id" component={ViewLanguage}/>
              <Route exact path="/languages-list" component={LanguagesList}/>

              <Route exact path="/add-currency" component={AddCurrency}/>
              <Route exact path="/currencies-list" component={CurrenciesList}/>
              <Route exact path="/view-currency/:id" component={ViewCurrency}/>
              <Route exact path="/edit-currency/:id" component={EditCurrency}/>

              <Route exact path="/add-faq" component={AddFaq}/>
              <Route exact path="/faqs-list" component={FaqsList}/>
              <Route exact path="/view-faq/:id" component={ViewFaq}/>
              <Route exact path="/edit-faq/:id" component={EditFaq}/>

              <Route exact path="/add-page" component={AddPage}/>
              <Route exact path="/pages-list" component={PagesList}/>
              <Route exact path="/view-page/:id" component={ViewPage}/>
              <Route exact path="/edit-page/:id" component={EditPage}/>

              <Route exact path="/add-newsletter" component={AddNewsletter}/>
              <Route exact path="/newsletters-list" component={NewslettersList}/>
              <Route exact path="/view-newsletter/:id" component={ViewNewsletter}/>
              <Route exact path="/edit-newsletter/:id" component={EditNewsletter}/>

              <Route exact path="/add-slider" component={AddSlider}/>
              <Route exact path="/sliders-list" component={SlidersList}/>
              <Route exact path="/view-slider/:id" component={ViewSlider}/>
              <Route exact path="/edit-slider/:id" component={EditSlider}/>

              <Route exact path="/shops-list" component={ShopsList}/>
              <Route exact path="/view-shop/:id" component={ViewShop}/>
              <Route exact path="/edit-shop/:id" component={EditShop}/>
              <Route exact path="/add-shop" component={AddShop}/>

              <Route exact path="/users-list" component={UsersList}/>
              <Route exact path="/view-user/:id" component={ViewUser}/>

              <Route exact path="/sellers-list" component={SellersList}/>
              <Route exact path="/view-seller/:id" component={ViewSeller}/>

              <Route exact path="/add-product" component={AddProduct}/>
              <Route exact path="/products-list" component={ProductsList}/>
              <Route exact path="/view-product/:id" component={ViewProduct}/>
              <Route exact path="/edit-product/:id" component={EditProduct}/>

              <Route exact path="/add-category" component={AddCategory}/>
              <Route exact path="/categories-list" component={CategoriesList}/>
              <Route exact path="/view-category/:id" component={ViewCategory}/>
              <Route exact path="/edit-category/:id" component={EditCategory}/>
              <Route exact path="/add-maincategory" component={AddMainCategory}/>
              <Route exact path="/maincategories-list" component={MainCategoriesList}/>
              <Route exact path="/view-maincategory/:id" component={ViewMainCategory}/>
              <Route exact path="/edit-maincategory/:id" component={EditMainCategory}/>
              <Route exact path="/add-subcategory" component={AddSubcategory}/>
              <Route exact path="/subcategories-list" component={SubCategoriesList}/>
              <Route exact path="/view-subcategory/:id" component={ViewSubCategory}/>
              <Route exact path="/edit-subcategory/:id" component={EditSubCategory}/>

              <Route exact path="/add-district" component={AddDistrict}/>
              <Route exact path="/districts-list" component={DistrictsList}/>
              <Route exact path="/edit-district/:id" component={EditDistrict}/>
              <Route exact path="/view-district/:id" component={ViewDistrict}/>

              <Route exact path="/add-brand" component={AddBrand}/>
              <Route exact path="/brands-list" component={BrandsList}/>
              <Route exact path="/view-brand/:id" component={ViewBrand}/>
              <Route exact path="/edit-brand/:id" component={EditBrand}/>

              <Route exact path="/add-blog-category" component={AddBlogCategory}/>
              <Route exact path="/blog-category-list" component={BlogCategoriesList}/>
              <Route exact path="/view-blog-category/:id" component={ViewBlogCategory}/>
              <Route exact path="/edit-blog-category/:id" component={EditBlogCategory}/>

              <Route exact path="/add-blog" component={AddBlog}/>
              <Route exact path="/blogs-list" component={BlogsList}/>
              <Route exact path="/view-blog/:id" component={ViewBlog}/>
              <Route exact path="/edit-blog/:id" component={EditBlog}/>
              <NotFound />
            </Switch>
            <Footer/>
            <ToastContainer />
        </Router>
      </div>
    );
  }
}

export default App;
