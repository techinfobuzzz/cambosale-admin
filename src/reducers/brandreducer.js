const initialState = {
    brandsList          : [],
    viewBrand           : {},
  }
  
  function BrandReducer(state = initialState, action) {
    switch (action.type) {
      case "LIST_BRANDS":
          return Object.assign({}, state, {
            brandsList     : action.payload,
          })
      case "VIEW_BRAND":
        return Object.assign({}, state, {
          viewBrand     : action.payload,
        })
      case "HANDLE_CHANGE_BRAND":
        return {
          ...state,
          viewBrand : {
              ...state.viewBrand,
              [action.name] : action.value
          }
        }
      default:
        return state
    }
  }
  
  export default BrandReducer;
  