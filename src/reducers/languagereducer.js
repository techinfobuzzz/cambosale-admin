const initialState = {
  languagesList          : [],
  viewLanguage           : {},
}

function LanguageReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_LANGUAGES":
        return Object.assign({}, state, {
          languagesList     : action.payload,
        })
    case "VIEW_LANGUAGE":
        return Object.assign({}, state, {
          viewLanguage     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewLanguage : {
              ...state.viewLanguage,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default LanguageReducer;
