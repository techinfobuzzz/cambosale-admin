const initialState = {
    blogcategoriesList  : [],
    blogsList           : [],
    viewBlogcategory    : {},
    viewBlog            : {},
  }
  
  function BlogReducer(state = initialState, action) {
    switch (action.type) {
      case "LIST_BLOGCATEGORIES":
          return Object.assign({}, state, {
            blogcategoriesList     : action.payload,
          })
      case "VIEW_BLOGCATEGORY":
        return Object.assign({}, state, {
          viewBlogcategory     : action.payload,
        })
      case "LIST_BLOG":
          return Object.assign({}, state, {
            blogsList     : action.payload,
          })
      case "VIEW_BLOG":
        return Object.assign({}, state, {
          viewBlog     : action.payload,
        })
      case "HANDLE_CHANGE_BLOG":
        if(action.module=="Category"){
          return {
            ...state,
            viewBlogcategory : {
                ...state.viewBlogcategory,
                [action.name] : action.value
            }
          }
        } else if(action.module=="Blog"){
          return {
            ...state,
            viewBlog : {
                ...state.viewBlog,
                [action.name] : action.value
            }
          }
        }
       
      default:
        return state
    }
  }
  
  export default BlogReducer;
  