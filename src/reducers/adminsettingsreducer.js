const initialState = {
  userId             : "",
  viewSettings       : {},
  viewSitesettings   : {},
  viewSocialmedia    : [],
}

function AdminsettingsReducer(state = initialState, action) {
  switch (action.type) {
    case "VIEW_ADMINSETTINGS":
        return Object.assign({}, state, {
          viewSitesettings     : action.payload.site,
          viewSettings         : action.payload.site,
          userId               : action.payload._id,
        })
    case "VIEW_SOCIALMEDIAS":
      return Object.assign({}, state, {
        viewSocialmedia      : action.payload,
      })
    case "HANDLE_INPUT_CHANGE":
      if(action.module=="SiteSettings") {
        return {
            ...state,
            viewSitesettings : {
                ...state.viewSitesettings,
                [action.name] : action.value
            }
          }
      }
      if(action.module=="Socialmedia") {
        return {
            ...state,
            viewSocialmedia : {
                ...state.viewSocialmedia,
                [action.name] : action.value
            }
          }
      }
    default:
      return state
  }
}

export default AdminsettingsReducer;
