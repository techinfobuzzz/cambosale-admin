const initialState = {
  slidersList          : [],
  viewSlider           : {},
}

function SliderReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_SLIDERS":
        return Object.assign({}, state, {
          slidersList     : action.payload,
        })
    case "VIEW_SLIDER":
        return Object.assign({}, state, {
          viewSlider     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewSlider : {
              ...state.viewSlider,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default SliderReducer;
