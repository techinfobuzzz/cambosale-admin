const initialState = {
  currencyList          : [],
  viewCurrency          : {},
}

function CurrencyReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_CURRENCIES":
        return Object.assign({}, state, {
          currencyList     : action.payload,
        })
    case "VIEW_CURRENCY":
        return Object.assign({}, state, {
          viewCurrency     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewCurrency : {
              ...state.viewCurrency,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default CurrencyReducer;
