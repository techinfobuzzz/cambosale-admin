const initialState = {
  newslettersList          : [],
  viewNewsletter           : {},
}

function NewsletterReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_NEWSLETTERS":
        return Object.assign({}, state, {
          newslettersList     : action.payload,
        })
    case "VIEW_NEWSLETTER":
        return Object.assign({}, state, {
          viewNewsletter     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewNewsletter : {
              ...state.viewNewsletter,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default NewsletterReducer;
