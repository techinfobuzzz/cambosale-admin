const initialState = {
  faqsList          : [],
  viewFaq           : {},
}

function FaqReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_FAQS":
        return Object.assign({}, state, {
          faqsList     : action.payload,
        })
    case "VIEW_FAQ":
        return Object.assign({}, state, {
          viewFaq     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewFaq : {
              ...state.viewFaq,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default FaqReducer;
