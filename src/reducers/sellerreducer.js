const initialState = {
  sellersList          : [],
  viewSeller           : {},
}

function SellerReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_SELLERS":
        return Object.assign({}, state, {
          sellersList     : action.payload,
        })
    case "VIEW_SELLER":
        return Object.assign({}, state, {
          viewSeller     : action.payload,
        })
    default:
      return state
  }
}

export default SellerReducer;
