const initialState = {
  shopsList          : [],
  viewShop           : {},
}

function ShopReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_SHOPS":
        return Object.assign({}, state, {
          shopsList     : action.payload,
        })
    case "VIEW_SHOP":
        return Object.assign({}, state, {
          viewShop     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewShop : {
              ...state.viewShop,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default ShopReducer;
