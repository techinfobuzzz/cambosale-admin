const initialState = {
  productsList          : [],
  viewProduct           : {},
  subCategories         : {},
  categories            : {},
  viewsubCategories     : {},
  viewCategories        : {},
}

function ProductReducer(state = initialState, action) {
  switch (action.type) {
    case "ONCHANGE_SUBCATEGORIES":
      return Object.assign({}, state, {
        subCategories         : action.payload,
      })
    case "ONCHANGE_CATEGORIES":
    return Object.assign({}, state, {
      categories         : action.payload,
    })
    case "HANDLE_SUBCATEGORIES":
      return Object.assign({}, state, {
        viewsubCategories     : action.payload,
      })
    case "HANDLE_CATEGORIES":
    return Object.assign({}, state, {
      viewCategories     : action.payload,
    })
    case "LIST_PRODUCTS":
      return Object.assign({}, state, {
        productsList     : action.payload,
    })
    case "VIEW_PRODUCT":
      return Object.assign({}, state, {
        viewProduct         : action.payload,
        viewsubCategories   : action.subcategories,
        viewCategories      : action.categories,
    })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewProduct : {
              ...state.viewProduct,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default ProductReducer;
