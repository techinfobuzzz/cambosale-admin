const initialState = {
  pagesList          : [],
  viewPage           : {},
}

function PageReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_PAGES":
        return Object.assign({}, state, {
          pagesList     : action.payload,
        })
    case "VIEW_PAGE":
        return Object.assign({}, state, {
          viewPage     : action.payload,
        })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewPage : {
              ...state.viewPage,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default PageReducer;
