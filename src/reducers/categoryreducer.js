const initialState = {
  maincategoriesList      : [],
  viewmainCategory        : {},
  subcategoriesList       : [],
  viewSubcategory         : {},
  viewsubcategoriesList   : [],
  subcategories           : [],
  categoriesList          : [],
  viewCategory            : {},
}

function CategoryReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_MAINCATEGORIES":
        return Object.assign({}, state, {
          maincategoriesList     : action.payload,
        })
    case "VIEW_MAINCATEGORY":
        return Object.assign({}, state, {
          viewmainCategory     : action.payload,
        })
    case "LIST_SUBCATEGORIES":
        return Object.assign({}, state, {
          subcategoriesList     : action.payload,
        })
    case "VIEW_SUBCATEGORY":
        return Object.assign({}, state, {
          viewSubcategory     : action.payload,
        })
    case "LIST_CATEGORIES":
      return Object.assign({}, state, {
        categoriesList     : action.payload,
      })
    case "VIEW_CATEGORY":
      return Object.assign({}, state, {
        viewCategory     : action.payload,
        subcategories    : action.subcategories
      })
    case "ONCHANGE_SUBCATEGORIESLIST":
      return Object.assign({}, state, {
        viewsubcategoriesList     : action.payload,
    })
    case "HANDLE_SUBCATEGORIESLIST":
      return Object.assign({}, state, {
        subcategories     : action.payload,
    })
    case "HANDLE_CHANGE":
     if(action.module=="MainCategory") {
        return {
            ...state,
            viewmainCategory : {
                ...state.viewmainCategory,
                [action.name] : action.value
            }
          }
      }
    if(action.module=="SubCategory") {
       return {
           ...state,
           viewSubcategory : {
               ...state.viewSubcategory,
               [action.name] : action.value
           }
         }
     }
    if(action.module=="Category") {
      return {
          ...state,
          viewCategory : {
              ...state.viewCategory,
              [action.name] : action.value
          }
        }
    }
    default:
      return state
  }
}

export default CategoryReducer;
