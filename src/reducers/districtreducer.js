const initialState = {
  districtsList          : [],
  viewDistrict           : {},
  citiesList             : [],
  totalcitiesList        : [],
}

function DistrictReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_DISTRICTS":
        return Object.assign({}, state, {
          districtsList     : action.payload,
        })
    case "VIEW_DISTRICT":
        return Object.assign({}, state, {
          viewDistrict     : action.payload,
          citiesList       : action.cities,
        })
    case "LIST_CITIES":
      return Object.assign({}, state, {
        citiesList     : action.payload,
      })
    case "TOTALCITIES_LIST":
      return Object.assign({}, state, {
        totalcitiesList     : action.payload,
      })
    case "HANDLE_CHANGE":
      return {
          ...state,
          viewDistrict : {
              ...state.viewDistrict,
              [action.name] : action.value
          }
        }
    default:
      return state
  }
}

export default DistrictReducer;
