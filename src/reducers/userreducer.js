const initialState = {
  usersList          : [],
  viewUser           : {},
}

function UserReducer(state = initialState, action) {
  switch (action.type) {
    case "LIST_USERS":
        return Object.assign({}, state, {
          usersList     : action.payload,
        })
    case "VIEW_USER":
        return Object.assign({}, state, {
          viewUser     : action.payload,
        })
    default:
      return state
  }
}

export default UserReducer;
