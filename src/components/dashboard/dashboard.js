import { useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux';
import { Link } from "react-router-dom";
import { AC_LIST_PRODUCTS } from '../../actions/product';
import { AC_LIST_SHOPS } from '../../actions/shop';
import { AC_LIST_SELLERS } from '../../actions/seller';
import { AC_LIST_USERS } from '../../actions/user';
import swal from 'sweetalert';

const Dashboard = e => {
    const dispatch = useDispatch()
    useEffect(()=>{
      dispatch(AC_LIST_PRODUCTS());
      dispatch(AC_LIST_SHOPS());
      dispatch(AC_LIST_SELLERS());
      dispatch(AC_LIST_USERS());
    },[]);
    const product = useSelector(state => state.ProductReducer)
    const productsList = product.productsList;

    const shop = useSelector(state => state.ShopReducer)
    const shopsList = shop.shopsList;

    const seller = useSelector(state => state.SellerReducer)
    const sellersList = seller.sellersList;

    const user = useSelector(state => state.UserReducer)
    const usersList = user.usersList;

    const logout = () =>{
      swal({
           title: "Logout",
           text: "Are you sure want to logout your account?",
           // icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             localStorage.removeItem("cambosaletoken");
             window.location="/";
           }
     });

    }
    return(
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">Dashboard</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active" onClick={logout}><a href="#">Logout</a></li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-3 col-6">
                <div className="small-box bg-info">
                  <div className="inner">
                    <h3>{productsList.length}</h3>
                    <p>Total Products</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-stats-bars"></i>
                  </div>
                  <Link to="/products-list" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-success">
                  <div className="inner">
                    <h3>{shopsList.length}</h3>
                    <p>Shops</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-bag"></i>
                  </div>
                  <Link to="/shops-list" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-warning">
                  <div className="inner">
                    <h3>{usersList.length}</h3>
                    <p>Users</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-person-add"></i>
                  </div>
                  <Link to="/users-list" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-danger">
                  <div className="inner">
                    <h3>{sellersList.length}</h3>
                    <p>Sellers</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-pie-graph"></i>
                  </div>
                  <Link to="/sellers-list" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

 export default Dashboard;
