import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import API from '../../common/api';

const Sidebar = () => {
    const adminsettings = useSelector(state => state.AdminsettingsReducer)
    const viewSitesettings = adminsettings.viewSettings;
    var image      = API.LOCALURL+"dist/img/user2-160x160.jpg";
    var logo       = API.LOCALURL+"dist/img/AdminLTELogo.png";
    var firstname  = "Gojo";
    var lastname   = "Sataru";
    var sitename   = "Cambosale";
    if(viewSitesettings) {
      if(viewSitesettings.sitename){
        sitename      = viewSitesettings.sitename
      }
      if(viewSitesettings.logo){
        logo       = API.LIVEURL+"uploads/"+viewSitesettings.logo
      }
    }
    return (
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        <a href="#" className="brand-link">
          <img src={logo} alt="" className="brand-image img-circle elevation-3"
               />
          <span className="brand-text font-weight-light">{sitename}</span>
        </a>
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img src={image} className="img-circle elevation-2" alt=""/>
            </div>
            <div className="info">
              <a href="#" className="d-block">{firstname} {lastname}</a>
            </div>
          </div>
          <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li className="nav-item has-treeview menu-open">
                <Link to="/" className="nav-link active">
                  <i className="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Dashboard
                  </p>
                </Link>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-cog"></i>
                  <p>
                    Admin Settings
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/site-settings" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Site settings</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/social-media" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Social Media</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-chart-pie"></i>
                  <p>
                    Category
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-maincategory" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Main Category</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/maincategories-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Main Categories List</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/add-subcategory" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Sub Category</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/subcategories-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Sub Categories List</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/add-category" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Category</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/categories-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Categories List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-shopping-cart"></i>
                  <p>
                    Shop
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-shop" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Shop</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/shops-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Shops List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon ion ion-bag"></i>
                  <p>
                    Product
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-product" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Product</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/products-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Products List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-columns"></i>
                  <p>
                    Districts
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-district" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add District</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/districts-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Districts List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon far fa-plus-square"></i>
                  <p>
                    Seller
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/sellers-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Sellers List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-columns"></i>
                  <p>
                    Blog
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-blog-category" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Blog Category</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/blog-category-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Blog Categories List</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/add-blog" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Blog </p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/blogs-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Blogs List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-columns"></i>
                  <p>
                    Brand
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-brand" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Brand</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/brands-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Brands List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon ion ion-person-add"></i>
                  <p>
                    User
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/users-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Users List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-th"></i>
                  <p>
                    Slider
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-slider" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Slider</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/sliders-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Sliders List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-copy"></i>
                  <p>
                    FAQ
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-faq" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add FAQ</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/faqs-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>FAQs List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-book"></i>
                  <p>
                    Page
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-page" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Page</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/pages-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Pages List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-columns"></i>
                  <p>
                    Newsletter
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-newsletter" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Newsletter</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/newsletters-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Newsletters List</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item has-treeview">
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-tree"></i>
                  <p>
                    Language
                    <i className="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/add-language" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Add Language</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/languages-list" className="nav-link">
                      <i className="far fa-circle nav-icon"></i>
                      <p>Languages List</p>
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </aside>
    )
}

 export default Sidebar;
