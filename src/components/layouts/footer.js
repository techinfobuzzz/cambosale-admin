import React from 'react';

const Footer = e => {
    return (
      <footer className="main-footer">
        <strong>Copyright &copy; 2020 <a href="http://adminlte.io">Brammatech.io</a>.</strong>
        All rights reserved.
        <div className="float-right d-none d-sm-inline-block">
          <b>Version</b> 1.0.0
        </div>
      </footer>
    )
}
 export default Footer;
