import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_BRAND,AC_HANDLE_CHANGE,AC_EDIT_BRAND} from '../../actions/brand';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const EditBrand = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_BRAND({id:id}));
  },[])

  const [values, setValues] = useState({
    nameError           : false,
    imgSrc              : '',
    imageError          : false,
    imagecheck          : false,
  });

  const BrandReducer = useSelector(state => state.BrandReducer)
  const viewBrand = BrandReducer.viewBrand;
  var image         = API.LOCALURL+"dist/img/image.png"
  if(viewBrand) {
    var name          = viewBrand.name
    var status        = viewBrand.status
    if(viewBrand.image){
      image         = API.LIVEURL+"uploads/"+viewBrand.image
    }
  }
  const validateForm = () => {
    const data          = BrandReducer.viewBrand;
    const stateData     = {name:false,image:false};
    const id            = props.match.params.id;
    if(!data.name){
       stateData.name = true;
     }
     setValues({...values,nameError:stateData.name,imageError:stateData.image});
     if(data.name&&!values.imagecheck){
       const formData = new FormData();
       formData.append('id', id);
       formData.append('name', data.name);
       formData.append('status', data.status);
       formData.append('image', data.image);
       dispatch(AC_EDIT_BRAND(formData));
   }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     if(name=="image"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file));
             setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false});
         }.bind(this);
         } else {
           setValues({...values,imgSrc: "",imagecheck:true,image:file,imageError:false});
         }
       }
     } else {
       dispatch(AC_HANDLE_CHANGE(name,value));
       setValues({...values,[error]:false});
     }
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Brand</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Brand</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Brand</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={name}onChange={onChangeValue}placeholder="Enter Brand name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Image</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                    {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.imgSrc?<img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={image} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditBrand;
