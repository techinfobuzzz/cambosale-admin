import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_LANGUAGE,AC_HANDLE_CHANGE,AC_EDIT_LANGUAGE} from '../../actions/language';

const EditLanguage = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_LANGUAGE({id:id}));
  },[])

  const [values, setValues] = useState({
    nameError    : false,
    codeError    : false,
  });

  const language = useSelector(state => state.LanguageReducer)
  const viewLanguage = language.viewLanguage;
  if(viewLanguage) {
    var name   = viewLanguage.name
    var code   = viewLanguage.code
    var status = viewLanguage.status
  }

  const validateForm = () => {
    const data      = language.viewLanguage;
    const stateData = {name:false,code:false};
    const id = props.match.params.id;
    if(!data.name){
       stateData.name = true;
     }
    if(!data.code){
       stateData.code = true;
     }
     setValues({...values,nameError:stateData.name,codeError:stateData.code});
     if(data.name&&data.code){
        var formData = {
          id      : id,
          name    : data.name,
          code    : data.code,
          status  : data.status,
        }
        dispatch(AC_EDIT_LANGUAGE(formData));
     }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     dispatch(AC_HANDLE_CHANGE(name,value));
     setValues({...values,[error]:false});
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Language</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Language</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Language</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Language Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={name}onChange={onChangeValue}placeholder="Enter Language name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Language Code</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="code" value={code} onChange={onChangeValue} placeholder="Enter Language Code"/>
                    {values.symbolError ? <label style={{color:"red"}}>Code is required</label> : ""}
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditLanguage;
