import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_HANDLE_INPUT_CHANGE,AC_UPDATE_ADMINSETTINGS} from '../../actions/adminsettings';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const SiteSettings = (props) => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    favSrc              : '',
    logoSrc             : '',
    favcheck            : false,
    logocheck           : false,
  });

  const adminsettings = useSelector(state => state.AdminsettingsReducer)
  const viewSitesettings = adminsettings.viewSitesettings;
  var favicon       = "dist/img/image.png"
  var logo          = "dist/img/image.png"

  if(viewSitesettings) {
    var sitename      = viewSitesettings.sitename
    var copyrights    = viewSitesettings.copyrights
    if(viewSitesettings.favicon){
      favicon       = API.LIVEURL+"uploads/"+viewSitesettings.favicon
    }
    if(viewSitesettings.logo){
      logo       = API.LIVEURL+"uploads/"+viewSitesettings.logo
    }
  }
  const validateForm = () => {
    const stateData     = {name:false,description:false,image:false};
    const id            = adminsettings.userId;
     if(!values.favcheck&&!values.logocheck){
       const formData = new FormData();
       formData.append("id", id);
       formData.append("module", "Site");
       for (var i in viewSitesettings){
         formData.append(i, viewSitesettings[i]);
       }
       dispatch(AC_UPDATE_ADMINSETTINGS(formData));
   }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     if(name=="favicon"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_INPUT_CHANGE(name,file,"SiteSettings"));
             setValues({...values,favSrc: [reader.result],favcheck:false});
         }.bind(this);
         } else {
           setValues({...values,favSrc: "dist/img/image.png",favcheck:true,});
         }
       }
     } else if(name=="logo") {
         var file            =  e.target.files[0];
         if(file){
           const imgvalidate   =  Imagevalidation(file);
           if(imgvalidate) {
             var reader          = new FileReader();
             var url             = reader.readAsDataURL(file);
             reader.onloadend    = function(e){
               dispatch(AC_HANDLE_INPUT_CHANGE(name,file,"SiteSettings"));
               setValues({...values,logoSrc: [reader.result],logocheck:false});
           }.bind(this);
           } else {
             setValues({...values,logoSrc: "dist/img/image.png",logocheck:true});
           }
         }
     } else {
       dispatch(AC_HANDLE_INPUT_CHANGE(name,value,"SiteSettings"));
     }
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Site Settings</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Site Settings</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Site Settings</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Site Name</label>
                    <input type="text" className="form-control" name="sitename" value={sitename}onChange={onChangeValue}placeholder="Enter Site name"/>
                  </div>
                  <div className="form-group">
                    <label>Logo</label>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="logo" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.logocheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.logoSrc?<img src={values.logoSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={logo} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Copyrights</label>
                    <input type="text" className="form-control" name="copyrights" value={copyrights} onChange={onChangeValue}placeholder="Enter copyrights"/>
                  </div>
                  <div className="form-group">
                    <label>Favicon</label>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="favicon" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.favcheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.favSrc?<img src={values.favSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={favicon} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default SiteSettings;
