import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import swal from 'sweetalert';
import { AC_ADDEDIT_SOCIALMEDIA, AC_VIEW_SOCIALMEDIAS,AC_DELETE_SOCIALMEDIA} from '../../actions/adminsettings';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const SocialmediaSettings = (props) => {
  const dispatch = useDispatch()
 
  const [values, setValues] = useState({
    iconname            : '',
    iconlink            : '',
    icon                : '',
    iconSrc             : 'dist/img/image.png',
    iconcheck           : false,
    iconnameError       : false,
    iconlinkError       : false,
    iconError           : false,
    selectedId          : "0",
  });

  const adminsettings = useSelector(state => state.AdminsettingsReducer)
  const viewSocialmedia = adminsettings.viewSocialmedia;

  useEffect(()=>{
    if(adminsettings.userId){
      dispatch(AC_VIEW_SOCIALMEDIAS({adminId:adminsettings.userId}));
    }
  },[adminsettings.userId])

  const validateForm = () => {
    const adminId = adminsettings.userId
    const data = {iconname:false,iconlink:false,icon:false};
    var check = 1;
    if(!values.iconname){
      data.iconname = true;
      check = 0;
    }
    if(!values.icon){
      data.icon = true;
      check = 0;
    }
    if(!values.iconlink){
      data.iconlink = true;
      check = 0;
    }
    setValues({...values,iconError:data.icon,iconlinkError:data.iconlink,iconnameError:data.iconname});
    if(check&&!values.iconcheck){
    const formData = new FormData();
    formData.append('id', values.selectedId);
    formData.append('iconname', values.iconname);
    formData.append('iconlink', values.iconlink);
    formData.append('icon', values.icon);
    formData.append('adminId', adminId);
    dispatch(AC_ADDEDIT_SOCIALMEDIA(formData));
    setValues({...values,icon:"",iconlink:"",iconname:"",iconSrc:"dist/img/image.png"});
   }
  }
  const setValue= (e)=>{
    const id              = e.target.id;
    const selecteddetail = viewSocialmedia.filter((details)=>{
      return  details._id == id;
    });
    if(selecteddetail[0]){
          setValues({...values,iconname:selecteddetail[0].iconname,iconlink:selecteddetail[0].iconlink,icon:selecteddetail[0].icon,adminId:selecteddetail[0].adminId,
            selectedId:selecteddetail[0]._id,iconError:false,iconlinkError:false,iconnameError:false,iconcheck:false,iconSrc:API.LIVEURL+"uploads/"+selecteddetail[0].icon});
    }
  }

  const deleteDetails= (e)=>{
    const id              = e.target.id;
    const adminId         = adminsettings.userId;
    swal({
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            dispatch(AC_DELETE_SOCIALMEDIA({id:id,adminId:adminId}));
          }
    });
  }
  
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    if(name=="icon"){
      var file            =  e.target.files[0];
      if(file){
        const imgvalidate   =  Imagevalidation(file);
        if(imgvalidate) {
          var reader          = new FileReader();
          var url             = reader.readAsDataURL(file);
          reader.onloadend    = function(e){
            setValues({...values,iconSrc: [reader.result],iconcheck:false,iconError:false,icon:file});
        }.bind(this);
        } else {
          setValues({...values,iconSrc: "dist/img/image.png",iconcheck:true,icon:file,iconError:false});
        }
      }
    } else {
      setValues({...values,[name]: value,[error]:false});
    }
  };
  var socialmedia = [];
  for(var i=0;i<viewSocialmedia.length;i++){
    socialmedia.push(
      <tr key={i}>
        <td>
        {viewSocialmedia[i].iconname}
        </td >
        <td>
        {viewSocialmedia[i].iconlink}
        </td >
        <td>
        <img src={API.LIVEURL+"uploads/"+viewSocialmedia[i].icon} alt=""/>
        </td >
        <td style={{width:"13%"}}>
          <div className="btn btn-primary" id={viewSocialmedia[i]._id} style={{marginRight:"5px"}} onClick={setValue} ><i className="fas fa-edit" delete='delete' id={viewSocialmedia[i]._id}></i></div>
          <div id={viewSocialmedia[i]._id} className="btn btn-danger" style={{marginRight:"-120px"}} onClick={deleteDetails} ><i className="fas fa-trash" delete='delete' id={viewSocialmedia[i]._id}></i></div>
        </td>
      </tr>
    )
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Social Media Settings</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Social Media Settings</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Social Media Settings</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Icon Name</label>
                    <input type="text" className="form-control" name="iconname" value={values.iconname}onChange={onChangeValue}placeholder="Enter Icon Name"/>
                    {values.iconnameError ? <label style={{color:"red"}}>Icont Name is Required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Icon</label>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="icon" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.iconError ? <label style={{color:"red"}}>Icon is Required</label> : ""}
                    {values.iconcheck ? <label style={{color:"red"}}>Icon format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       <img src={values.iconSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Icon Link</label>
                    <input type="text" className="form-control" name="iconlink" value={values.iconlink}onChange={onChangeValue}placeholder="Enter Icon link"/>
                    {values.iconlinkError ? <label style={{color:"red"}}>Icon Link is Required</label> : ""}
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
      <section className="content" style={{display:viewSocialmedia.length?"block":"none"}}>
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body table-responsive p-0" >
                    <table className="table table-hover text-nowrap">
                     <thead>
                        <tr>
                          <th>Icon Name</th>
                          <th>Icon Link</th>
                          <th>Icon</th>
                          <th>Action</th>
                        </tr>
                       </thead>
                      <tbody>
                        {socialmedia}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default SocialmediaSettings;
