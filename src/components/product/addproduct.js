import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_LIST_SHOPS } from '../../actions/shop';
import { AC_LIST_MAINCATEGORIES } from '../../actions/category';
import { AC_ONCHANGE_SUBCATEGORIES,AC_ADD_PRODUCT,AC_ONCHANGE_CATEGORIES } from '../../actions/product';
import { Imagevalidation } from '../../common/validate';
import { AC_LIST_CITIES } from '../../actions/district';
import { AC_LIST_BRANDS } from '../../actions/brand';

const initialState = {
  maincategory          : "",
  subcategory           : "",
  category              : "",
  shop                  : "",
  slug                  : "",
  city                  : "",
  brand                 : "",
  title                 : "",
  discount              : "",
  discounttype          : "",
  price                 : "",
  discountprice         : "",
  productcondition      : "",
  youtubeurl            : "",
  description           : '',
  productaddress        : '',
  productlat            : '',
  productlon            : '',
  deliverystatus        : '',
  listingfeature        : '',
  images                : [],
  imgSrc                : [],
  status                : "active",
  maincategoryError     : false,
  categoryError         : false,
  subcategoryError      : false,
  shopError             : false,
  brandError            : false,
  cityError             : false,
  slugError             : false,
  titleError            : false,
  discountError         : false,
  discounttypeError     : false,
  priceError            : false,
  discountpriceError    : false,
  productconditionError : false,
  productaddressError   : false,
  productlatError       : false,
  productlonError       : false,
  deliverystatusError   : false,
  descriptionError      : false,
  imageError            : false,
  imagecheck            : false,
}
const AddProduct = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState(initialState);

  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    if(name=="images"){
       const files = Array.from(e.target.files);
       for (var i = 0 ; i < files.length ; i++)
       {
         var file            =  files[i];
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate){
           Promise.all(files.map(file => {
               return (new Promise((resolve,reject) => {
                   const reader = new FileReader();
                   reader.addEventListener('load', (ev) => {
                       resolve(ev.target.result);
                   });
                   reader.addEventListener('error', reject);
                   reader.readAsDataURL(file);
               }));
           }))
           .then(imagesdata => {
            setValues({ ...values,imageError:false,imgSrc : imagesdata,images : [...values.images, ...e.target.files],imagecheck:false })
           }, error => {
               console.error(error);
           });
         } else {
          setValues({...values,imagecheck:true,imageError:false});
         }
       }
    } else if(name=="maincategory"){
      dispatch(AC_ONCHANGE_SUBCATEGORIES({id:value,type:"subcategories"}));
      setValues({...values,[name]: value,[error]:false,subcategory:""});
    } else if(name=="subcategory"){
      // const mainId = values.maincategory;
      dispatch(AC_ONCHANGE_CATEGORIES({subcategory:value,type:"categories"}));
      setValues({...values,[name]: value,[error]:false,category:""});
    } else if(name=="slug"){
        var slug = value;
        slug = slug.replace(/[^\w\-]+/g, "-");
        slug = slug.toLowerCase();
      setValues({...values,[name]: slug,[error]:false});
    } else {
      setValues({...values,[name]: value,[error]:false});
    }
  };
  useEffect(()=>{
    dispatch(AC_LIST_SHOPS());
    dispatch(AC_LIST_MAINCATEGORIES());
    dispatch(AC_LIST_CITIES());
    dispatch(AC_LIST_BRANDS());
  },[])

  const shop = useSelector(state => state.ShopReducer)
  const shopsList = shop.shopsList;

  const category = useSelector(state => state.CategoryReducer)
  const maincategoriesList    = category.maincategoriesList;
  
  const product = useSelector(state => state.ProductReducer)
  const subcategoriesList    = product.subCategories;
  const categoriesList       = product.categories;

  const DistrictReducer = useSelector(state => state.DistrictReducer)
  const totalcitiesList    = DistrictReducer.totalcitiesList;

  const BrandReducer = useSelector(state => state.BrandReducer)
  const brandsList   = BrandReducer.brandsList;

  const brandsArray = [];
  for(var i=0;i<brandsList.length;i++) {
    brandsArray.push(
      <option key={i} value={brandsList[i]._id} selected={values.brand==brandsList[i]._id}> {brandsList[i].name}</option>
    )
  }

  const citiesArray = [];
  for(var i=0;i<totalcitiesList.length;i++) {
    citiesArray.push(
      <option key={i} value={totalcitiesList[i]._id} selected={values.city==totalcitiesList[i]._id}> {totalcitiesList[i].city}</option>
    )
  }
  const maincategoriesArray = []
  for(var i=0;i<maincategoriesList.length;i++) {
    maincategoriesArray.push(
      <option key={i} value={maincategoriesList[i]._id} selected={values.maincategory==maincategoriesList[i]._id}> {maincategoriesList[i].name}</option>
    )
  }
  if(maincategoriesList.length==0){
    maincategoriesArray.push(<option value="">No item...</option>)
  }
  const subcategoriesArray = []
  for(var i=0;i<subcategoriesList.length;i++) {
    subcategoriesArray.push(
      <option key={i} value={subcategoriesList[i]._id} selected={values.subcategory==subcategoriesList[i]._id}> {subcategoriesList[i].name}</option>
    )
  }
  if(subcategoriesList.length==0){
    subcategoriesArray.push(<option value="">No item...</option>)
  }
  const categoriesArray = []
  for(var i=0;i<categoriesList.length;i++) {
    categoriesArray.push(
      <option key={i} value={categoriesList[i]._id} selected={values.category==categoriesList[i]._id}> {categoriesList[i].name}</option>
    )
  }
  if(categoriesList.length==0){
    categoriesArray.push(<option value="">No item...</option>)
  }
  const shopsArray = []
  for(var i=0;i<shopsList.length;i++) {
    shopsArray.push(
      <option key={i} value={shopsList[i]._id} selected={values.shop==shopsList[i]._id}> {shopsList[i].shopname}</option>
    )
  }
  
  var discountprice = 0;
  if(values.discounttype=="flat"){
    discountprice = values.price?values.price-values.discount:values.price;
  } else if (values.discounttype=="%"){
    discountprice = values.price?values.price-values.price*(values.discount/100):values.price;
  } else {
    discountprice = values.price
  }

  const validateForm = () => {
     const data = {slug:false,productlon:false,productlat:false,productaddress:false,
                   youtubeurl:false, price:false, discounttype:false,
                   discount:false, title:false, shop:false,
                   subcategory:false,maincategory:false,description:false,
                   image:false, deliverystatus:false,category:false,city:false,brand:false};
     var check = 1;
     if(!values.maincategory){
       data.maincategory = true;
       check = 0;
     }
     if(!values.subcategory){
      data.subcategory = true;
      check = 0;
    }
    if(!values.category){
      data.category = true;
      check = 0;
    }
    if(!values.shop){
      data.shop = true;
      check = 0;
    }
    if(!values.brand){
      data.brand = true;
      check = 0;
    }
    if(!values.title){
      data.title = true;
      check = 0;
    }
    if(values.discounttype){
      // data.discounttype = true;
      if(!values.discount){
        data.discount = true;
        check = 0;
      }
    }
    if(!values.price){
      data.price = true;
      check = 0;
    }
    if(!values.slug){
      data.slug = true;
      check = 0;
    }
    if(!values.city){
      data.city = true;
      check = 0;
    }
    if(!values.productaddress){
      data.productaddress = true;
      check = 0;
    }
    if(!values.productlat){
      data.productlat = true;
      check = 0;
    }
    if(!values.productlon){
      data.productlon = true;
      check = 0;
    }
    if(!values.deliverystatus){
      data.deliverystatus = true;
      check = 0;
    }
    if(!values.productcondition){
      data.productcondition = true;
      check = 0;
    }
    if(!values.description){
      data.description = true;
      check = 0;
    }
    if(values.images.length==0){
      data.image = true;
      check = 0;
    }
    setValues({...values,brandError:data.brand,slugError:data.slug,categoryError:data.category,deliverystatusError:data.deliverystatus,productlonError:data.productlon, productlatError:data.productlat,
                  productaddressError:data.productaddress, descriptionError:data.description,
                  productconditionError:data.productcondition,priceError:data.price,discounttypeError:data.discounttype,
                  discountError:data.discount,titleError:data.title,shopError:data.shop,
                  subcategoryError:data.subcategory,maincategoryError:data.maincategory,descriptionError:data.description,
                  imageError:data.image,cityError:data.city});
    if( check&&!values.imagecheck) {
      const formData = new FormData();
      for (const file of values.images) {
         formData.append('images', file);
      }
      formData.append('id', "0");
      formData.append('maincategory', values.maincategory);
      formData.append('subcategory', values.subcategory);
      formData.append('category',  values.category);
      formData.append('shop', values.shop);
      formData.append('title', values.title);
      formData.append('brand', values.brand);
      formData.append('city', values.city);
      formData.append('discount', values.discount);
      formData.append('discounttype', values.discounttype);
      formData.append('slug', values.slug);
      formData.append('discountprice', discountprice);
      formData.append('price', values.price);
      formData.append('productcondition', values.productcondition);
      formData.append('description', values.description);
      formData.append('productaddress', values.productaddress);
      formData.append('productlat', values.productlat);
      formData.append('productlon', values.productlon);
      formData.append('deliverystatus', values.deliverystatus);
      formData.append('youtubeurl', values.youtubeurl);
      formData.append('status', values.status);
      formData.append('listingfeature', values.listingfeature);
      
      dispatch(AC_ADD_PRODUCT(formData));
      setValues({...initialState});

      // setValues({...values,category:"",price: "",productcondition : "", youtubeurl  : "",description : '',productaddress : '', productlat: '',productlon : '',
      // deliverystatus : '',maincategory:"",discount:"",discounttype:"",title:"",shop:"",subcategory:"",status:"active",maincategory:"",description:"",image:[],imgSrc:[]});
    }
  }
  const photo = []
  for (var i=0;i<values.imgSrc.length;i++){
    photo.push(<img src={values.imgSrc[i]} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>)
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Add Product</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Add Product</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Add Product</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Main Category</label><span style={{color:"red"}}>*</span>
                    <select className="form-control" name="maincategory" onChange={onChangeValue}>
                      <option value="">Select</option>
                      {maincategoriesArray}
                    </select>
                    {values.maincategoryError ? <label style={{color:"red"}}>Main category is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Category</label><span style={{color:"red"}}>*</span>
                    {values.subcategory&&values.maincategory?<select className="form-control" name="category" onChange={onChangeValue}>
                      <option value="">Select Category</option>
                      {categoriesArray}
                    </select>:
                    <select className="form-control" disabled>
                      <option value="">Select Category</option>
                    </select>}
                    {values.categoryError ? <label style={{color:"red"}}>Category is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Title</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="title" value={values.title}onChange={onChangeValue}placeholder="Enter Title"/>
                    {values.titleError ? <label style={{color:"red"}}>Title is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Discount Type</label>
                    <select className="form-control" name="discounttype" onChange={onChangeValue}>
                      <option value="" selected={values.discounttype==""}>Select</option>
                      <option value="flat"selected={values.discounttype=="flat"}>Flat</option>
                      <option value="%"selected={values.discounttype=="%"}>%</option>
                    </select>
                    {values.discounttypeError ? <label style={{color:"red"}}>Discount type is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Price</label><span style={{color:"red"}}>*</span>
                    <input type="number" className="form-control" name="price" value={values.price}onChange={onChangeValue}placeholder="Enter Price"/>
                    {values.priceError ? <label style={{color:"red"}}>Price is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Product condition</label><span style={{color:"red"}}>*</span>
                    {/* <input type="text" className="form-control" name="productcondition" value={values.productcondition}onChange={onChangeValue}placeholder="Enter Product condition"/> */}
                    <select className="form-control" name="productcondition" onChange={onChangeValue}>
                      <option value="" selected={values.productcondition==""}>Select</option>
                      <option value="new" selected={values.productcondition=="new"}>New</option>
                      <option value="used" selected={values.productcondition=="used"}>Used</option>
                    </select>
                    {values.productconditionError ? <label style={{color:"red"}}>Product condition is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Product Lat</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="productlat" value={values.productlat}onChange={onChangeValue}placeholder="Enter LAT"/>
                    {values.productlatError ? <label style={{color:"red"}}>Product LAT is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Product Address</label><span style={{color:"red"}}>*</span>
                    <textarea type="text" className="form-control" name="productaddress" value={values.productaddress}onChange={onChangeValue}placeholder="Enter Address"/>
                    {values.productaddressError ? <label style={{color:"red"}}>Address is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="active" selected={values.status=="active"}>Active</option>
                      <option value="inactive" selected={values.status=="inactive"}>Inactive</option>
                      <option value="sold" selected={values.status=="sold"}>Sold</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>City</label><span style={{color:"red"}}>*</span>
                    <select className="form-control" name="city" onChange={onChangeValue}>
                      <option value="" selected={values.city==""}>Select</option>
                      {citiesArray}
                    </select>
                    {values.cityError ? <label style={{color:"red"}}>City is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Images</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                        <div className="custom-file">
                            <input type="file" name="images" multiple className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                            <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                        </div>
                        <div className="input-group-append">
                            <button className="input-group-text" id="">Upload</button>
                        </div>
                    </div>
                    {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                    {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                        {photo}
                      </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Sub Category</label><span style={{color:"red"}}>*</span>
                    {values.maincategory?<select className="form-control" name="subcategory" onChange={onChangeValue}>
                      <option value="">Select subcategory</option>
                      {subcategoriesArray}
                    </select>:
                    <select className="form-control" disabled name="subcategory" onChange={onChangeValue}>
                      <option value="">Select subcategory</option>
                    </select>}
                    {values.subcategoryError ? <label style={{color:"red"}}>Sub category is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Brand</label><span style={{color:"red"}}>*</span>
                    <select className="form-control" name="brand" onChange={onChangeValue}>
                      <option value="">Select</option>
                      {brandsArray}
                    </select>
                    {values.brandError ? <label style={{color:"red"}}>Brand is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Shop</label><span style={{color:"red"}}>*</span>
                    <select className="form-control" name="shop" onChange={onChangeValue}>
                      <option value="">Select</option>
                      {shopsArray}
                    </select>
                    {values.shopError ? <label style={{color:"red"}}>Shop is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Slug</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="slug" onChange={onChangeValue} value={values.slug} placeholder="Enter Slug"/>
                    {values.slugError ? <label style={{color:"red"}}>Slug is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Discount</label>
                    <input type="text" className="form-control" name="discount" value={values.discount}onChange={onChangeValue}placeholder="Enter Discount"/>
                    {values.discountError ? <label style={{color:"red"}}>Discount is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Discount Price</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="discountprice" value={discountprice}onChange={onChangeValue}placeholder="Enter Discount Price"/>
                    {values.discountpriceError ? <label style={{color:"red"}}>Discount price is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Youtube URL (optional)</label>
                    <input type="text" className="form-control" name="youtubeurl" value={values.youtubeurl}onChange={onChangeValue}placeholder="Enter URL"/>
                  </div>
                  <div className="form-group">
                    <label>Product LON</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="productlon" value={values.productlon}onChange={onChangeValue}placeholder="Enter LON"/>
                    {values.productlonError ? <label style={{color:"red"}}>Product LON is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Product deliver status</label><span style={{color:"red"}}>*</span>
                    <select className="form-control" name="deliverystatus" onChange={onChangeValue}>
                      <option value=""selected={values.deliverystatus==""}>Select</option>
                      <option value="free" selected={values.deliverystatus=="free"}>Free</option>
                      <option value="paid" selected={values.deliverystatus=="free"}>Paid</option>
                    </select>
                    {values.deliverystatusError ? <label style={{color:"red"}}>Delivery is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Description</label><span style={{color:"red"}}>*</span>
                    <textarea type="textarea" className="form-control" name="description" value={values.description} onChange={onChangeValue}placeholder="Enter Description"/>
                    {values.descriptionError ? <label style={{color:"red"}}>Description is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Listing Feature</label>
                    <select className="form-control" name="listingfeature" onChange={onChangeValue}>
                      <option value="" selected={values.listingfeature==""}>None</option>
                      <option value="popular" selected={values.listingfeature=="popular"}>Popular</option>
                      <option value="discount" selected={values.listingfeature=="discount"}>Discount</option>
                      <option value="bidding" selected={values.listingfeature=="bidding"}>Bidding</option>
                      <option value="latest" selected={values.listingfeature=="latest"}>Latest</option>
                    </select>
                  </div>
                  
                </div>
              </div>
            </div>
            <div className="card-footer">
              <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default AddProduct;
