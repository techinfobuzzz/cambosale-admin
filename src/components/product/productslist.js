import React,{ useState, useEffect } from 'react';
import { Link ,Redirect} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AC_LIST_PRODUCTS,AC_DELETE_PRODUCT } from '../../actions/product';
import { COLDEF } from '../../common/gridoptions';
import swal from 'sweetalert';
import API from '../../common/api';

const ProductsList = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(AC_LIST_PRODUCTS());
  },[])
  const product = useSelector(state => state.ProductReducer)
  const productsList = product.productsList;

  const [value, setValue] = useState({
    columnDefs :[
      { "headerName" : "No",field:"#",valueGetter :"node.rowIndex+1",sortable:false, width: "100"
      },
      { "headerName" : "Name",field:"title",
      },
      { "headerName" : "Main category",field:"maincategory.name",width: "150",
      },
      { "headerName" : "Sub category",field:"subcategory.name",width: "150",
      },
      { "headerName" : "Category",field:"category.name", width: "150",
      },
      
      { "header" : "Status", field :"status", width: "150",
         cellRenderer: function(params) {
          if(params.data.status=="active") {
            return '<span class="badge badge-success" data-action-type="Status">Active</span>';
          } else if(params.data.status=="sold"){
            return '<span class="badge badge-warning" data-action-type="Status">Sold</span>';
          } else  {
            return '<span class="badge badge-success" style="background-color: rgb(220, 53, 69);" data-action-type="Status">Inactive</span>';
          }
        }
      },
      { headerName: 'Actions', width: "200",sortable: false,filter:false,
        template:
           `<div>
           <div class="btn bg-info btn-xs" href='#' style={{marginRight:"10px",height:"5px",width:"5px"}} data-action-type="View">
           <i class="fas fa-folder"  data-action-type="View"></i>
           View
           </div>
           <div class="btn bg-primary btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Edit">
           <i class="fas fa-pencil-alt" data-action-type="Edit"></i>
           Edit
           </div>
           <div class="btn bg-danger btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Delete">
           <i class="fas fa-trash"  data-action-type="Delete"></i>
           Delete
           </div>
           </div>`,
       },
   ],
   defaultColDef: COLDEF,
  });
    const onSortChanged = e => {
     e.api.refreshCells();
    }
   const onRowClicked = event =>{
    const rowValue = event.event.target;
    const value    = rowValue.getAttribute('data-action-type');
    if(value === 'View'){
      setValue({...value,redirect:'View',id:event.data._id})
    }
    if(value === 'Edit'){
      setValue({...value,redirect:'Edit',id:event.data._id})
    }
    if(value === 'Delete'){
      swal({
           // title: "Are you sure?",
           text: "Once deleted, you will not be able to recover this data!",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             dispatch(AC_DELETE_PRODUCT({id:event.data._id}));
           }
     });
    }
  }


  if(value.redirect === 'View'){
    return <Redirect to ={'/view-product/'+value.id}/>
  }
  if(value.redirect === 'Edit'){
    return <Redirect to ={'/edit-product/'+value.id}/>
  }
  return(
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Products List</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Products List</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <div className="ag-theme-balham" >
      <AgGridReact
          rowHeight={40}
          rowClass ={'centerAlign'}
          onSortChanged={onSortChanged}
          rowSelection={'single'}
          onRowClicked={onRowClicked}
          suppressCellSelection={true}
          pagination={true}
          domLayout={"autoHeight"}
          defaultColDef={COLDEF}
          columnDefs={value.columnDefs}
          rowData={productsList}>
      </AgGridReact>
      </div>
    </div>
  )
}

export default ProductsList;
