import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_LIST_SHOPS } from '../../actions/shop';
import { AC_LIST_MAINCATEGORIES } from '../../actions/category';
import { AC_HANDLE_SUBCATEGORIES,AC_HANDLE_CATEGORIES,AC_EDIT_PRODUCT,AC_VIEW_PRODUCT,AC_HANDLE_CHANGE } from '../../actions/product';
import { Imagevalidation } from '../../common/validate';
import API from '../../common/api';
import { AC_LIST_CITIES } from '../../actions/district';
import { AC_LIST_BRANDS } from '../../actions/brand';

const EditProduct = (props) => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    images                : [],
    imgSrc                : [],
    maincategoryError     : false,
    subcategoryError      : false,
    categoryError         : false,
    shopError             : false,
    localeError           : false,
    titleError            : false,
    discountError         : false,
    discounttypeError     : false,
    priceError            : false,
    discountpriceError    : false,
    productconditionError : false,
    youtubeurlError       : false,
    productaddressError   : false,
    productlatError       : false,
    productlonError       : false,
    deliverystatusError   : false,
    descriptionError      : false,
    imageError            : false,
    imagecheck            : false,
  });

  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    if(name=="images"){
       const files = Array.from(e.target.files);
       for (var i = 0 ; i < files.length ; i++)
       {
         var file            =  files[i];
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate){
           Promise.all(files.map(file => {
               return (new Promise((resolve,reject) => {
                   const reader = new FileReader();
                   reader.addEventListener('load', (ev) => {
                       resolve(ev.target.result);
                   });
                   reader.addEventListener('error', reject);
                   reader.readAsDataURL(file);
               }));
           }))
           .then(imagesdata => {
             setValues({ ...values,imageError:false,imgSrc : imagesdata,images : [...values.images, ...e.target.files],imagecheck:false })
           }, error => {
               console.error(error);
           });
         } else {
          setValues({...values,imagecheck:true,imageError:false});
         }
       }
    } else if(name=="maincategory"){
        dispatch(AC_HANDLE_SUBCATEGORIES({id:value,type:"subcategories"}));
        dispatch(AC_HANDLE_CHANGE(name,value));
        dispatch(AC_HANDLE_CHANGE("subcategory",""));
        setValues({...values,[error]:false});
    } else if(name=="subcategory"){
      dispatch(AC_HANDLE_CATEGORIES({subcategory:value,type:"categories"}));
      dispatch(AC_HANDLE_CHANGE(name,value));
      dispatch(AC_HANDLE_CHANGE("category",""));
      setValues({...values,[error]:false});
    } else {
          dispatch(AC_HANDLE_CHANGE(name,value));
          setValues({...values,[error]:false});
      }
  };
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_PRODUCT({id:id}));
    dispatch(AC_LIST_SHOPS());
    dispatch(AC_LIST_MAINCATEGORIES());
    dispatch(AC_LIST_CITIES());
    dispatch(AC_LIST_BRANDS());
  },[])

  const shops = useSelector(state => state.ShopReducer)
  const shopsList = shops.shopsList;

  const categoryReducer = useSelector(state => state.CategoryReducer)
  const maincategoriesList    = categoryReducer.maincategoriesList;
  
  const product = useSelector(state => state.ProductReducer)
  const subcategoriesList    = product.viewsubCategories;
  const categoriesList       = product.viewCategories;
  const viewProduct          = product.viewProduct;

  const DistrictReducer = useSelector(state => state.DistrictReducer)
  const totalcitiesList    = DistrictReducer.totalcitiesList;

  const BrandReducer = useSelector(state => state.BrandReducer)
  const brandsList   = BrandReducer.brandsList;

  var images = []

  if(viewProduct){
    var maincategory      = viewProduct.maincategory
    var subcategory       = viewProduct.subcategory
    var category          = viewProduct.category
    var shop              = viewProduct.shop
    var locale            = viewProduct.locale
    var discountprice     = viewProduct.discountprice
    var slug              = viewProduct.slug
    var description       = viewProduct.description
    var discount          = viewProduct.discount
    var discounttype      = viewProduct.discounttype
    var price             = viewProduct.price
    var status            = viewProduct.status
    var title             = viewProduct.title
    var productcondition  = viewProduct.productcondition
    var productlat        = viewProduct.productlat
    var productaddress    = viewProduct.productaddress
    var youtubeurl        = viewProduct.youtubeurl
    var productlon        = viewProduct.productlon
    var deliverystatus    = viewProduct.deliverystatus
    var listingfeature    = viewProduct.listingfeature
    var slug              = viewProduct.slug
    var brand             = viewProduct.brand
    var city              = viewProduct.city
    images                = viewProduct.images
  }
  
  const brandsArray = [];
  for(var i=0;i<brandsList.length;i++) {
    brandsArray.push(
      <option key={i} value={brandsList[i]._id} selected={brand==brandsList[i]._id}> {brandsList[i].name}</option>
    )
  }
  const citiesArray = [];
  for(var i=0;i<totalcitiesList.length;i++) {
    citiesArray.push(
      <option key={i} value={totalcitiesList[i]._id} selected={city==totalcitiesList[i]._id}> {totalcitiesList[i].city}</option>
    )
  }

  const maincategoriesArray = []
  for(var i=0;i<maincategoriesList.length;i++) {
    maincategoriesArray.push(
      <option key={i} value={maincategoriesList[i]._id} selected={maincategory==maincategoriesList[i]._id}> {maincategoriesList[i].name}</option>
    )
  }
  const subcategoriesArray = []
  for(var i=0;i<subcategoriesList.length;i++) {
    subcategoriesArray.push(
      <option key={i} value={subcategoriesList[i]._id} selected={subcategory==subcategoriesList[i]._id}> {subcategoriesList[i].name}</option>
    )
  }
  if(subcategoriesList.length==0){
    subcategoriesArray.push(<option value="">No item...</option>)
  }
  const categoriesArray = []
  for(var i=0;i<categoriesList.length;i++) {
    categoriesArray.push(
      <option key={i} value={categoriesList[i]._id} selected={category==categoriesList[i]._id}> {categoriesList[i].name}</option>
    )
  }
  if(categoriesList.length==0){
    categoriesArray.push(<option value="">No item...</option>)
  }
  const shopsArray = []
  for(var i=0;i<shopsList.length;i++) {
    shopsArray.push(
      <option key={i} value={shopsList[i]._id} selected={shop==shopsList[i]._id}> {shopsList[i].shopname}</option>
    )
  }
  
  var discountpricevalue = 0;
  if(discounttype=="flat"){
    discountpricevalue = price?price-discount:price;
  } else if (discounttype=="%"){
    discountpricevalue = price?price-price*(discount/100):price;
  } else {
    discountprice = viewProduct.price
  }

  const validateForm = () => {
     const data = {productlon:false,productlat:false,productaddress:false,
                   youtubeurl:false,price:false,discounttype:false,
                   discount:false,title:false,locale:false,shop:false,
                   subcategory:false,maincategory:false,description:false,
                   image:false,deliverystatus:false,category:false};
    const id = props.match.params.id;
    var imagesdata = viewProduct.images;
    var check = 1;
     if(values.images.length){
      imagesdata = values.images
     }
     if(! viewProduct.maincategory){
       data.maincategory = true;
       check=0;
     }
     if(! viewProduct.subcategory){
      data.subcategory = true;
      check=0;
    }
    if(! viewProduct.category){
      data.category = true;
      check=0;
    }
    // if(! viewProduct.shop){
    //   data.shop = true;
    //   check=0;
    // }
    if(! viewProduct.title){
      data.title = true;
      check=0;
    }
    // if(! viewProduct.discount){
    //   data.discount = true;
    //   check=0;
    // }
    // if(! viewProduct.discounttype){
    //   data.discounttype = true;
    //   check=0;
    // }
    if(! viewProduct.price){
      data.price = true;
      check=0;
    }
    // if(! viewProduct.youtubeurl){
    //   data.youtubeurl = true;
    //   check=0;
    // }
    if(! viewProduct.productaddress){
      data.productaddress = true;
      check=0;
    }
    // if(! viewProduct.productlat){
    //   data.productlat = true;
    //   check=0;
    // }
    // if(! viewProduct.productlon){
    //   data.productlon = true;
    //   check=0;
    // }
    if(! viewProduct.deliverystatus){
      data.deliverystatus = true;
      check=0;
    }
    if(! viewProduct.productcondition){
      data.productcondition = true;
      check=0;
    }
     if(! viewProduct.description){
       data.description = true;
       check=0;
     }
     if( imagesdata.length==0){
       data.image = true;
       check=0;
     }
     setValues({...values,categoryError:data.category,deliverystatusError:data.deliverystatus,productlonError:data.productlon,productlatError:data.productlat,productaddressError:data.productaddress,descriptionError:data.description,youtubeurlError:data.youtubeurl,productconditionError:data.productcondition,priceError:data.price,discounttypeError:data.discounttype,discountError:data.discount,titleError:data.title,localeError:data.locale,shopError:data.shop,subcategoryError:data.subcategory,maincategoryError:data.maincategory,descriptionError:data.description,imageError:data.image});
      if( check &&! viewProduct.imagecheck){
       const formData = new FormData();
       for (const file of  imagesdata) {
        formData.append('images', file);
       }
       formData.append('id', id);
       formData.append('maincategory',  viewProduct.maincategory);
       formData.append('subcategory',  viewProduct.subcategory);
       formData.append('category',  viewProduct.category);
       if(viewProduct.shop){
        formData.append('shop',  viewProduct.shop);
       }
       formData.append('title',  viewProduct.title);
       formData.append('discount',  viewProduct.discount);
       formData.append('discounttype',  viewProduct.discounttype);
       formData.append('slug', viewProduct.slug);
       formData.append('city', viewProduct.city);
       formData.append('brand', viewProduct.brand);
       formData.append('discountprice', discountprice);
       formData.append('price',  viewProduct.price);
       formData.append('productcondition',  viewProduct.productcondition);
       formData.append('description',  viewProduct.description);
       formData.append('productaddress',  viewProduct.productaddress);
       formData.append('productlat',  viewProduct.productlat);
       formData.append('productlon',  viewProduct.productlon);
       formData.append('deliverystatus',  viewProduct.deliverystatus);
       formData.append('youtubeurl',  viewProduct.youtubeurl);
       formData.append('status',  viewProduct.status);
       formData.append('listingfeature',  viewProduct.listingfeature);
       
       dispatch(AC_EDIT_PRODUCT(formData));
      //  setValues({...values,price: 0,productcondition : "", youtubeurl  : "",description : '',productaddress : '', productlat: '',productlon : '',
      //   deliverystatus : '',maincategory:"",discount:"",discounttype:"",title:"",locale:"",shop:"",subcategory:"",status:"active",maincategory:"",description:"",image:[],imgSrc:[]});
   }
  }
  const photo = []
  if(values.imgSrc.length){
    for (var i=0;i<values.imgSrc.length;i++){
      photo.push(<img src={values.imgSrc[i]} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>)
    }
  } else if (images ){
    for (var i=0;i< images.length;i++){
      photo.push(<img src={API.LIVEURL+"uploads/"+images[i]} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>)
    }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Edit Product</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Edit Product</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Edit Product</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Main Category</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="maincategory" onChange={onChangeValue}>
                        {maincategoriesArray}
                      </select>
                      {values.maincategoryError ? <label style={{color:"red"}}>Main category is required</label> : ""}
                    </div>
                    <div className="form-group">
                    <label>Category</label><span style={{color:"red"}}>*</span>
                    {subcategory&&maincategory?<select className="form-control" name="category" onChange={onChangeValue}>
                        <option value="">Select Category</option>
                        {categoriesArray}
                      </select>:
                      <select className="form-control" disabled>
                        <option value="">Select Category</option>
                      </select>}
                      {values.categoryError ? <label style={{color:"red"}}>Category is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Title</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="title" value={ title}onChange={onChangeValue}placeholder="Enter Title"/>
                      {values.titleError ? <label style={{color:"red"}}>Title is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Discount Type</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="discounttype" onChange={onChangeValue}>
                        <option value="flat"selected={ discounttype=="flat"}>Flat</option>
                        <option value="%"selected={ discounttype=="%"}>%</option>
                      </select>
                      {values.discounttypeError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Price</label><span style={{color:"red"}}>*</span>
                      <input type="number" className="form-control" name="price" value={ price}onChange={onChangeValue}placeholder="Enter Price"/>
                      {values.priceError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Product condition</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="productcondition" value={ productcondition}onChange={onChangeValue}placeholder="Enter Product condition"/>
                      {values.productconditionError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Product Lat</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="productlat" value={productlat}onChange={onChangeValue}placeholder="Enter LAT"/>
                      {values.productlatError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Product Address</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" name="productaddress" value={productaddress}onChange={onChangeValue}placeholder="Enter Address"/>
                      {values.productaddressError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="active" selected={status=="active"}>Active</option>
                        <option value="inactive" selected={status=="inactive"}>Inactive</option>
                        <option value="sold" selected={status=="sold"}>Sold</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>City</label>
                      <select className="form-control" name="city" onChange={onChangeValue}>
                        {citiesArray}
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Images</label><span style={{color:"red"}}>*</span>
                      <div className="input-group">
                         <div className="custom-file">
                             <input type="file" name="images" multiple className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                             <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                         </div>
                         <div className="input-group-append">
                             <button className="input-group-text" id="">Upload</button>
                         </div>
                      </div>
                      {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                      {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                      <div>
                         {photo}
                       </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Sub Category</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="subcategory" onChange={onChangeValue}>
                        <option value="" selected={subcategory==""}>Select</option>
                        {subcategoriesArray}
                      </select>
                      {values.subcategoryError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Brand</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="brand" onChange={onChangeValue}>
                        {/* <option>Select</option> */}
                        {brandsArray}
                      </select>
                      {values.brandError ? <label style={{color:"red"}}>Brand is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Shop</label>
                      <select className="form-control" name="shop" onChange={onChangeValue}>
                        <option value="">Select</option>
                        {shopsArray}
                      </select>
                      {values.shopError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Slug</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="slug" value={slug} disabled placeholder="Enter Slug"/>
                      {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Discount</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="discount" value={discount}onChange={onChangeValue}placeholder="Enter Discount"/>
                      {values.discountError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Discount Price</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="discountprice" value={discountpricevalue}onChange={onChangeValue}placeholder="Enter Discount Price"/>
                      {values.discountpriceError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Youtube URL</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="youtubeurl" value={youtubeurl}onChange={onChangeValue}placeholder="Enter URL"/>
                      {values.youtubeurlError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Product LON</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="productlon" value={productlon}onChange={onChangeValue}placeholder="Enter LON"/>
                      {values.productlonError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Product deliver status</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="deliverystatus" onChange={onChangeValue}>
                        <option value="free" selected={deliverystatus=="free"}>Free</option>
                        <option value="paid" selected={deliverystatus=="paid"}>Paid</option>
                      </select>
                      {values.deliverystatusError ? <label style={{color:"red"}}>Delivery is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Description</label><span style={{color:"red"}}>*</span>
                      <textarea type="textarea" className="form-control" name="description" value={description} onChange={onChangeValue}placeholder="Enter Description"/>
                      {values.descriptionError ? <label style={{color:"red"}}>Description is required</label> : ""}
                    </div>
                    <div className="form-group">
                    <label>Listing Feature</label>
                    <select className="form-control" name="listingfeature" onChange={onChangeValue}>
                      <option value="" selected={listingfeature==""}>None</option>
                      <option value="popular" selected={listingfeature=="popular"}>Popular</option>
                      <option value="discount" selected={listingfeature=="discount"}>Discount</option>
                      <option value="bidding" selected={listingfeature=="bidding"}>Bidding</option>
                      <option value="latest" selected={listingfeature=="latest"}>Latest</option>
                    </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default EditProduct;
