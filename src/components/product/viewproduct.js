import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_LIST_SHOPS } from '../../actions/shop';
import { AC_LIST_MAINCATEGORIES } from '../../actions/category';
import { AC_VIEW_PRODUCT } from '../../actions/product';
import { Imagevalidation } from '../../common/validate';
import API from '../../common/api';

const AddProduct = (props) => {
  const dispatch = useDispatch()

  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_PRODUCT({id:id}));
    dispatch(AC_LIST_SHOPS());
    dispatch(AC_LIST_MAINCATEGORIES());
  },[])

  const shops = useSelector(state => state.ShopReducer)
  const shopsList = shops.shopsList;

  const categoryReducer       = useSelector(state => state.CategoryReducer)
  const maincategoriesList    = categoryReducer.maincategoriesList;
  
  const product = useSelector(state => state.ProductReducer)
  const subcategoriesList    = product.viewsubCategories;
  const categoriesList    = product.viewCategories;
  const viewProduct    = product.viewProduct;
  var images = []
  if(viewProduct){
    var maincategory      = viewProduct.maincategory
    var subcategory       = viewProduct.subcategory
    var category          = viewProduct.category
    var shop              = viewProduct.shop
    var locale            = viewProduct.locale
    var discountprice     = viewProduct.discountprice
    var slug              = viewProduct.slug
    var description       = viewProduct.description
    var discount          = viewProduct.discount
    var discounttype      = viewProduct.discounttype
    var price             = viewProduct.price
    var status            = viewProduct.status
    var title             = viewProduct.title
    var productcondition  = viewProduct.productcondition
    var productlat        = viewProduct.productlat
    var productaddress    = viewProduct.productaddress
    var youtubeurl        = viewProduct.youtubeurl
    var productlon        = viewProduct.productlon
    var deliverystatus    = viewProduct.deliverystatus
    var listingfeature    = viewProduct.listingfeature
    images                = viewProduct.images
  }
  const maincategoriesArray = []
  for(var i=0;i< maincategoriesList.length;i++) {
    maincategoriesArray.push(
      <option key={i} value={ maincategoriesList[i]._id} selected={maincategory== maincategoriesList[i]._id}> { maincategoriesList[i].name}</option>
    )
  }
  const subcategoriesArray = []
  for(var i=0;i<subcategoriesList.length;i++) {
    subcategoriesArray.push(
      <option key={i} value={subcategoriesList[i]._id} selected={subcategory==subcategoriesList[i]._id}> {subcategoriesList[i].name}</option>
    )
  }
  const categoriesArray = []
  for(var i=0;i<categoriesList.length;i++) {
    categoriesArray.push(
      <option key={i} value={categoriesList[i]._id} selected={category==categoriesList[i]._id}> {categoriesList[i].name}</option>
    )
  }
  const shopsArray = []
  for(var i=0;i<shopsList.length;i++) {
    shopsArray.push(
      <option key={i} value={shopsList[i]._id} selected={shop==shopsList[i]._id}> {shopsList[i].shopname}</option>
    )
  }

  const photo = []
  if(images){
    for (var i=0;i< images.length;i++){
      photo.push(<img src={API.LIVEURL+"uploads/"+images[i]} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>)
    }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>View Product</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">View Product</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>View Product</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Main Category</label>  
                      <select   className="form-control"  disabled name="maincategory"  >
                        <option value="">Select</option>
                        {maincategoriesArray}
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Main Category</label>  
                      <select   className="form-control"  disabled name="maincategory"  >
                        <option value="">Select</option>
                        {categoriesArray}
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Title</label>  
                      <input type="text"   className="form-control"  disabled name="title" value={ title} placeholder="Enter Title"/>
                    </div>
                    <div className="form-group">
                      <label>Discount Type</label>  
                      <select   className="form-control"  disabled name="discounttype"  >
                        <option value="" selected={ discounttype==""}>Select</option>
                        <option value="flat"selected={ discounttype=="flat"}>Flat</option>
                        <option value="%"selected={ discounttype=="%"}>%</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Price</label>  
                      <input type="number"   className="form-control"  disabled name="price" value={ price} placeholder="Enter Price"/>
                    </div>
                    <div className="form-group">
                      <label>Product condition</label>  
                      <input type="text"   className="form-control"  disabled name="productcondition" value={ productcondition} placeholder="Enter Product condition"/>
                    </div>
                    <div className="form-group">
                      <label>Product Lat</label>  
                      <input type="text"   className="form-control"  disabled name="productlat" value={ productlat} placeholder="Enter LAT"/>
                    </div>
                    <div className="form-group">
                      <label>Product Address</label>  
                      <textarea type="text"   className="form-control"  disabled name="productaddress" value={ productaddress} placeholder="Enter Address"/>
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select   className="form-control"  disabled name="status"  >
                        <option value="active" selected={ status=="active"}>Active</option>
                        <option value="inactive" selected={ status=="inactive"}>Inactive</option>
                        <option value="sold" selected={ status=="sold"}>Sold</option>
                      </select>
                    </div>
                    <div className="form-group">
                    <label>Listing Feature</label>
                    <select className="form-control" name="listingfeature" disabled>
                      <option value="" >None</option>
                      <option value="popular" selected={ listingfeature=="popular"}>Popular</option>
                      <option value="discount" selected={ listingfeature=="discount"}>Discount</option>
                      <option value="bidding" selected={ listingfeature=="bidding"}>Bidding</option>
                      <option value="latest" selected={ listingfeature=="latest"}>Latest</option>
                    </select>
                  </div>
                    <div className="form-group">
                      <label>Images</label>  
                      <div>
                         {photo}
                       </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Sub Category</label>  
                      <select   className="form-control"  disabled name="subcategory"  >
                        <option value="">Select</option>
                        {subcategoriesArray}
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Shop</label>  
                      <select   className="form-control"  disabled name="shop"  >
                        <option value="">Select</option>
                        {shopsArray}
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Slug</label>  
                      <input type="text"   className="form-control"  disabled name="slug" value={slug} placeholder="Enter Slug"/>
                    </div>
                    <div className="form-group">
                      <label>Discount</label>  
                      <input type="text"   className="form-control"  disabled name="discount" value={ discount} placeholder="Enter Discount"/>
                    </div>
                    <div className="form-group">
                      <label>Discount Price</label>  
                      <input type="text"   className="form-control"  disabled name="discountprice" value={discountprice} placeholder="Enter Discount Price"/>
                    </div>
                    <div className="form-group">
                      <label>Youtube URL</label>  
                      <input type="text"   className="form-control"  disabled name="youtubeurl" value={ youtubeurl} placeholder="Enter URL"/>
                    </div>
                    <div className="form-group">
                      <label>Product LON</label>  
                      <input type="text"   className="form-control"  disabled name="productlon" value={ productlon} placeholder="Enter LON"/>
                    </div>
                    <div className="form-group">
                      <label>Product deliver status</label>  
                      <select   className="form-control"  disabled name="deliverystatus"  >
                        <option value=""selected={ deliverystatus==""}>Select</option>
                        <option value="packed" selected={ deliverystatus=="packed"}>Packed</option>
                        <option value="shipped" selected={ deliverystatus=="shipped"}>Shipped</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Description</label>  
                      <textarea type="textarea"   className="form-control"  disabled name="description" value={ description}  placeholder="Enter Description"/>
                    </div>
                    <div className="form-group">
                      <label>Locale</label>  
                      <input type="text"   className="form-control"  disabled name="locale" value={ locale} placeholder="Enter Locale"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddProduct;
