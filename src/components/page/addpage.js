import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_ADD_PAGE } from '../../actions/page';

const AddPage = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    name             : "",
    slug             : "",
    description      : '',
    status           : "1",
    nameError        : false,
    descriptionError : false,
  });
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    setValues({...values,[name]: value,[error]:false});
  };
  useEffect(()=>{
    window.$(function () {
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const validateForm = () => {
     const data = {name:false,description:false};
     const description    =  window.$('#compose-textarea').summernote('code');
     if(!values.name){
       data.name = true;
     }
     if(description=="<p><br></p>"){
       data.description = true;
     }
     setValues({...values,nameError:data.name,descriptionError:data.description});
     if(values.name&&description!="<p><br></p>"){
        var formData = {
          id             : "0",
          name           : values.name,
          description    : description,
          status         : values.status,
      }
      dispatch(AC_ADD_PAGE(formData));
      setValues({...values,status:"1",name:"",descriptionError:false});
      window.$('#compose-textarea').summernote('reset');
   }
  }

    var slug = values.name;
    slug = slug.replace(/[^\w\-]+/g, "-");
    slug = slug.toLowerCase();
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Add Page</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Add Page</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Add Page</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Name</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="name" value={values.name}onChange={onChangeValue}placeholder="Enter Page name"/>
                      {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Description</label><span style={{color:"red"}}>*</span>
                      <textarea type="textarea" id="compose-textarea"  className="form-control" name="description" value={values.description} onChange={onChangeValue}placeholder="Enter Language name"/>
                      {values.descriptionError ? <label style={{color:"red"}}>Description is required</label> : ""}
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Slug</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="slug" value={slug} placeholder="Page Slug"/>
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="1" selected={values.status=="1"}>Active</option>
                        <option value="0" selected={values.status=="0"}>Inactive</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddPage;
