import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_PAGE,AC_HANDLE_CHANGE,AC_EDIT_PAGE} from '../../actions/page';

const EditPage = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_PAGE({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const [values, setValues] = useState({
    nameError           : false,
    descriptionError    : false,
  });

  const page = useSelector(state => state.PageReducer)
  const viewPage = page.viewPage;
  if(viewPage) {
    var name          = viewPage.name
    var description   = viewPage.description
    var status        = viewPage.status
    window.$('#compose-textarea').summernote('code',description);
  }
  var slug          = ""
  if(name){
    slug              = name.replace(/[^\w\-]+/g, "-");
    slug              =  slug.toLowerCase();
  }

  const validateForm = () => {
    const data          = page.viewPage;
    const stateData     = {name:false,description:false};
    const description   = window.$('#compose-textarea').summernote('code');
    const id            = props.match.params.id;
    if(!data.name){
       stateData.name = true;
     }
    if(description=="<p><br></p>"){
       stateData.description = true;
     }
    setValues({...values,nameError:stateData.name,descriptionError:stateData.description});
    if(data.name&&description!="<p><br></p>") {
      var formData = {
        id             : id,
        name           : data.name,
        description    : description,
        status         : data.status,
      }
      dispatch(AC_EDIT_PAGE(formData));
   }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     dispatch(AC_HANDLE_CHANGE(name,value));
     dispatch(AC_HANDLE_CHANGE("description",window.$('#compose-textarea').summernote('code')));
     setValues({...values,[error]:false});
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Page</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Page</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Page</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={name}onChange={onChangeValue}placeholder="Enter Page name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                     <label>Description</label>
                     <textarea type="textarea" id="compose-textarea" name="description" className="form-control" style={{height: "200px"}}/>
                     {values.descriptionError?<label style={{color:"red"}}>Description is Required</label>:""}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Slug</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="slug" value={slug} disabled placeholder="Slug"/>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditPage;
