import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_NEWSLETTER,AC_HANDLE_CHANGE,AC_EDIT_NEWSLETTER} from '../../actions/newsletter';

const EditPage = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_NEWSLETTER({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const [values, setValues] = useState({
    templatenameError       : false,
    emailcontentError       : false,
    emailsubjectError       : false,
  });

  const newsletter = useSelector(state => state.NewsletterReducer)
  const viewNewsletter = newsletter.viewNewsletter;
  if(viewNewsletter) {
    var templatename  = viewNewsletter.templatename
    var emailcontent  = viewNewsletter.emailcontent
    var emailsubject  = viewNewsletter.emailsubject
    var status        = viewNewsletter.status
    window.$('#compose-textarea').summernote('code',emailcontent);
  }

  const validateForm = () => {
    const data          = viewNewsletter;
    const stateData     = {name:false,subject:false,content:false};
    const emailcontent  = window.$('#compose-textarea').summernote('code');
    const id            = props.match.params.id;
    if(!data.templatename){
       stateData.name = true;
     }
    if(!data.emailsubject){
      stateData.subject = true;
    }
    if(emailcontent=="<p><br></p>"){
      stateData.content = true;
    }
    setValues({...values,templatenameError:data.name,emailcontentError:data.content,emailsubjectError:data.subject});
    if(data.templatename&&data.emailsubject&&emailcontent!="<p><br></p>"){
      var formData = {
        id             : id,
        templatename   : data.templatename,
        emailsubject   : data.emailsubject,
        emailcontent   : emailcontent,
        status         : data.status,
      }
      dispatch(AC_EDIT_NEWSLETTER(formData));
   }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     dispatch(AC_HANDLE_CHANGE(name,value));
     dispatch(AC_HANDLE_CHANGE("emailcontent",window.$('#compose-textarea').summernote('code')));
     setValues({...values,[error]:false});
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Page</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Page</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Page</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Template Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="templatename" value={templatename}onChange={onChangeValue}placeholder="Enter Template name"/>
                    {values.templatename ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Email Content</label><span style={{color:"red"}}>*</span>
                     <textarea type="textarea" id="compose-textarea" name="emailcontent" className="form-control" style={{height: "200px"}}/>
                     {values.emailcontentError ? <label style={{color:"red"}}>Email Content is required</label> : ""}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Email Subject</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="emailsubject" value={emailsubject} onChange={onChangeValue} placeholder="Email Subject"/>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditPage;
