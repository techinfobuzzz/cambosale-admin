import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_NEWSLETTER} from '../../actions/newsletter';

const ViewNewsletter = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_NEWSLETTER({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
      window.$('#compose-textarea').summernote('disable');
    })
  },[])
  const newsletter     = useSelector(state => state.NewsletterReducer)
  const viewNewsletter = newsletter.viewNewsletter;
  if(viewNewsletter) {
    var templatename   = viewNewsletter.templatename
    var emailsubject   = viewNewsletter.emailsubject
    var emailcontent   = viewNewsletter.emailcontent
    var status         = viewNewsletter.status
  }
  window.$('#compose-textarea').summernote('code',emailcontent);
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Newsletter</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Newsletter</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Newsletter</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Template Name</label>
                    <input type="text" className="form-control" name="name" value={templatename}disabled placeholder="Template name"/>
                  </div>
                  <div className="form-group">
                     <label>Email Content</label>
                     <textarea type="textarea" id="compose-textarea" disabled name="description" className="form-control" style={{height: "200px"}}/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Email Subject</label>
                    <input type="text" className="form-control" name="code" value={emailsubject} disabled placeholder="Enter Language code"/>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewNewsletter;
