import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_ADD_NEWSLETTER } from '../../actions/newsletter';

const AddNewsletter = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    templatename            : "",
    emailsubject            : "",
    emailcontent            : '',
    status                  : "1",
    templatenameError       : false,
    emailcontentError       : false,
    emailsubjectError       : false,
  });
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    setValues({...values,[name]: value,[error]:false});
  };
  useEffect(()=>{
    window.$(function () {
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const validateForm = () => {
     const data            = {name:false,subject:false,content:false};
     const emailcontent    =  window.$('#compose-textarea').summernote('code');
     if(!values.templatename){
       data.name = true;
     }
     if(!values.emailsubject){
       data.subject = true;
     }
     if(emailcontent=="<p><br></p>"){
       data.content = true;
     }
     setValues({...values,templatenameError:data.name,emailcontentError:data.content,emailsubjectError:data.subject});
     if(values.templatename&&values.emailsubject&&emailcontent!="<p><br></p>"){
        var formData = {
          id             : "0",
          templatename   : values.templatename,
          emailsubject   : values.emailsubject,
          emailcontent   : emailcontent,
          status         : values.status,
      }
      dispatch(AC_ADD_NEWSLETTER(formData));
      setValues({...values,status:"1",emailsubject:"",templatename:"",emailcontentError:false});
      window.$('#compose-textarea').summernote('reset');
   }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Add Newsletter</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Add Newsletter</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Add Newsletter</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Template Name</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="templatename" value={values.templatename}onChange={onChangeValue}placeholder="Enter Template name"/>
                      {values.templatenameError ? <label style={{color:"red"}}>Template Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Email Content</label><span style={{color:"red"}}>*</span>
                      <textarea type="textarea" id="compose-textarea"  className="form-control" name="emailcontent"/>
                      {values.emailcontentError ? <label style={{color:"red"}}>Email Content is required</label> : ""}
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Email Subject</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="emailsubject" value={values.emailsubject} onChange={onChangeValue} placeholder="Enter Email Subject"/>
                      {values.emailsubjectError ? <label style={{color:"red"}}>Email Subject is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="1" selected={values.status=="1"}>Active</option>
                        <option value="0" selected={values.status=="0"}>Inactive</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddNewsletter;
