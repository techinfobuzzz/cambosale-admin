import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import API from '../../common/api';
import { AC_LIST_BLOGCATEGORY, AC_VIEW_BLOG } from '../../actions/blog';

const ViewBlog = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_LIST_BLOGCATEGORY());
    dispatch(AC_VIEW_BLOG({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
      window.$('#compose-textarea').summernote('disable');
    })
  },[])
  const BlogReducer = useSelector(state => state.BlogReducer)
  const viewBlog = BlogReducer.viewBlog;
  if(viewBlog) {
    var title          = viewBlog.title
    var metakeyword          = viewBlog.metakeyword
    var metadescription          = viewBlog.metadescription
    var blogcategory          = viewBlog.blogcategory
    var image         = API.LIVEURL+"uploads/"+viewBlog.image
    var content   = viewBlog.content
    var status        = viewBlog.status
  }
  window.$('#compose-textarea').summernote('code',content);
  const blogcategoriesList = BlogReducer.blogcategoriesList;

  const blogCategoriesArray = [];
  for(var i=0;i<blogcategoriesList.length;i++) {
    blogCategoriesArray.push(
      <option key={i} value={blogcategoriesList[i]._id} selected={blogcategory==blogcategoriesList[i]._id}> {blogcategoriesList[i].title}</option>
    )
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Blog</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Blog</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Blog</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                      <label>Blog Category</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="blogcategory" disabled>
                        <option value="" selected={blogcategory==""}>Select</option>
                        {blogCategoriesArray}
                      </select>
                  </div>
                  <div className="form-group">
                      <label>Meta keyword</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" disabled name="metakeyword" value={metakeyword} placeholder="Enter Blog name"/>
                  </div>
                  
                  <div className="form-group">
                     <label>Content</label>
                     <textarea type="textarea" id="compose-textarea" disabled name="content" className="form-control" style={{height: "200px"}}/>
                  </div>
                </div>
                <div className="col-md-6">
                <div className="form-group">
                    <label>Title</label>
                    <input type="text" className="form-control" name="name" value={title}disabled placeholder="Blog name"/>
                  </div>
                  <div className="form-group">
                      <label>Meta description</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" disabled name="metadescription" value={metadescription} placeholder="Enter Meta description"/>
                    </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Image</label>
                    <div>
                       <img src={image} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewBlog;
