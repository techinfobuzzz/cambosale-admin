import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import API from '../../common/api';
import { AC_VIEW_BLOGCATEGORY } from '../../actions/blog';

const ViewBlogCategory = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_BLOGCATEGORY({id:id}));
  },[])
  const BlogReducer = useSelector(state => state.BlogReducer)
  const viewBlogcategory = BlogReducer.viewBlogcategory;
  var image         = API.LOCALURL+"dist/img/image.png"
  if(viewBlogcategory) {
    var title          = viewBlogcategory.title
    if(viewBlogcategory.image){
      image         = API.LIVEURL+"uploads/"+viewBlogcategory.image
    }
    var status        = viewBlogcategory.status
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Blog Category</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Blog Category</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Blog Category</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Title</label>
                    <input type="text" className="form-control" name="name" value={title}disabled placeholder="Brand name"/>
                  </div>
                  <div className="form-group">
                    <label>Image</label>
                    <div>
                       <img src={image} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewBlogCategory;
