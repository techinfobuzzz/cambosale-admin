import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_EDIT_BLOGCATEGORY, AC_VIEW_BLOGCATEGORY,AC_HANDLE_CHANGE } from '../../actions/blog';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const EditBlogCategory = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_BLOGCATEGORY({id:id}));
  },[])

  const [values, setValues] = useState({
    titleError           : false,
    imgSrc              : '',
    imageError          : false,
    imagecheck          : false,
  });

  const BlogReducer = useSelector(state => state.BlogReducer)
  const viewBlogcategory = BlogReducer.viewBlogcategory;
  var image         = API.LOCALURL+"dist/img/image.png"
  if(viewBlogcategory) {
    var title          = viewBlogcategory.title
    var status        = viewBlogcategory.status
    if(viewBlogcategory.image){
      image         = API.LIVEURL+"uploads/"+viewBlogcategory.image
    }
  }
  const validateForm = () => {
    const data          = BlogReducer.viewBlogcategory;
    const stateData     = {title:false,image:false};
    const id            = props.match.params.id;
    if(!data.title){
       stateData.title = true;
     }
     setValues({...values,titleError:stateData.title,imageError:stateData.image});
     if(data.title&&!values.imagecheck){
       const formData = new FormData();
       formData.append('id', id);
       formData.append('title', data.title);
       formData.append('status', data.status);
       formData.append('image', data.image);
       dispatch(AC_EDIT_BLOGCATEGORY(formData));
   }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     if(name=="image"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file,"Category"));
             setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false});
         }.bind(this);
         } else {
           setValues({...values,imgSrc: "",imagecheck:true,image:file,imageError:false});
         }
       }
     } else {
       dispatch(AC_HANDLE_CHANGE(name,value,"Category"));
       setValues({...values,[error]:false});
     }
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div classbrand="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Blog Category</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Blog Category</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Blog Category</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Title</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="title" value={title}onChange={onChangeValue}placeholder="Enter Blog title"/>
                    {values.titleError ? <label style={{color:"red"}}>Title is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Image</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                    {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.imgSrc?<img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={image} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditBlogCategory;
