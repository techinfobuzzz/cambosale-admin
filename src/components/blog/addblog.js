import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { Imagevalidation } from '../../common/validate';
import { AC_ADD_BLOG, AC_LIST_BLOGCATEGORY } from '../../actions/blog';

const initialState = {
  blogcategory             : "",
  title             : "",
  metakeyword      : '',
  metadescription            : '',
  content           : '',
  feature           : '',
  image            : '',
  imgSrc           : 'dist/img/image.png',
  status           : "1",
  titleError        : false,
  blogcategoryError             : false,
  metakeywordError      : false,
  metadescriptionError            : false,
  contentError           : false,
  imageError       : false,
  imagecheck       : false,
}
const AddBlog = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState(initialState);

  useEffect(()=>{
    dispatch(AC_LIST_BLOGCATEGORY());
  },[])
  const BlogReducer = useSelector(state => state.BlogReducer)
  const blogcategoriesList = BlogReducer.blogcategoriesList;

  const blogCategoriesArray = [];
  for(var i=0;i<blogcategoriesList.length;i++) {
    blogCategoriesArray.push(
      <option key={i} value={blogcategoriesList[i]._id} selected={values.blogcategory==blogcategoriesList[i]._id}> {blogcategoriesList[i].title}</option>
    )
  }

  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    if(name=="image"){
      var file            =  e.target.files[0];
      if(file){
        const imgvalidate   =  Imagevalidation(file);
        if(imgvalidate) {
          var reader          = new FileReader();
          var url             = reader.readAsDataURL(file);
          reader.onloadend    = function(e){
            setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false,image:file});
        }.bind(this);
        } else {
          setValues({...values,imgSrc: "dist/img/image.png",imagecheck:true,image:file,imageError:false});
        }
      }
    } else {
      setValues({...values,[name]: value,[error]:false});
    }
  };

  useEffect(()=>{
    window.$(function () {
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const validateForm = () => {
     const data = {title:false,blogcategory:false,metadescription:false,metakeyword:false,content:false,image:false};
     const content    =  window.$('#compose-textarea').summernote('code');
     var check=1;
     if(!values.title){
       data.title = true;
       check=0;
     }
     if(content=="<p><br></p>"){
       data.content = true;
       check=0;
     }
     if(!values.image){
       data.image = true;
       check=0;
     }
     if(!values.blogcategory){
      data.blogcategory = true;
      check=0;
    }
    if(!values.metadescription){
      data.metadescription = true;
      check=0;
    }
    if(!values.metakeyword){
      data.metakeyword = true;
      check=0;
    }
     setValues({...values,metakeywordError:data.metakeyword,metadescriptionError:data.metadescription,blogcategoryError:data.blogcategory,titleError:data.title,contentError:data.content,imageError:data.image});
     if(check&&!values.imagecheck){
       const formData = new FormData();
       formData.append('id', "0");
       formData.append('title', values.title);
       formData.append('content', content);
       formData.append('status', values.status);
       formData.append('blogcategory', values.blogcategory);
       formData.append('metadescription', values.metadescription);
       formData.append('metakeyword', values.metakeyword);
       formData.append('image', values.image);
       formData.append('feature', values.feature);
       dispatch(AC_ADD_BLOG(formData));
       setValues({...initialState});
       window.$('#compose-textarea').summernote('reset');
   }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Add Blog</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Add Blog</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Add Blog</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Blog Category</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="blogcategory" onChange={onChangeValue}>
                        <option value="" selected={values.blogcategory==""}>Select</option>
                        {blogCategoriesArray}
                      </select>
                      {values.blogcategoryError ? <label style={{color:"red"}}>Blog Category is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Meta keyword</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="metakeyword" value={values.metakeyword}onChange={onChangeValue} placeholder="Enter Meta Keyword"/>
                      {values.metakeywordError ? <label style={{color:"red"}}>Meta keyword is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Content</label><span style={{color:"red"}}>*</span>
                      <textarea type="textarea" id="compose-textarea"  className="form-control" name="content" value={values.description} onChange={onChangeValue}placeholder="Enter Language name"/>
                      {values.contentError ? <label style={{color:"red"}}>Content is required</label> : ""}
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Title</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="title" value={values.title}onChange={onChangeValue}placeholder="Enter Blog title"/>
                      {values.titleError ? <label style={{color:"red"}}>Title is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Meta description</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="metadescription" value={values.metadescription} onChange={onChangeValue}placeholder="Enter Meta description"/>
                      {values.metadescriptionError ? <label style={{color:"red"}}>Meta description is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Feature</label>
                      <select className="form-control" name="feature" onChange={onChangeValue}>
                        <option value="" selected={values.feature==""}>Select</option>
                        <option value="trending" selected={values.feature=="trending"}>Trending</option>
                        <option value="popular" selected={values.feature=="popular"}>Popular</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="1" selected={values.status=="1"}>Active</option>
                        <option value="0" selected={values.status=="0"}>Inactive</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Image</label><span style={{color:"red"}}>*</span>
                      <div className="input-group">
                         <div className="custom-file">
                             <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                             <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                         </div>
                         <div className="input-group-append">
                             <button className="input-group-text" id="">Upload</button>
                         </div>
                      </div>
                      {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                      {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                      <div>
                         <img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                       </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddBlog;
