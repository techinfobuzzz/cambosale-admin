import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_EDIT_BLOG, AC_LIST_BLOGCATEGORY, AC_VIEW_BLOG,AC_HANDLE_CHANGE } from '../../actions/blog';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const EditBlog = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_LIST_BLOGCATEGORY());
    dispatch(AC_VIEW_BLOG({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const [values, setValues] = useState({
    imgSrc              : '',
    imageError          : false,
    imagecheck          : false,
    titleError        : false,
    blogcategoryError             : false,
    metakeywordError      : false,
    metadescriptionError            : false,
    contentError           : false,
  });

  const BlogReducer = useSelector(state => state.BlogReducer)
  const viewBlog = BlogReducer.viewBlog;
  if(viewBlog) {
    var title          = viewBlog.title
    var metakeyword          = viewBlog.metakeyword
    var metadescription          = viewBlog.metadescription
    var blogcategory          = viewBlog.blogcategory
    var image         = API.LIVEURL+"uploads/"+viewBlog.image
    var content   = viewBlog.content
    var status        = viewBlog.status
    var feature       = viewBlog.feature
  }
  window.$('#compose-textarea').summernote('code',content);
  const blogcategoriesList = BlogReducer.blogcategoriesList;

  const blogCategoriesArray = [];
  for(var i=0;i<blogcategoriesList.length;i++) {
    blogCategoriesArray.push(
      <option key={i} value={blogcategoriesList[i]._id} selected={blogcategory==blogcategoriesList[i]._id}> {blogcategoriesList[i].title}</option>
    )
  }

  const validateForm = () => {
    const data = {title:false,blogcategory:false,metadescription:false,metakeyword:false,content:false,image:false};
    const content    =  window.$('#compose-textarea').summernote('code');
    const id            = props.match.params.id;
    var check=1;
    if(!viewBlog.title){
      data.title = true;
      check=0;
    }
    if(content=="<p><br></p>"){
      data.content = true;
      check=0;
    }
    if(!viewBlog.image){
      data.image = true;
      check=0;
    }
    if(!viewBlog.blogcategory){
     data.blogcategory = true;
     check=0;
   }
   if(!viewBlog.metadescription){
     data.metadescription = true;
     check=0;
   }
   if(!viewBlog.metakeyword){
     data.metakeyword = true;
     check=0;
   }
    setValues({...values,metakeywordError:data.metakeyword,metadescriptionError:data.metadescription,blogcategoryError:data.blogcategory,titleError:data.title,contentError:data.content,imageError:data.image});
    if(check&&!values.imagecheck){
      const formData = new FormData();
      formData.append('id', id);
      formData.append('title', viewBlog.title);
      formData.append('content', content);
      formData.append('status', viewBlog.status);
      formData.append('blogcategory', viewBlog.blogcategory);
      formData.append('metadescription', viewBlog.metadescription);
      formData.append('metakeyword', viewBlog.metakeyword);
      formData.append('image', viewBlog.image);
      formData.append('feature', viewBlog.feature);
      dispatch(AC_EDIT_BLOG(formData));
  }
 }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     if(name=="image"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file,"Blog"));
             setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false});
         }.bind(this);
         } else {
           setValues({...values,imgSrc: "",imagecheck:true,image:file,imageError:false});
         }
       }
     } else {
       dispatch(AC_HANDLE_CHANGE(name,value,"Blog"));
       setValues({...values,[error]:false});
     }
     dispatch(AC_HANDLE_CHANGE("content",window.$('#compose-textarea').summernote('code'),"Blog"));
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Blog</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Blog</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Blog</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                    <div className="form-group">
                      <label>Blog Category</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="blogcategory" onChange={onChangeValue}>
                        <option value="" selected={values.blogcategory==""}>Select</option>
                        {blogCategoriesArray}
                      </select>
                      {values.blogcategoryError ? <label style={{color:"red"}}>Blog Category is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Meta keyword</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="metakeyword" value={metakeyword}onChange={onChangeValue}placeholder="Enter Blog name"/>
                      {values.metakeywordError ? <label style={{color:"red"}}>Meta keyword is required</label> : ""}
                    </div>
                  <div className="form-group">
                     <label>Content</label>
                     <textarea type="textarea" id="compose-textarea" name="content" className="form-control" style={{height: "200px"}}/>
                     {values.contentError?<label style={{color:"red"}}>Content is Required</label>:""}
                  </div>
                </div>
                <div className="col-md-6">
                    <div className="form-group">
                      <label>Title</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="title" value={title}onChange={onChangeValue}placeholder="Enter Blog name"/>
                      {values.titleError ? <label style={{color:"red"}}>Title is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Meta description</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="metadescription" value={metadescription} onChange={onChangeValue}placeholder="Enter Blog name"/>
                      {values.metadescriptionError ? <label style={{color:"red"}}>Meta description is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Feature</label>
                      <select className="form-control" name="feature" onChange={onChangeValue}>
                        <option value="" selected={feature==""}>Select</option>
                        <option value="trending" selected={feature=="trending"}>Trending</option>
                        <option value="popular" selected={feature=="popular"}>Popular</option>
                      </select>
                    </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Image</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                    {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.imgSrc?<img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={image} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditBlog;
