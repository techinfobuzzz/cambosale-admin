import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_DISTRICT,AC_HANDLE_CHANGE,AC_EDIT_DISTRICT, AC_ADDEDIT_CITY, AC_DELETE_CITY} from '../../actions/district';
import swal from 'sweetalert';

const EditDistrict = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_DISTRICT({id:id}));
  },[])

  const [values, setValues] = useState({
    districtError    : false,
    city             : "",
    status           : "1",
    cityError        : false,
    selectedId       : ""
  });

  const districtReducer = useSelector(state => state.DistrictReducer)
  const viewDistrict = districtReducer.viewDistrict;
  const citiesList = districtReducer.citiesList;
  if(viewDistrict) {
    var state       = viewDistrict.state
    var district   = viewDistrict.district
    var status     = viewDistrict.status
  }
  if(district){
    var slug = district;
    slug = slug.replace(/[^\w\-]+/g, "-");
    slug = slug.toLowerCase();
  }
  const validateForm = () => {
    const data      = viewDistrict;
    const stateData = {district:false};
    const id = props.match.params.id;
    if(!data.district){
      stateData.district = true;
    }
    setValues({...values,districtError:stateData.district});
     if(data.district){
        var formData = {
          id         : id,
          state      : data.state,
          district   : data.district,
          slug       : slug,
          status     : data.status,
        }
        dispatch(AC_EDIT_DISTRICT(formData));
     }
  }
  var cityslug = values.city;
  cityslug = cityslug.replace(/[^\w\-]+/g, "-");
  cityslug = cityslug.toLowerCase();

  const addCity = () => {
    const stateData = {city:false};
    const id = props.match.params.id;
    if(!values.city){
      stateData.city = true;
    }
    setValues({...values,cityError:stateData.city});
    if(values.city){
      if(values.selectedId){
        var formData = {
          id         : values.selectedId,
          city       : values.city,
          districtId : id,
          slug       : cityslug,
          status     : values.status,
        }
        dispatch(AC_ADDEDIT_CITY(formData));
        setValues({...values,cityError:false,selectedId:"",city:"",status:"1"});
       } else {
          var formData = {
            id         : "0",
            city       : values.city,
            districtId : id,
            slug       : cityslug,
            status     : values.status,
          }
          dispatch(AC_ADDEDIT_CITY(formData));
          setValues({...values,cityError:false,city:"",status:"1"});
      }
    }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     dispatch(AC_HANDLE_CHANGE(name,value));
     setValues({...values,[error]:false});
  }

  const cityOnChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    setValues({...values,[name]: value,[error]:false});
  };

  const setCityValue= (e)=>{
    const id              = e.target.id;
    const selecteddetail  = citiesList.filter((details)=>{
      return  details._id == id;
    });
    if(selecteddetail[0]){
          setValues({...values,city:selecteddetail[0].city,selectedId:selecteddetail[0]._id});
    }
  }
  const deleteCity= (e)=>{
    const id              = e.target.id;
    const districtId      = props.match.params.id;
    swal({
      // title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        dispatch(AC_DELETE_CITY({id:id,districtId:districtId}));
      }
});
  }
  var citiesArray    =[];
      for(var i = 0; i < citiesList.length; i++)  {
        citiesArray.push(
          <tr key={i} >
            <td >
              {citiesList[i].city}
            </td >
            <td >
             {citiesList[i].slug}
            </td>
            <td >
            {citiesList[i].status?<span class="badge badge-success" data-action-type="Status">Active</span>:
            <span class="badge badge-danger" data-action-type="Status">Inactive</span>}
            </td>
            <td >
            <a href="javascript:void(0)" className="btn btn-primary" id={citiesList[i]._id} onClick={setCityValue}><i className="fas fa-pencil-alt" id={citiesList[i]._id}></i></a>
            <a href="javascript:void(0)" className="btn btn-danger" id={citiesList[i]._id}  onClick={deleteCity}><i className="fas fa-trash" id={citiesList[i]._id}></i></a>
            </td>
          </tr>
        )
       }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit District</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit District</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit District</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                      <label>State</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="state" value={state} disabled placeholder="Enter State"/>
                  </div>
                  <div className="form-group">
                    <label>Slug</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="slug" value={slug}onChange={onChangeValue}placeholder="Enter Slug"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>District</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="district" value={district} onChange={onChangeValue} placeholder="Enter District"/>
                    {values.symbolError ? <label style={{color:"red"}}>Code is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
      <section className="content-header">
        <div className="container-fluid">
          <section className="content">
            <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-header" style={{backgroundColor:"#007bff"}}>
                    <h3 className="card-title" style={{color:"white"}}>Cities</h3>
                    <div className="card-tools">
                      <div className="input-group input-group-sm">
                        <div className="input-group-append">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-body table-responsive p-0" style={{display: true?"block":"none"}}>
                    <table className="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>City</th>
                          <th>Slug</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <input type="text" className="form-control" name="city" placeholder="City" value={values.city} onChange={cityOnChangeValue}/>
                            {values.cityError?<span style={{color:"red"}}>City is required</span>:""}
                          </td>
                          <td>
                            <input type="text" className="form-control" name="slug" placeholder="Slug" value={cityslug} onChange={cityOnChangeValue}/>
                          </td>
                          <td>
                          <select className="form-control" name="status" onChange={cityOnChangeValue}>
                            <option value="1" selected={values.status==true}>Active</option>
                            <option value="0" selected={values.status==false}>Inactive</option>
                          </select>
                          </td>
                          <td className="text-right py-0 align-middle">
                            <div className="btn-group btn-group-sm">
                              <a href="javascript:void(0)" className="btn btn-primary" onClick={addCity}><i className="fas fa-plus"></i></a>
                            </div>
                          </td>
                        </tr>
                        {citiesArray}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  )
}

export default EditDistrict;
