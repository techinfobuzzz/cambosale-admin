import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_DISTRICT,} from '../../actions/district';
import swal from 'sweetalert';

const ViewDistrict = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_DISTRICT({id:id}));
  },[])

  const districtReducer = useSelector(state => state.DistrictReducer)
  const viewDistrict = districtReducer.viewDistrict;
  const citiesList = districtReducer.citiesList;
  if(viewDistrict) {
    var state       = viewDistrict.state
    var district   = viewDistrict.district
    var status     = viewDistrict.status
    var slug       = viewDistrict.slug
  }

  var citiesArray    =[];
      for(var i = 0; i < citiesList.length; i++)  {
        citiesArray.push(
          <tr key={i} >
            <td >
              {citiesList[i].city}
            </td >
            <td >
             {citiesList[i].slug}
            </td>
            <td >
            {citiesList[i].status?<span class="badge badge-success" data-action-type="Status">Active</span>:
            <span class="badge badge-danger" data-action-type="Status">Inactive</span>}
            </td>
          </tr>
        )
       }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View District</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View District</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View District</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                      <label>State</label> 
                      <input type="text" className="form-control" name="state" disabled value={state} disabled placeholder="Enter State"/>
                  </div>
                  <div className="form-group">
                    <label>Slug</label> 
                    <input type="text" className="form-control" name="slug" disabled value={slug}  placeholder="Enter Language name"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>District</label> 
                    <input type="text" className="form-control" name="district" disabled value={district}   placeholder="Enter Language Code"/>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" disabled  >
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="content-header" style={{display: citiesList.length?"block":"none"}}>
        <div className="container-fluid">
          <section className="content">
            <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body table-responsive p-0" >
                    <table className="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>City</th>
                          <th>Slug</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        {citiesArray}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  )
}

export default ViewDistrict;
