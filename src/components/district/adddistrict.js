import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_ADD_DISTRICT } from '../../actions/district';

const AddDistrict = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    state            : "TamilNadu",
    district         : "",
    status           : "1",
    districtError    : false,
  });
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    setValues({...values,[name]: value,[error]:false});
  };
  var slug = values.district;
  slug = slug.replace(/[^\w\-]+/g, "-");
  slug = slug.toLowerCase();

  const validateForm = () => {
     const data = {district:false};
     if(!values.district){
       data.district = true;
     }
     setValues({...values,districtError:data.district});
     if(values.district){
        var formData = {
          id         : "0",
          state      : values.state,
          district   : values.district,
          slug       : slug,
          status     : values.status,
      }
      dispatch(AC_ADD_DISTRICT(formData));
      setValues({...values,status:"1",district:""});
   }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Add District</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Add District</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Add District</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>State</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="state" value={values.state} disabled placeholder="Enter State"/>
                    </div>
                    <div className="form-group">
                      <label>Slug</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="slug" value={slug} onChange={onChangeValue} placeholder="Slug"/>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>District</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="district" value={values.district} onChange={onChangeValue} placeholder="Enter District"/>
                      {values.districtError ? <label style={{color:"red"}}>District is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="1" selected={values.status=="1"}>Active</option>
                        <option value="0" selected={values.status=="0"}>Inactive</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddDistrict;