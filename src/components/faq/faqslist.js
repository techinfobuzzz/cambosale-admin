import React,{ useState, useEffect } from 'react';
import { Link ,Redirect} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AC_LIST_FAQS, AC_DELETE_FAQ } from '../../actions/faq';
import { COLDEF } from '../../common/gridoptions';
import swal from 'sweetalert';

const FaqsList = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(AC_LIST_FAQS());
  },[])

  const [value, setValue] = useState({
    columnDefs :[
      { "headerName" : "Sl No",field:"#",valueGetter :"node.rowIndex+1",sortable:false,
      },
      { "headerName" : "Question",field:"question",
     },
      { "headerName" : "Answer", field :"answer",
     },
       { "header" : "Status", field :"status",sortable:false,filter: false ,
         cellRenderer: function(params) {
          if(params.data.status) {
            return '<span class="badge badge-success" data-action-type="Status">Active</span>';
          } else {
            return '<span class="badge badge-success" style="background-color: rgb(220, 53, 69);" data-action-type="Status">Inactive</span>';
          }
        }},
      { headerName: 'Actions', width: "300",sortable: false,filter:false,
        template:
           `<div>
           <div class="btn bg-info btn-xs" href='#' style={{marginRight:"10px",height:"5px",width:"5px"}} data-action-type="View">
           <i class="fas fa-folder"  style={{padding: "10px"}} data-action-type="View"></i>
           View
           </div>
           <div class="btn bg-primary btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Edit">
           <i class="fas fa-pencil-alt"  style={{padding: "10px"}} data-action-type="Edit"></i>
           Edit
           </div>
           <div class="btn bg-danger btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Delete">
           <i class="fas fa-trash"  style={{padding: "10px"}} data-action-type="Delete"></i>
           Delete
           </div>
           </div>`,

       },
   ],
   defaultColDef: COLDEF,
  });
    const onSortChanged = e => {
     e.api.refreshCells();
    }
   const onRowClicked = event =>{
    const rowValue = event.event.target;
    const value    = rowValue.getAttribute('data-action-type');
    if(value === 'View'){
      setValue({...value,redirect:'View',id:event.data._id})
    }
    if(value === 'Edit'){
      setValue({...value,redirect:'Edit',id:event.data._id})
    }
    if(value === 'Delete'){
      swal({
           // title: "Are you sure?",
           text: "Once deleted, you will not be able to recover this data!",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             dispatch(AC_DELETE_FAQ({id:event.data._id}));
           }
     });
    }
  }

  const faq = useSelector(state => state.FaqReducer)
  const faqsList = faq.faqsList;

  if(value.redirect === 'View'){
    return <Redirect to ={'/view-faq/'+value.id}/>
  }
  if(value.redirect === 'Edit'){
    return <Redirect to ={'/edit-faq/'+value.id}/>
  }
  return(
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>FAQs List</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">FAQs List</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <div className="ag-theme-balham" >
      <AgGridReact
          rowHeight={40}
          rowClass ={'centerAlign'}
          onSortChanged={onSortChanged}
          rowSelection={'single'}
          onRowClicked={onRowClicked}
          suppressCellSelection={true}
          pagination={true}
          domLayout={"autoHeight"}
          defaultColDef={value.defaultColDef}
          columnDefs={value.columnDefs}
          rowData={faqsList}>
      </AgGridReact>
      </div>
    </div>
  )
}

export default FaqsList;
