import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_FAQ,AC_HANDLE_CHANGE,AC_EDIT_FAQ} from '../../actions/faq';

const EditFaq = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_FAQ({id:id}));
  },[])

  const [values, setValues] = useState({
    questionError    : false,
    answerError      : false,
  });

  const faq = useSelector(state => state.FaqReducer)
  const viewFaq = faq.viewFaq;
  if(viewFaq) {
    var question = viewFaq.question
    var answer   = viewFaq.answer
    var status   = viewFaq.status
  }

  const validateForm = () => {
    const data      = faq.viewFaq;
    const stateData = {question:false,answer:false};
    const id = props.match.params.id;
    if(!data.question){
       stateData.question = true;
     }
    if(!data.answer){
       stateData.answer = true;
     }
     setValues({...values,questionError:stateData.question,answerError:stateData.answer});
     if(data.question&&data.answer){
        var formData = {
          id         : id,
          question   : data.question,
          answer     : data.answer,
          status     : data.status,
        }
        dispatch(AC_EDIT_FAQ(formData));
     }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     dispatch(AC_HANDLE_CHANGE(name,value));
     setValues({...values,[error]:false});
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit FAQ</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit FAQ</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit FAQ</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Question</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="question" value={question}onChange={onChangeValue}placeholder="Question"/>
                    {values.questionError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Answer</label><span style={{color:"red"}}>*</span>
                    <textarea type="text" className="form-control" style={{height:"120px"}}name="answer" value={answer} onChange={onChangeValue} placeholder="Answer"/>
                    {values.answerError ? <label style={{color:"red"}}>Answer is required</label> : ""}
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditFaq;
