import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_FAQ} from '../../actions/faq';

const ViewFaq = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_FAQ({id:id}));
  },[])
  const faq = useSelector(state => state.FaqReducer)
  const viewFaq = faq.viewFaq;
  if(viewFaq) {
    var question = viewFaq.question
    var answer   = viewFaq.answer
    var status   = viewFaq.status
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View FAQ</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View FAQ</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View FAQ</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Question</label>
                    <input type="text" className="form-control" name="question" value={question}disabled placeholder="Question"/>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Answer</label>
                    <textarea type="text" className="form-control" style={{height:"120px"}} name="answer" value={answer} disabled placeholder="Answer "/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewFaq;
