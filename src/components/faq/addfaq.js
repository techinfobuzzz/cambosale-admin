import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_ADD_FAQ } from '../../actions/faq';

const AddFaq = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    question         : "",
    answer           : "",
    status           : "1",
    questionError    : false,
    answerError      : false,
  });
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    setValues({...values,[name]: value,[error]:false});
  };

  const validateForm = () => {
     const data = {question:false,answer:false};
     if(!values.question){
       data.question = true;
     }
     if(!values.answer){
       data.answer = true;
     }
     setValues({...values,questionError:data.question,answerError:data.answer});
     if(values.question&&values.answer){
        var formData = {
          id         :"0",
          question   : values.question,
          answer     : values.answer,
          status     : values.status,
      }
      dispatch(AC_ADD_FAQ(formData));
      setValues({...values,status:"1",question:"",answer:""});
   }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Add FAQ</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Add FAQ</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Add FAQ</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Question</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="question" value={values.question}onChange={onChangeValue}placeholder="Enter Question"/>
                      {values.questionError ? <label style={{color:"red"}}>Question is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="1" selected={values.status=="1"}>Active</option>
                        <option value="0" selected={values.status=="0"}>Inactive</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Answer</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" style={{height:"120px"}} name="answer" value={values.answer} onChange={onChangeValue} placeholder="Enter Answer"/>
                      {values.answerError ? <label style={{color:"red"}}>Answer is required</label> : ""}
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddFaq;
