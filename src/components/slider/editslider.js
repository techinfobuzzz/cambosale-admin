import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_SLIDER,AC_HANDLE_CHANGE,AC_EDIT_SLIDER} from '../../actions/slider';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const EditSlider = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SLIDER({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const [values, setValues] = useState({
    nameError           : false,
    descriptionError    : false,
    imgSrc              : '',
    imageError          : false,
    imagecheck          : false,
  });

  const slider = useSelector(state => state.SliderReducer)
  const viewSlider = slider.viewSlider;
  if(viewSlider) {
    var name          = viewSlider.name
    var description   = viewSlider.description
    var status        = viewSlider.status
    var image         = API.LIVEURL+"uploads/"+viewSlider.image
    window.$('#compose-textarea').summernote('code',description);
  }
  const validateForm = () => {
    const data          = slider.viewSlider;
    const stateData     = {name:false,description:false,image:false};
    const description   = window.$('#compose-textarea').summernote('code');
    const id            = props.match.params.id;
    if(!data.name){
       stateData.name = true;
     }
    if(description=="<p><br></p>"){
       stateData.description = true;
     }
     if(!data.image){
       stateData.image = true;
     }
     setValues({...values,nameError:stateData.name,descriptionError:stateData.description,imageError:stateData.image});
     if(data.name&&description!="<p><br></p>"&&data.image&&!values.imagecheck){
       const formData = new FormData();
       formData.append('id', id);
       formData.append('name', data.name);
       formData.append('description', description);
       formData.append('status', data.status);
       formData.append('image', data.image);
       dispatch(AC_EDIT_SLIDER(formData));
   }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     if(name=="image"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file));
             setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false});
         }.bind(this);
         } else {
           setValues({...values,imgSrc: "",imagecheck:true,image:file,imageError:false});
         }
       }
     } else {
       dispatch(AC_HANDLE_CHANGE(name,value));
       setValues({...values,[error]:false});
     }
     dispatch(AC_HANDLE_CHANGE("description",window.$('#compose-textarea').summernote('code')));
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Slider</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Slider</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Slider</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={name}onChange={onChangeValue}placeholder="Enter Slider name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                     <label>Description</label>
                     <textarea type="textarea" id="compose-textarea" name="description" className="form-control" style={{height: "200px"}}/>
                     {values.descriptionError?<label style={{color:"red"}}>Description is Required</label>:""}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Image</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                    {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.imgSrc?<img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={image} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditSlider;
