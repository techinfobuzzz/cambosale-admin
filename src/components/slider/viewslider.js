import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_SLIDER} from '../../actions/slider';
import API from '../../common/api';

const ViewSlider = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SLIDER({id:id}));
    window.$(function (){
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
      window.$('#compose-textarea').summernote('disable');
    })
  },[])
  const slider = useSelector(state => state.SliderReducer)
  const viewSlider = slider.viewSlider;
  if(viewSlider) {
    var name          = viewSlider.name
    var image         = API.LIVEURL+"uploads/"+viewSlider.image
    var description   = viewSlider.description
    var status        = viewSlider.status
  }
  window.$('#compose-textarea').summernote('code',description);
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Slider</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Slider</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Slider</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" value={name}disabled placeholder="Slider name"/>
                  </div>
                  <div className="form-group">
                     <label>Description</label>
                     <textarea type="textarea" id="compose-textarea" disabled name="description" className="form-control" style={{height: "200px"}}/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Image</label>
                    <div>
                       <img src={image} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewSlider;
