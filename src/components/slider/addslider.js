import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_ADD_SLIDER } from '../../actions/slider';
import { Imagevalidation } from '../../common/validate';

const AddSlider = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    name             : "",
    description      : '',
    image            : '',
    imgSrc           : 'dist/img/image.png',
    status           : "1",
    nameError        : false,
    descriptionError : false,
    imageError       : false,
    imagecheck       : false,
  });

  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    if(name=="image"){
      var file            =  e.target.files[0];
      if(file){
        const imgvalidate   =  Imagevalidation(file);
        if(imgvalidate) {
          var reader          = new FileReader();
          var url             = reader.readAsDataURL(file);
          reader.onloadend    = function(e){
            setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false,image:file});
        }.bind(this);
        } else {
          setValues({...values,imgSrc: "dist/img/image.png",imagecheck:true,image:file,imageError:false});
        }
      }
    } else {
      setValues({...values,[name]: value,[error]:false});
    }
  };

  useEffect(()=>{
    window.$(function () {
      window.$('#compose-textarea').summernote({ height: 200,focus: true });
    })
  },[])

  const validateForm = () => {
     const data = {name:false,description:false,image:false};
     const description    =  window.$('#compose-textarea').summernote('code');
     if(!values.name){
       data.name = true;
     }
     if(description=="<p><br></p>"){
       data.description = true;
     }
     if(!values.image){
       data.image = true;
     }
     setValues({...values,nameError:data.name,descriptionError:data.description,imageError:data.image});
     if(values.name&&description!="<p><br></p>"&&values.image&&!values.imagecheck){
       const formData = new FormData();
       formData.append('id', "0");
       formData.append('name', values.name);
       formData.append('description', description);
       formData.append('status', values.status);
       formData.append('image', values.image);
       dispatch(AC_ADD_SLIDER(formData));
       setValues({...values,status:"1",name:"",descriptionError:false,image:"",imgSrc:"dist/img/image.png"});
       window.$('#compose-textarea').summernote('reset');
   }
  }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Add Slider</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Add Slider</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Add Slider</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Name</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="name" value={values.name}onChange={onChangeValue}placeholder="Enter Slider name"/>
                      {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Description</label><span style={{color:"red"}}>*</span>
                      <textarea type="textarea" id="compose-textarea"  className="form-control" name="description" value={values.description} onChange={onChangeValue}placeholder="Enter Language name"/>
                      {values.descriptionError ? <label style={{color:"red"}}>Description is required</label> : ""}
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Status</label>
                      <select className="form-control" name="status" onChange={onChangeValue}>
                        <option value="1" selected={values.status=="1"}>Active</option>
                        <option value="0" selected={values.status=="0"}>Inactive</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Image</label><span style={{color:"red"}}>*</span>
                      <div className="input-group">
                         <div className="custom-file">
                             <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                             <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                         </div>
                         <div className="input-group-append">
                             <button className="input-group-text" id="">Upload</button>
                         </div>
                      </div>
                      {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                      {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                      <div>
                         <img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                       </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default AddSlider;
