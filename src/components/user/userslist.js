import React,{ useState, useEffect } from 'react';
import { Link ,Redirect} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AC_LIST_USERS,AC_DELETE_USER } from '../../actions/user';
import { COLDEF } from '../../common/gridoptions';
import swal from 'sweetalert';
import API from '../../common/api';

const UsersList = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(AC_LIST_USERS());
  },[])
  const user = useSelector(state => state.UserReducer)
  const usersList = user.usersList;
  const [value, setValue] = useState({
    columnDefs :[
      { "headerName" : "Sl No",field:"#",valueGetter :"node.rowIndex+1",sortable:false
      },
      { "headerName" : "Full Name",field:"fullname",
      },
      { "headerName" : "Phonenumber",field:"phonenumber",
      },
      { "headerName" : "Email",field:"email",
      },
      { headerName: 'Actions', width: "300",sortable: false,filter:false,
        template:
           `<div>
           <div class="btn bg-info btn-xs" href='#' style={{marginRight:"10px",height:"5px",width:"5px"}} data-action-type="View">
           <i class="fas fa-folder"  data-action-type="View"></i>
           View
           </div>
           <div class="btn bg-danger btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Delete">
           <i class="fas fa-trash"  data-action-type="Delete"></i>
           Delete
           </div>
           </div>`,
       },
   ]
  });
    const onSortChanged = e => {
     e.api.refreshCells();
    }
   const onRowClicked = event =>{
    const rowValue = event.event.target;
    const value    = rowValue.getAttribute('data-action-type');
    if(value === 'View'){
      setValue({...value,redirect:'View',id:event.data._id})
    }
    if(value === 'Delete'){
      swal({
           // title: "Are you sure?",
           text: "Once deleted, you will not be able to recover this data!",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             dispatch(AC_DELETE_USER({id:event.data._id}));
           }
     });
    }
  }


  if(value.redirect === 'View'){
    return <Redirect to ={'/view-user/'+value.id}/>
  }

  return(
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Users List</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Users List</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <div className="ag-theme-balham" >
      <AgGridReact
          rowHeight={40}
          rowClass ={'centerAlign'}
          onSortChanged={onSortChanged}
          rowSelection={'single'}
          onRowClicked={onRowClicked}
          suppressCellSelection={true}
          pagination={true}
          domLayout={"autoHeight"}
          defaultColDef={COLDEF}
          columnDefs={value.columnDefs}
          rowData={usersList}>
      </AgGridReact>
      </div>
    </div>
  )
}

export default UsersList;
