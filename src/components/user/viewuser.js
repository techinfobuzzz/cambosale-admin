import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_USER } from '../../actions/user';

const ViewUser = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_USER({id:id}));
  },[])
  const user = useSelector(state => state.UserReducer)
  const viewUser = user.viewUser;
  if(viewUser) {
    var name           = viewUser.fullname
    var phonenumber    = viewUser.phonenumber
    var email          = viewUser.email
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View User</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View User</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View User</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" value={name}disabled placeholder="Name"/>
                  </div>
                  <div className="form-group">
                     <label>Email</label>
                     <input type="text" className="form-control" name="email" value={email}disabled placeholder="Email"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                     <label>Phone Number</label>
                     <input type="text" className="form-control" name="phonenumber" value={phonenumber}disabled placeholder="Phone number"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewUser;
