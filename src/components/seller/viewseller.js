import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_SELLER } from '../../actions/seller';

const ViewSeller = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SELLER({id:id}));
  },[])
  const seller = useSelector(state => state.SellerReducer)
  const viewSeller = seller.viewSeller;
  if(viewSeller) {
    var name          = viewSeller.name
    var email         = viewSeller.email
    var location      = viewSeller.location
    var contactnumber = viewSeller.contactnumber
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Seller</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Seller</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Seller</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" value={name}disabled placeholder="Name"/>
                  </div>
                  <div className="form-group">
                     <label>Location</label>
                     <input type="text" className="form-control" name="contactnumber" value={location}disabled placeholder="Contact Number"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                     <label>Email</label>
                     <input type="text" className="form-control" name="email" value={email}disabled placeholder="Email"/>
                  </div>
                  <div className="form-group">
                     <label>Contact Number</label>
                     <input type="text" className="form-control" name="contactnumber" value={contactnumber}disabled placeholder="Email"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewSeller;
