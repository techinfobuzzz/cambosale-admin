import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_SHOP } from '../../actions/shop';
import API from '../../common/api';

const ViewShop = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SHOP({id:id}));
  },[])
  const shop = useSelector(state => state.ShopReducer)
  var accountdetails = []
  const viewShop = shop.viewShop;
    if(viewShop) {
      var shopname        = viewShop.shopname
      var image           = API.LIVEURL+"uploads/"+viewShop.image
      var coverpicture    = API.LIVEURL+"uploads/"+viewShop.coverpicture
      var email           = viewShop.email
      var contactnumber   = viewShop.contactnumber
      var rangelocation   = viewShop.rangelocation
      var locationaddress = viewShop.locationaddress
      var locationlat     = viewShop.locationlat
      var locationlon     = viewShop.locationlon
      var slug            = viewShop.slug
      var distance        = viewShop.distance
      var category        = viewShop.category
      accountdetails      = viewShop.accountdetails?viewShop.accountdetails:[];
      var delivery        = viewShop.delivery
      var description     = viewShop.description
  }
  var bankdetailsArray = []
  for(var i=0;i<accountdetails.length;i++) {
    bankdetailsArray.push(
      <>
        <div className="row">
          <div className="col-md-6">
            <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Bank Name</label>
                          <input type="text" className="form-control" disabled id={i} value={accountdetails[i].bankname} name="bankname" placeholder="Enter Shop name"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Account holder Name</label>
                        <input type="text" className="form-control" disabled id={i} value={accountdetails[i].holdername} name="holdername" placeholder="Enter Shop name"/>
                      </div>
                    </div>
                  </div>
                </form>
            </div>
          </div>
          <div className="col-md-6">
              <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Account Number</label>
                        <input type="text" className="form-control" disabled id={i} value={accountdetails[i].accountnumber} name="accountnumber" placeholder="Enter Bank Account number"/>
                      </div>
                    </div>
                  </div>
                </form>
             </div>
          </div>
        </div>
        <hr/>
      </>
    )
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Shop</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Shop</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Shop</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                      <label>Shop Name</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="shopname" value={ shopname} disabled placeholder="Enter Shop name"/>
                  </div>
                  <div className="form-group">
                      <label>Range Location</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="rangelocation" value={ rangelocation} disabled placeholder="Enter Range Location"/>
                    </div>
                    <div className="form-group">
                      <label>Category</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="category" value={ category} disabled placeholder="Enter Category name"/>
                    </div>
                    <div className="form-group">
                      <label>Email</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="email" value={ email} disabled placeholder="Enter Email"/>
                    </div>
                    <div className="form-group">
                      <label>Location LAT</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="locationlat" value={ locationlat} disabled placeholder="Enter Latitude"/>
                    </div>
                    <div className="form-group">
                      <label>Location Address</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" name="locationaddress" value={ locationaddress} disabled placeholder="Enter Address"/>
                    </div>
                  <div className="form-group">
                    <label>Logo</label>
                    <div>
                       <img src={image} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                  {/* <div className="form-group">
                      <label>Payment Method</label><span style={{color:"red"}}>*</span>
                      <div class="form-check">
                          <input class="form-check-input" disabled checked={ paymentmethod.indexOf("netbanking")>-1?true:false} name="paymentmethod" type="checkbox" value="netbanking"  /><label class="form-check-label">Net banking</label>
                      </div>
                      <div class="form-check">
                          <input class="form-check-input" disabled checked={ paymentmethod.indexOf("upi")>-1?true:false} name="paymentmethod" type="checkbox" value="upi"  /><label class="form-check-label">UPI</label>
                      </div>
                      <div class="form-check">
                          <input class="form-check-input" disabled checked={ paymentmethod.indexOf("cod")>-1?true:false} name="paymentmethod" type="checkbox" value="cod"  /><label class="form-check-label">COD</label>
                      </div>
                    </div> */}
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Slug</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="slug" value={ slug} disabled placeholder="Enter Slug"/>
                  </div>
                  <div className="form-group">
                    <label>Distance</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="distance" value={ distance} disabled placeholder="Enter Distance"/>
                  </div>
                  <div className="form-group">
                    <label>Delivery</label><span style={{color:"red"}}>*</span>
                    <select className="form-control" disabled name="delivery"  >
                      <option value="yes" selected={ delivery=="yes"}>Yes</option>
                      <option value="no" selected={ delivery=="no"}>No</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Contact Number</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="contactnumber" value={ contactnumber} disabled placeholder="Enter Contact number"/>
                  </div>
                  <div className="form-group">
                    <label>Location LON</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="locationlon" value={ locationlon} disabled placeholder="Enter Longitude"/>
                  </div>
                  <div className="form-group">
                      <label>Description</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" name="description" value={ description} disabled placeholder="Enter Description"/>
                  </div>
                  <div className="form-group">
                    <label>Cover picture</label><span style={{color:"red"}}>*</span>
                    <div>
                        <img src={ coverpicture} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                      </div>
                  </div>
                  <div className="form-group">
                    <label>Cover picture</label><span style={{color:"red"}}>*</span>
                    <div>
                        <img src={ coverpicture} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                      </div>
                  </div>
                  <div className="form-group">
                      <label>Description</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" name="description" value={ description} disabled placeholder="Enter Description"/>
                    </div>
                </div>
              </div>
            </div>
            <div className="card-header" >
                <h3 className="card-title" ><b>View Account details</b></h3>
            </div>
            <div className="card-body">
              {bankdetailsArray}
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewShop;
