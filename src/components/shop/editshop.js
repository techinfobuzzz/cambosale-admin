import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_EDIT_SHOP, AC_VIEW_SHOP, AC_HANDLE_CHANGE } from '../../actions/shop';
import { Imagevalidation, Emailvalidate } from '../../common/validate';
import API from '../../common/api';

const EditShop = (props) => {
  const dispatch = useDispatch()
 
  const [values, setValues] = useState({
    imgSrc                  : '',
    coverpictureSrc         : '',
    shopnameError           : false,
    slugError               : false,
    rangelocationError      : false,
    distanceError           : false,
    categoryError           : false,
    paymentmethodError      : false,
    deliveryError           : false,
    contactnumberError      : false,
    emailError              : false,
    locationaddressError    : false,
    locationlatError        : false,
    locationlonError        : false,
    descriptionError        : false,
    bankaccountError        : false,
    imageError              : false,
    coverpictureError       : false,
    imagecheck              : false,
    coverpicturecheck       : false,
    emailcheck              : false,
    accountdetailslength    : 0,
    accountdetails          : [],
  });

  const shop = useSelector(state => state.ShopReducer)
  var accountdetails = []
  const viewShop = shop.viewShop;
    if(viewShop) {
      var shopname        = viewShop.shopname
      var image           = API.LIVEURL+"uploads/"+viewShop.image
      var coverpicture    = API.LIVEURL+"uploads/"+viewShop.coverpicture
      var email           = viewShop.email
      var contactnumber   = viewShop.contactnumber
      var rangelocation   = viewShop.rangelocation
      var locationaddress = viewShop.locationaddress
      var locationlat     = viewShop.locationlat
      var locationlon     = viewShop.locationlon
      var slug            = viewShop.slug
      var distance        = viewShop.distance
      var category        = viewShop.category
      accountdetails      = viewShop.accountdetails?viewShop.accountdetails:[];
      var delivery        = viewShop.delivery
      var description     = viewShop.description
  }
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SHOP({id:id}));
    },[])
    
  useEffect(()=>{
    if(accountdetails.length){
      setValues({...values,accountdetails: accountdetails});
    }
  },[accountdetails.length])
  
  const incrementLength = e =>{
    var length = values.accountdetailslength+1
    setValues({...values,accountdetailslength:length});
  }
  const onChangeInput = e =>{
    const { name, value, id } = e.target;
    var safedata = values.accountdetails
    var decaccountdetails = 0
    if(safedata[id]){
      safedata[id][name] = value
    } else {
      var data = { [name]:value}
      safedata.push(data)
    }
    setValues({...values,accountdetails:safedata,accountdetailslength:decaccountdetails});
  }
  const deleteAccount = e => {
    var accountId = e.target.id;
    var accountarray = values.accountdetails;
    accountarray.splice(accountId,1)
    setValues({...values,accountdetails:accountarray});
  }
  var bankdetailsArray = []
  for(var i=0;i<values.accountdetails.length;i++) {
    bankdetailsArray.push(
      <>
        <div className="row">
          <div className="col-md-6">
            <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Bank Name</label>
                          <input type="text" className="form-control" id={i} value={values.accountdetails[i].bankname} name="bankname" onChange={onChangeInput}placeholder="Enter Shop name"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Account holder Name</label>
                        <input type="text" className="form-control" id={i} value={values.accountdetails[i].holdername} name="holdername" onChange={onChangeInput}placeholder="Enter Shop name"/>
                      </div>
                    </div>
                  </div>
                </form>
            </div>
          </div>
          <div className="col-md-6">
          <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Account Number</label>
                        <input type="text" className="form-control" id={i} value={values.accountdetails[i].accountnumber} name="accountnumber" onChange={onChangeInput}placeholder="Enter Bank Account number"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        {/* <label >Account Number</label> */}
                       <button type="button" className="btn btn-danger float-right" id={i} onClick={deleteAccount}><i class="fas fa-trash" id={i} data-action-type="Delete"></i></button>
                      </div>
                    </div>
                  </div>
                </form>
             </div>
          </div>
        </div>
        <hr/>
      </>
    )
  }
  const lengthofaccount = values.accountdetails.length;
  for(var i=0;i<values.accountdetailslength;i++) {
    bankdetailsArray.push(
      <>
        <div className="row">
          <div className="col-md-6">
            <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Bank Name</label>
                          <input type="text" className="form-control" id={lengthofaccount+i} name="bankname" onChange={onChangeInput}placeholder="Enter Shop name"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Account holder Name</label>
                        <input type="text" className="form-control" id={lengthofaccount+i} name="holdername" onChange={onChangeInput}placeholder="Enter Shop name"/>
                      </div>
                    </div>
                  </div>
                </form>
            </div>
          </div>
          <div className="col-md-6">
          <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Account Number</label>
                        <input type="text" className="form-control" id={lengthofaccount+i} name="accountnumber" onChange={onChangeInput}placeholder="Enter Bank Account number"/>
                      </div>
                    </div>
                  </div>
                </form>
             </div>
          </div>
        </div>
        <hr/>
      </>
    )
  }
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    if(name=="image"){
      var file            =  e.target.files[0];
      if(file){
        const imgvalidate   =  Imagevalidation(file);
        if(imgvalidate) {
          var reader          = new FileReader();
          var url             = reader.readAsDataURL(file);
          reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file));
             setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false});
        }.bind(this);
        } else {
          setValues({...values,imgSrc: "dist/img/image.png",imagecheck:true,image:file,imageError:false});
        }
      }
    } else if(name=="coverpicture"){
      var file            =  e.target.files[0];
      if(file){
        const imgvalidate   =  Imagevalidation(file);
        if(imgvalidate) {
          var reader          = new FileReader();
          var url             = reader.readAsDataURL(file);
          reader.onloadend    = function(e){
            dispatch(AC_HANDLE_CHANGE(name,file));
            setValues({...values,coverpictureSrc: [reader.result],coverpicturecheck:false,coverpictureError:false});
        }.bind(this);
        } else {
          setValues({...values,coverpictureSrc: "dist/img/image.png",coverpicturecheck:true,icon:file,coverpictureError:false});
        }
      }
    } else if(name=="paymentmethod"){
        const methods = viewShop.paymentmethod;
        if(e.target.checked==false){
          const index = methods.indexOf(value)
          methods.splice(index,1)
            dispatch(AC_HANDLE_CHANGE(name,methods));
            setValues({...values,paymentmethod: methods});
        } else {
            methods.push(value)
            dispatch(AC_HANDLE_CHANGE(name,methods));
            setValues({...values,paymentmethodError:false});
        }
    }  else if(name=="email"){
      const emailvalidate   =  Emailvalidate(value);
      if(emailvalidate){
       setValues({...values,[error]:false,emailcheck:false});
      } else {
       setValues({...values,[error]:false,emailcheck:true});
      }
      dispatch(AC_HANDLE_CHANGE(name,value));
    } else {
            dispatch(AC_HANDLE_CHANGE(name,value));
            setValues({...values,[error]:false});
    }
  };

  const validateForm = () => {
     const data = {shopname:false,coverpicture:false,image:false,slug:false,
                    rangelocation:false,distance:false,category:false,paymentmethod:false,
                    delivery:false,contactnumber:false,email:false,locationaddress:false,
                    locationlat:false,locationlon:false,description:false,bankaccount:false,};
     var check = 1;
     if(!shopname){
       data.shopname = true;
       check = 0;
     }
     if(!slug){
      data.slug = true;
      check = 0;
    }
     if(!rangelocation){
      data.rangelocation = true;
      check = 0;
    }
     if(!distance){
      data.distance = true;
      check = 0;
    }
     if(!category){
      data.category = true;
      check = 0;
    }
     if(values.accountdetails.length==0){
      data.bankaccount = true;
      check = 0;
    }
     if(!delivery){
      data.delivery = true;
      check = 0;
    }
     if(! contactnumber){
      data.contactnumber = true;
      check = 0;
    }
     if(! email){
      data.email = true;
      check = 0;
    }
     if(! locationaddress){
      data.locationaddress = true;
      check = 0;
     }
     if(! locationlat){
      data.locationlat = true;
      check = 0;
     }
     if(! locationlon){
      data.locationlon = true;
      check = 0;
     }
     if(! description){
      data.description = true;
      check = 0;
     }
     if(! viewShop.image){
       data.image = true;
       check = 0;
      }
     if(! viewShop.coverpicture){
       data.coverpicture = true;
       check = 0;
      }
     setValues({...values,rangelocationError:data.rangelocation,slugError:data.slug,
                  shopnameError:data.shopname,coverpictureError:data.coverpicture,imageError:data.image,
                  distanceError:data.distance,categoryError:data.category,paymentmethodError:data.paymentmethod,
                  deliveryError:data.delivery,contactnumberError:data.contactnumber,emailError:data.email,
                  locationaddressError:data.locationaddress,locationlatError:data.locationlat,
                  locationlonError:data.locationlon,descriptionError:data.description,bankaccountError:data.bankaccount,});
      if(check&&!values.imagecheck&&!values.coverpicturecheck&&!values.emailcheck) {
        const id = props.match.params.id;
        const formData = new FormData();
          formData.append('id', id);
          formData.append('shopname',  shopname);
          formData.append('slug',  slug);
          formData.append('rangelocation',  rangelocation);
          formData.append('distance',  distance);
          formData.append('category',  category);
          formData.append('accountdetails', JSON.stringify(values.accountdetails));
          formData.append('delivery',  delivery);
          formData.append('contactnumber',  contactnumber);
          formData.append('email',  email);
          formData.append('locationaddress',  locationaddress);
          formData.append('locationlat',  locationlat);
          formData.append('locationlon',  locationlon);
          formData.append('description',  description);
          formData.append('coverpicture',  viewShop.coverpicture);
          formData.append('image',  viewShop.image);
          dispatch(AC_EDIT_SHOP(formData));
   }
  }
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Edit Shop</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  <li className="breadcrumb-item active">Edit Shop</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section className="content">
          <div className="container-fluid">
            <div className="card card-default">
              <div className="card-header" style={{backgroundColor:"#007bff"}}>
                <h3 className="card-title" style={{color:"white"}}>Edit shop</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                  <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Shop Name</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="shopname" value={shopname}onChange={onChangeValue}placeholder="Enter Shop name"/>
                      {values.shopnameError ? <label style={{color:"red"}}>Shop name is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Range Location</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="rangelocation" value={rangelocation}onChange={onChangeValue}placeholder="Enter Range Location"/>
                      {values.rangelocationError ? <label style={{color:"red"}}>Range Location is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Category</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="category" value={category}onChange={onChangeValue}placeholder="Enter Category name"/>
                      {values.categoryError ? <label style={{color:"red"}}>Category is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Email</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="email" value={email}onChange={onChangeValue}placeholder="Enter Email"/>
                      {values.emailError ? <label style={{color:"red"}}>Email is required</label> : ""}
                      {values.emailcheck ? <label style={{color:"red"}}>Email is Invalid</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Location LAT</label><span style={{color:"red"}}>*</span>
                      <input type="number" className="form-control" name="locationlat" value={locationlat}onChange={onChangeValue}placeholder="Enter Latitude"/>
                      {values.locationlatError ? <label style={{color:"red"}}>Location LAT is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Location Address</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" name="locationaddress" value={locationaddress}onChange={onChangeValue}placeholder="Enter Address"/>
                      {values.locationaddressError ? <label style={{color:"red"}}>Location Address is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Image</label><span style={{color:"red"}}>*</span>
                      <div className="input-group">
                         <div className="custom-file">
                             <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                             <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                         </div>
                         <div className="input-group-append">
                             <button className="input-group-text" id="">Upload</button>
                         </div>
                      </div>
                      {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                      {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                      <div>
                       {values.imgSrc?<img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={image} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                       </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Slug</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="slug" value={slug}onChange={onChangeValue}placeholder="Enter Slug"/>
                      {values.slugError ? <label style={{color:"red"}}>Slug is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Distance</label><span style={{color:"red"}}>*</span>
                      <input type="text" className="form-control" name="distance" value={distance}onChange={onChangeValue}placeholder="Enter Distance"/>
                      {values.distanceError ? <label style={{color:"red"}}>Distance is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Delivery</label><span style={{color:"red"}}>*</span>
                      <select className="form-control" name="delivery" onChange={onChangeValue}>
                        <option value="yes" selected={delivery=="yes"}>Yes</option>
                        <option value="no" selected={delivery=="no"}>No</option>
                      </select>
                      {values.deliveryError ? <label style={{color:"red"}}>Delivery is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Contact Number</label><span style={{color:"red"}}>*</span>
                      <input type="number" className="form-control" name="contactnumber" value={contactnumber}onChange={onChangeValue}placeholder="Enter Contact number"/>
                      {values.contactnumberError ? <label style={{color:"red"}}>Contact Number is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Location LON</label><span style={{color:"red"}}>*</span>
                      <input type="number" className="form-control" name="locationlon" value={locationlon}onChange={onChangeValue}placeholder="Enter Longitude"/>
                      {values.locationlonError ? <label style={{color:"red"}}>Location LON is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Description</label><span style={{color:"red"}}>*</span>
                      <textarea type="text" className="form-control" name="description" value={description}onChange={onChangeValue}placeholder="Enter Description"/>
                      {values.descriptionError ? <label style={{color:"red"}}>Description is required</label> : ""}
                    </div>
                    <div className="form-group">
                      <label>Cover picture</label><span style={{color:"red"}}>*</span>
                      <div className="input-group">
                         <div className="custom-file">
                             <input type="file" name="coverpicture" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                             <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                         </div>
                         <div className="input-group-append">
                             <button className="input-group-text" id="">Upload</button>
                         </div>
                      </div>
                      {values.coverpictureError ? <label style={{color:"red"}}>Cover picture is required</label> : ""}
                      {values.coverpicturecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                      <div>
                       {values.coverpictureSrc?<img src={values.coverpictureSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.coverpicturecheck?"":<img src={coverpicture} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                       </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-header" >
                <h3 className="card-title" ><b>Add Account details</b></h3>
                <button type="button" className="btn btn-info float-right" onClick={incrementLength}>Add detail</button>
              </div>
              <div className="card-body">
              {values.bankaccountError ? <label style={{color:"red"}}>Account Details are required</label> : ""}
                {bankdetailsArray}
              </div>
              <div className="card-footer">
               <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
}

export default EditShop;
