import React,{ useState, useEffect } from 'react';
import { Link ,Redirect} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AC_LIST_SHOPS,AC_DELETE_SHOP } from '../../actions/shop';
import { COLDEF } from '../../common/gridoptions';
import swal from 'sweetalert';
import API from '../../common/api';

const ShopsList = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(AC_LIST_SHOPS());
  },[])
  const shop = useSelector(state => state.ShopReducer)
  const shopsList = shop.shopsList;

  const [value, setValue] = useState({
    columnDefs :[
      { "headerName" : "Sl No",field:"#",valueGetter :"node.rowIndex+1",sortable:false,width:"100"
      },
      { "headerName" : "Shop Name",field:"shopname",width:"150"
      },
      { "headerName" : "Slug",field:"slug",width:"150"
      },
      { "headerName" : "Image", field :"image",width:"150",
        cellRenderer: function (params) {
         var image = API.LIVEURL.concat("uploads/" ,params.value);
         return '<img src='+image+' alt="Image not found" style="width: 80%; height: 100%;"/>';
        }
      },
      { "headerName" : "Email",field:"email",width:"150"
      },
      { "headerName" : "Contact Number",field:"contactnumber",width:"150"
      },
      
      { headerName: 'Actions', width: "200",sortable: false,filter:false,
        template:
           `<div>
           <div class="btn bg-info btn-xs" href='#' style={{marginRight:"10px",height:"5px",width:"5px"}} data-action-type="View">
           <i class="fas fa-folder"  data-action-type="View"></i>
           View
           </div>
           <div class="btn bg-primary btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Edit">
           <i class="fas fa-pencil-alt" data-action-type="Edit"></i>
           Edit
           </div>
           <div class="btn bg-danger btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Delete">
           <i class="fas fa-trash"  data-action-type="Delete"></i>
           Delete
           </div>
           </div>`,
       },
   ],
   defaultColDef: COLDEF,
  });
    const onSortChanged = e => {
     e.api.refreshCells();
    }
   const onRowClicked = event =>{
    const rowValue = event.event.target;
    const value    = rowValue.getAttribute('data-action-type');
    if(value === 'View'){
      setValue({...value,redirect:'View',id:event.data._id})
    }
    if(value === 'Edit'){
      setValue({...value,redirect:'Edit',id:event.data._id})
    }
    if(value === 'Delete'){
      swal({
           // title: "Are you sure?",
           text: "Once deleted, you will not be able to recover this data!",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             dispatch(AC_DELETE_SHOP({id:event.data._id}));
           }
     });
    }
  }


  if(value.redirect === 'View'){
    return <Redirect to ={'/view-shop/'+value.id}/>
  }
  if(value.redirect === 'Edit'){
    return <Redirect to ={'/edit-shop/'+value.id}/>
  }
  return(
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Shops List</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Shops List</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <div className="ag-theme-balham" >
      <AgGridReact
          rowHeight={40}
          rowClass ={'centerAlign'}
          onSortChanged={onSortChanged}
          rowSelection={'single'}
          onRowClicked={onRowClicked}
          suppressCellSelection={true}
          pagination={true}
          domLayout={"autoHeight"}
          defaultColDef={COLDEF}
          columnDefs={value.columnDefs}
          rowData={shopsList}>
      </AgGridReact>
      </div>
    </div>
  )
}

export default ShopsList;
