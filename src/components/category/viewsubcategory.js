import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_SUBCATEGORY,AC_LIST_CATEGORIES } from '../../actions/category';
import API from '../../common/api';

const ViewSubCategory = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SUBCATEGORY({id:id}));
    dispatch(AC_LIST_CATEGORIES());
  },[])

  const category = useSelector(state => state.CategoryReducer)
  const viewCategory = category.viewSubcategory;
  const categoriesList = category.maincategoriesList;

  if(viewCategory) {
    var name          = viewCategory.name
    var parentID      = viewCategory.parentID
    var image         = API.LIVEURL+"uploads/"+viewCategory.image
    var icon          = API.LIVEURL+"uploads/"+viewCategory.icon
    var status        = viewCategory.status
  }
  const categoriesArray = []
  for(var i=0;i<categoriesList.length;i++) {
    categoriesArray.push(
      <option key={i} value={categoriesList[i]._id} selected={parentID==categoriesList[i]._id}> {categoriesList[i].name}</option>
    )
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Sub Category</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Sub Category</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Sub Category</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Parent Category</label>
                    <select className="form-control" name="parentID" disabled>
                      {categoriesArray}
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Image</label>
                    <div>
                       <img src={image} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" value={name}disabled placeholder="Category name"/>
                  </div>
                  <div className="form-group">
                    <label>Icon</label>
                    <div>
                       <img src={icon} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>
                     </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewSubCategory;
