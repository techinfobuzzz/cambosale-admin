import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_SUBCATEGORY,AC_HANDLE_CHANGE,AC_EDIT_SUBCATEGORY,AC_LIST_MAINCATEGORIES } from '../../actions/category';
import API from '../../common/api';
import {Imagevalidation} from '../../common/validate';

const EditSubCategory = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_SUBCATEGORY({id:id}));
    dispatch(AC_LIST_MAINCATEGORIES());
  },[])

  const [values, setValues] = useState({
    nameError           : false,
    imgSrc              : '',
    iconSrc             : '',
    imageError          : false,
    imagecheck          : false,
    iconError           : false,
    iconcheck           : false,
  });

  const category       = useSelector(state => state.CategoryReducer)
  const categoriesList = category.maincategoriesList;
  const viewCategory   = category.viewSubcategory;
  if(viewCategory) {
    var name          = viewCategory.name
    var parentID      = viewCategory.parentID;
    var status        = viewCategory.status
    var image         = API.LIVEURL+"uploads/"+viewCategory.image
    var icon          = API.LIVEURL+"uploads/"+viewCategory.icon
  }
  const validateForm = () => {
    const data          = viewCategory;
    const stateData     = {name:false,icon:false,image:false};
    const id            = props.match.params.id;
    if(!data.name){
       stateData.name = true;
     }
     if(!data.image){
       stateData.image = true;
     }
     if(!data.icon){
       stateData.icon = true;
     }
     setValues({...values,nameError:stateData.name,iconError:stateData.icon,imageError:stateData.image});
     if(data.name&&data.image&&!values.imagecheck&&data.icon&&!values.iconcheck){
       const formData = new FormData();
       formData.append('id', id);
       formData.append('name', data.name);
       formData.append('parentID', data.parentID);
       formData.append('status', data.status);
       formData.append('image', data.image);
       formData.append('icon', data.icon);
       dispatch(AC_EDIT_SUBCATEGORY(formData));
   }
  }
  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     if(name=="image"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file,"SubCategory"));
             setValues({...values,imgSrc: [reader.result],imagecheck:false,imageError:false});
         }.bind(this);
         } else {
           setValues({...values,imgSrc: "",imagecheck:true,imageError:false});
         }
       }
     } else if(name=="icon"){
       var file            =  e.target.files[0];
       if(file){
         const imgvalidate   =  Imagevalidation(file);
         if(imgvalidate) {
           var reader          = new FileReader();
           var url             = reader.readAsDataURL(file);
           reader.onloadend    = function(e){
             dispatch(AC_HANDLE_CHANGE(name,file,"SubCategory"));
             setValues({...values,iconSrc: [reader.result],iconcheck:false,iconError:false});
         }.bind(this);
         } else {
           setValues({...values,iconSrc: "",iconcheck:true,iconError:false});
         }
       }
     } else {
       dispatch(AC_HANDLE_CHANGE(name,value,"SubCategory"));
       setValues({...values,[error]:false});
     }
  }

  const categoriesArray = []
  for(var i=0;i<categoriesList.length;i++) {
    categoriesArray.push(
      <option key={i} value={categoriesList[i]._id} selected={parentID==categoriesList[i]._id}> {categoriesList[i].name}</option>
    )
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Sub Category</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Sub Category</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Sub Category</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                <div className="form-group">
                  <label>Main Category</label><span style={{color:"red"}}>*</span>
                  <select className="form-control" name="parentID" onChange={onChangeValue}>
                    {categoriesArray}
                  </select>
                  {values.parentIDError ? <label style={{color:"red"}}>Main Category is required</label> : ""}
                </div>
                  <div className="form-group">
                    <label>Image</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="image" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.imageError ? <label style={{color:"red"}}>Image is required</label> : ""}
                    {values.imagecheck ? <label style={{color:"red"}}>Image format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.imgSrc?<img src={values.imgSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.imagecheck?"":<img src={image} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={name}onChange={onChangeValue}placeholder="Enter Category name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Icon</label><span style={{color:"red"}}>*</span>
                    <div className="input-group">
                       <div className="custom-file">
                           <input type="file" name="icon" className="custom-file-input" id="exampleInputFile" onChange={onChangeValue}/>
                           <label className="custom-file-label" htmlFor="exampleInputFile">Choose file</label>
                       </div>
                       <div className="input-group-append">
                           <button className="input-group-text" id="">Upload</button>
                       </div>
                    </div>
                    {values.iconError ? <label style={{color:"red"}}>Icon is required</label> : ""}
                    {values.iconcheck ? <label style={{color:"red"}}>Icon format is Invalid(.jpg/.png only)</label> : ""}
                    <div>
                       {values.iconSrc?<img src={values.iconSrc} style={{height:"100px",width:"150px",padding:'10px'}} alt=""/>:values.iconcheck?"":<img src={icon} style={{height:"100px",width:"150px",padding:'10px'}}/>}
                     </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditSubCategory;
