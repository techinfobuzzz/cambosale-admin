import React,{ useState, useEffect } from 'react';
import { Link ,Redirect} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AC_LIST_CATEGORIES,AC_DELETE_CATEGORY } from '../../actions/category';
import { COLDEF } from '../../common/gridoptions';
import swal from 'sweetalert';
import API from '../../common/api';

const CategoriesList = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(AC_LIST_CATEGORIES());
  },[])
  const category = useSelector(state => state.CategoryReducer)
  const categoriesList = category.categoriesList;

  const [value, setValue] = useState({
    columnDefs :[
      { "headerName" : "No",field:"#",valueGetter :"node.rowIndex+1",sortable:false,width: "60"
      },
      { "headerName" : "Main Category",field:"maincategory.name",width: "150"
      },
      { "headerName" : "Sub Category",field:"subcategory.name",width: "150"
      },
      { "headerName" : "Category",field:"name",width: "150"
      },
      { "headerName" : "Sorting Order",field:"sort",width: "150"
      },
      { "headerName" : "Image", field :"image",sortable: false,filter:false,width: "150",
        cellRenderer: function (params) {
         var image = API.LIVEURL.concat("uploads/" ,params.value);
         return '<img src='+image+' alt="Image not found" style="width: 80%; height: 100%;"/>';
        }
      },
      { "headerName" : "Icon", field :"icon",sortable: false,filter:false,width: "150",
        cellRenderer: function (params) {
         var image = API.LIVEURL.concat("uploads/" ,params.value);
         return '<img src='+image+' alt="Image not found" style="width: 80%; height: 100%;"/>';
        }
      },
      { "header" : "Status", field :"status",sortable: false,filter:false,width: "100",
         cellRenderer: function(params) {
          if(params.data.status) {
            return '<span class="badge badge-success" data-action-type="Status">Active</span>';
          } else {
            return '<span class="badge badge-success" style="background-color: rgb(220, 53, 69);" data-action-type="Status">Inactive</span>';
          }
        }
      },
      { headerName: 'Actions', width: "200",sortable: false,filter:false,
        template:
           `<div>
           <div class="btn bg-info btn-xs" href='#' style={{marginRight:"10px",height:"5px",width:"5px"}} data-action-type="View">
           <i class="fas fa-folder"  data-action-type="View"></i>
           View
           </div>
           <div class="btn bg-primary btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Edit">
           <i class="fas fa-pencil-alt" data-action-type="Edit"></i>
           Edit
           </div>
           <div class="btn bg-danger btn-xs" href='#' style={{marginRight:"10px"}} data-action-type="Delete">
           <i class="fas fa-trash"  data-action-type="Delete"></i>
           Delete
           </div>
           </div>`,
       },
   ],
   defaultColDef: COLDEF,
  });
    const onSortChanged = e => {
     e.api.refreshCells();
    }
   const onRowClicked = event =>{
    const rowValue = event.event.target;
    const value    = rowValue.getAttribute('data-action-type');
    if(value === 'View'){
      setValue({...value,redirect:'View',id:event.data._id})
    }
    if(value === 'Edit'){
      setValue({...value,redirect:'Edit',id:event.data._id})
    }
    if(value === 'Delete'){
      swal({
           text: "Once deleted, you will not be able to recover this data!",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             dispatch(AC_DELETE_CATEGORY({id:event.data._id}));
           }
     });
    }
  }

  if(value.redirect === 'View'){
    return <Redirect to ={'/view-category/'+value.id}/>
  }
  if(value.redirect === 'Edit'){
    return <Redirect to ={'/edit-category/'+value.id}/>
  }
  return(
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Categories List</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Categories List</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <div className="ag-theme-balham" >
      <AgGridReact
          rowHeight={40}
          rowClass ={'centerAlign'}
          onSortChanged={onSortChanged}
          rowSelection={'single'}
          onRowClicked={onRowClicked}
          suppressCellSelection={true}
          pagination={true}
          domLayout={"autoHeight"}
          defaultColDef={COLDEF}
          columnDefs={value.columnDefs}
          rowData={categoriesList}>
      </AgGridReact>
      </div>
    </div>
  )
}

export default CategoriesList;
