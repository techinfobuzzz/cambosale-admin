import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_VIEW_CURRENCY,AC_HANDLE_CHANGE,AC_EDIT_CURRENCY} from '../../actions/currency';

const EditCurrency = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_CURRENCY({id:id}));
  },[])

  const [values, setValues] = useState({
    nameError    : false,
    symbolError  : false,
  });

  const currency = useSelector(state => state.CurrencyReducer)
  const viewCurrency = currency.viewCurrency;
  if(viewCurrency) {
    var name   = viewCurrency.name
    var symbol = viewCurrency.symbol
    var status = viewCurrency.status
  }

  const validateForm = () => {
    const data      = currency.viewCurrency;
    const stateData = {name:false,symbol:false};
    const id = props.match.params.id;
    if(!data.name){
       stateData.name = true;
     }
    if(!data.symbol){
       stateData.symbol = true;
     }
     setValues({...values,nameError:stateData.name,symbolError:stateData.symbol});
     if(data.name&&data.symbol){
        var formData = {
          id      : id,
          name    : data.name,
          symbol  : data.symbol,
          status  : data.status,
        }
        dispatch(AC_EDIT_CURRENCY(formData));
     }
  }

  const onChangeValue = e =>  {
    const { name, value } = e.target;
     var error    = name+"Error";
     dispatch(AC_HANDLE_CHANGE(name,value));
     setValues({...values,[error]:false});
  }

  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Currency</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Edit Currency</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Edit Currency</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Currency Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={name}onChange={onChangeValue}placeholder="Enter Project name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={status==true}>Active</option>
                      <option value="0" selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Currency Symbol</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="symbol" value={symbol} onChange={onChangeValue} placeholder="Enter Project Type"/>
                    {values.symbolError ? <label style={{color:"red"}}>Symbol is required</label> : ""}
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EditCurrency;
