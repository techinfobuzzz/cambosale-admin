import React,{ useState, useEffect } from 'react';
import { Link ,Redirect} from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AC_LIST_CURRENCIES,AC_DELETE_CURRENCY } from '../../actions/currency';
// import swal from 'sweetalert';

const CurrenciesList = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(AC_LIST_CURRENCIES());
  },[])

  const [value, setValue] = useState({
    columnDefs :[
      { "headerName" : "Name",field:"name",width:"200",sortable:true,filter: true ,
        cellStyle: function(params) {
             return {'font-size': '16px','margin-top': '5px'};
       }},
      { "headerName" : "Symbol", field :"symbol",width:"200",sortable:true,filter: true ,
        cellStyle: function(params) {
             return {'font-size': '16px','margin-top': '5px'};
       }},
       { "header" : "Status", field :"status",width:"300",
         cellStyle: function(params) {
              return {'font-size': '16px','margin-top': '5px'};
         },
         cellRenderer: function(params) {
          if(params.data.status) {
            return '<span class="badge badge-success" data-action-type="Status">Active</span>';
          } else {
            return '<span class="badge badge-success" style="background-color: rgb(220, 53, 69);" data-action-type="Status">Inactive</span>';
          }
        }},
      { headerName: 'Actions', width: "300",sortable: false,filter:false,
        template:
           `<div>
           <div class="btn btn-info btn-sm" href='#' style={{marginRight:"10px"}} data-action-type="View">
           <i class="fas fa-folder"  style={{padding: "10px"}} data-action-type="View"></i>
           View
           </div>
           <div class="btn btn-primary btn-sm" href='#' style={{marginRight:"10px"}} data-action-type="Edit">
           <i class="fas fa-pencil-alt"  style={{padding: "10px"}} data-action-type="Edit"></i>
           Edit
           </div>
           <div class="btn btn-danger btn-sm" href='#' style={{marginRight:"10px"}} data-action-type="Delete">
           <i class="fas fa-trash"  style={{padding: "10px"}} data-action-type="Delete"></i>
           Delete
           </div>
           </div>`,
           cellStyle: function(params) {
                  return {'margin-top' :'8px'};
          },
       },
   ]
  });
    const onSortChanged = e => {
     e.api.refreshCells();
    }
   const onRowClicked = event =>{
    const rowValue = event.event.target;
    const value    = rowValue.getAttribute('data-action-type');
    if(value === 'View'){
      setValue({...value,redirect:'View',id:event.data._id})
    }
    if(value === 'Edit'){
      setValue({...value,redirect:'Edit',id:event.data._id})
    }
    if(value === 'Delete'){
      dispatch(AC_DELETE_CURRENCY({id:event.data._id}));
    }
  }

  const currency = useSelector(state => state.CurrencyReducer)
  const currenciesList = currency.currencyList;

  if(value.redirect === 'View'){
    return <Redirect to ={'/view-currency/'+value.id}/>
  }
  if(value.redirect === 'Edit'){
    return <Redirect to ={'/edit-currency/'+value.id}/>
  }
  return(
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Currencies List</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Currencies List</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <div className="ag-theme-balham" style={ {height: '600px', width: 'auto'} }>
      <AgGridReact
          rowHeight={100}
          rowClass ={'centerAlign'}
          onSortChanged={onSortChanged}
          rowSelection={'single'}
          onRowClicked={onRowClicked}
          suppressCellSelection={true}
          pagination={true}
          paginationAutoPageSize={true}
          columnDefs={value.columnDefs}
          rowData={currenciesList}>
      </AgGridReact>
      </div>
    </div>
  )
}

export default CurrenciesList;
