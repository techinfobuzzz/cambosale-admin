import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { AC_ADD_CURRENCY } from '../../actions/currency';

const AddCurrency = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({
    name         : "",
    symbol       : "",
    status       : "1",
    nameError    : false,
    symbolError  : false,
  });
  const onChangeValue = e => {
    const { name, value } = e.target;
    const error=name+"Error";
    setValues({...values,[name]: value,[error]:false});
  };

  const validateForm = () => {
     const data = {name:false,symbol:false};
     if(!values.name){
       data.name = true;
     }
     if(!values.symbol){
       data.symbol = true;
     }
     setValues({...values,nameError:data.name,symbolError:data.symbol});
     if(values.name&&values.symbol){
        var formData = {
          id      :"0",
          name    : values.name,
          symbol  : values.symbol,
          status  : values.status,
      }
      dispatch(AC_ADD_CURRENCY(formData));
      setValues({...values,status:"1",name:"",symbol:""});
   }
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Add Currency</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">Add Currency</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>Add Currency</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Name</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="name" value={values.name}onChange={onChangeValue}placeholder="Enter Currency name"/>
                    {values.nameError ? <label style={{color:"red"}}>Name is required</label> : ""}
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" name="status" onChange={onChangeValue}>
                      <option value="1" selected={values.status=="1"}>Active</option>
                      <option value="0" selected={values.status=="0"}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Symbol</label><span style={{color:"red"}}>*</span>
                    <input type="text" className="form-control" name="symbol" value={values.symbol} onChange={onChangeValue} placeholder="Enter Currency Symbol"/>
                    {values.symbolError ? <label style={{color:"red"}}>Symbol is required</label> : ""}
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer">
             <button type="button" className="btn btn-primary" onClick={validateForm}>Submit</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default AddCurrency;
