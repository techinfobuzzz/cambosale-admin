import React,{useEffect} from 'react';
import { Link } from "react-router-dom";
import { useSelector,useDispatch  } from 'react-redux'
import { AC_VIEW_CURRENCY} from '../../actions/currency';

const ViewCurrency = (props) => {
  const dispatch = useDispatch()
  useEffect(()=>{
    const id = props.match.params.id;
    dispatch(AC_VIEW_CURRENCY({id:id}));
  },[])
  const currency = useSelector(state => state.CurrencyReducer)
  const viewCurrency = currency.viewCurrency;
  if(viewCurrency) {
    var name   = viewCurrency.name
    var symbol = viewCurrency.symbol
    var status = viewCurrency.status
  }
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>View Currency</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                <li className="breadcrumb-item active">View Currency</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="container-fluid">
          <div className="card card-default">
            <div className="card-header" style={{backgroundColor:"#007bff"}}>
              <h3 className="card-title" style={{color:"white"}}>View Currency</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times"></i></button>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Currency Name</label>
                    <input type="text" className="form-control" name="name" value={name}disabled placeholder="Enter Language name"/>
                  </div>
                  <div className="form-group">
                    <label>Status</label>
                    <select className="form-control" disabled name="status">
                      <option selected={status==true}>Active</option>
                      <option selected={status==false}>Inactive</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Currency Symbol</label>
                    <input type="text" className="form-control" name="code" value={symbol} disabled placeholder="Enter Language code"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default ViewCurrency;
