import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import {Link } from "react-router-dom";
import {Validpassword} from '../../common/validate.js';
import { AC_CHANGE_PASSWORD } from '../../actions/adminsettings';

const ChangePassword = () => {
    const dispatch = useDispatch()
    const [values, setValues] = useState({
      password               : "",
      confirmpassword        : "",
      passswordError         : false,
      passswordCheck         : false,
      confirmpasswordError   : false,
    });
    const onChangeValue = e => {
      const { name, value } = e.target;
      const error=name+"Error";
      if(name == "password"){
        if(Validpassword(value)){
          setValues({...values,[name]: value,[error]:false,passswordCheck:false});
        } else {
          setValues({...values,[name]: value,[error]:false,passswordCheck:true});
        }
      } else {
        if(values.password==value){
          setValues({...values,[name]: value,[error]:false,confirmpasswordError:""});
        } else {
          setValues({...values,[name]: value,[error]:false,confirmpasswordError:"Password doesn't match"});
        }
      }
    };

    const validateForm = () => {
       const data          = {password:false,confirmpassword:""};
       const urlParams     = new URLSearchParams(window.location.search);
       const token         = urlParams.get('token')
       if(token){
           if(!values.confirmpassword){
             data.confirmpassword = "Confirm password is required";
           } else {
             if(values.password!=values.confirmpassword){
               data.confirmpassword = "Password doesn't match";
             }
           }
           if(!values.password){
             data.password = true;
           }
           setValues({...values,confirmpasswordError:data.confirmpassword,passwordError:data.password});
           if(values.password==values.confirmpassword&&values.password){
              var formData = {
                token       : token,
                password    : values.password,
            }
            dispatch(AC_CHANGE_PASSWORD(formData));
          }
      }
    }
    return(
      <body className="hold-transition login-page">
        <div className="login-box">
          <div className="login-logo">
            <b>Cambo</b>Sale
          </div>
          <div className="card">
            <div className="card-body login-card-body">
              <p className="login-box-msg">You are only one step a way from your new password, recover your password now.</p>
                <div className="input-group mb-3">
                  <div style={{width:"85%"}}title="Password pattern should be minimum 8 characters,one uppercase,one lowercase,one number,one special character" data-for='toolTip1' data-place='top'>
                  <input type="password" className="form-control"name="password" placeholder="Password" onChange={onChangeValue} />
                  </div>
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                {values.passwordError ? <label style={{color:"red"}}>Password is required</label> : ""}
                {values.passswordCheck ? <label style={{color:"red"}}>Password is Invalid</label> : ""}
                <div className="input-group mb-3">
                  <div title="Confirm passsword should be same as new password" style={{width:"85%"}} data-for='toolTip1' data-place='top'>
                  <input type="password" className="form-control" name="confirmpassword" onChange={onChangeValue} placeholder="Confirm Password" />
                  </div>
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                {values.confirmpasswordError ? <label style={{color:"red"}}>{values.confirmpasswordError}</label> : ""}
                <div className="col-12">
                 <button type="submit" className="btn btn-primary btn-block"onClick={validateForm}>Change password</button>
                </div>
                <div className="mt-3 mb-1">
                <Link to='/' className="mt-3 mb-1">
                  <a href="">Login</a>
                </Link>
                </div>
            </div>
          </div>
        </div>
      </body>
    )
}

export default ChangePassword;
