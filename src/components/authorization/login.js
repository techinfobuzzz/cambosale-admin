import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link, Redirect } from "react-router-dom";
import {Emailvalidate} from '../../common/validate.js';
import { AC_AUTHORIZATION } from '../../actions/adminsettings';

const Login = () => {
    const dispatch = useDispatch()
    const [values, setValues] = useState({
      email            : "",
      password         : "",
      emailError       : false,
      emailCheck       : false,
      passwordError    : false,
    });
    const onChangeValue = e => {
      const { name, value } = e.target;
      const error=name+"Error";
      if(name == "email"){
        if(Emailvalidate(value)){
          setValues({...values,[name]: value,[error]:false,emailCheck:false});
        } else {
          setValues({...values,[name]: value,[error]:false,emailCheck:true});
        }
      } else {
        setValues({...values,[name]: value,[error]:false});
      }
    };

    const validateForm = () => {
       const data = {email:false,password:false};
       if(!values.email){
         data.email = true;
       }
       if(!values.password){
         data.password = true;
       }
       setValues({...values,emailError:data.email,passwordError:data.password});
       if(values.email&&values.password&&!values.emailCheck){
          var formData = {
            email       : values.email,
            password    : values.password,
        }
        dispatch(AC_AUTHORIZATION(formData));
     }
    }
    return(
      <div className="hold-transition login-page">
        <div className="login-box">
          <div className="login-logo">
            <b>Cambo</b>Sale
          </div>
          <div className="card">
            <div className="card-body login-card-body">
              <p className="login-box-msg">Sign in to start your session</p>
                <div className="input-group mb-3">
                  <input type="email" className="form-control" placeholder="Email" name="email"  onChange={onChangeValue}/>
                   <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-envelope"></span>
                    </div>
                  </div>
                </div>
                {values.emailError ? <label style={{color:"red"}}>Email is required</label> : ""}
                {values.emailCheck ? <label style={{color:"red"}}>Email is Invalid</label> : ""}
                <div className="input-group mb-3">
                  <input type="password" className="form-control" placeholder="Password" name="password" onChange={onChangeValue}/>
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                {values.passwordError ? <label style={{color:"red"}}>Password is required</label> : ""}
                <div className="row">
                  <div className="col-8">
                    <div className="icheck-primary" style={{display:"none"}}>
                      <input type="checkbox" name="remember" value="" othersProps onChange={onChangeValue} id="remember"/>
                      <label htmlFor="remember" >
                        Remember Me
                      </label>
                    </div>
                  </div>
                  <div className="col-4">
                    <button type="button" value="submit" className="btn btn-primary btn-block btn-flat" onClick={validateForm}>Sign In</button>
                  </div>
                </div>
              <p className="mb-1">
              <Link to="/forgot-password">I forgot my password</Link>
              </p>
              </div>
            </div>
          </div>
        </div>
    )
  }

export default Login;
