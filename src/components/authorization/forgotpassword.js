import { useState, useEffect } from 'react';
import { useSelector,useDispatch  } from 'react-redux'
import { Link } from "react-router-dom";
import { Emailvalidate } from '../../common/validate';
import { AC_FORGOT_PASSWORD } from '../../actions/adminsettings';

const Forgotpassword = () => {
    const dispatch = useDispatch()
    const [values, setValues] = useState({
      email            : "",
      emailError       : false,
      emailCheck       : false,
    });
    const onChangeValue = e => {
      const { name, value } = e.target;
      const error=name+"Error";
      if(Emailvalidate(value)){
        setValues({...values,[name]: value,[error]:false,emailCheck:false});
      } else {
        setValues({...values,[name]: value,[error]:false,emailCheck:true});
      }
    };

    const validateForm = () => {
       const data = {email:false};
       if(!values.email){
         data.email = true;
       }
       setValues({...values,emailError:data.email});
       if(values.email&&!values.emailCheck){
          var formData = {
            email       : values.email,
        }
        dispatch(AC_FORGOT_PASSWORD(formData));
     }
    }
    return(
      <body className="hold-transition login-page">
      <div className="login-box">
        <div className="login-logo">
          <b>Cambo</b>Sale
        </div>
        <div className="card">
          <div className="card-body login-card-body">
            <p className="login-box-msg">You forgot your password? Here you can easily retrieve a new password</p>
              <div className="input-group mb-3">
                <input type="email" className="form-control" placeholder="Email" name="email" onChange={onChangeValue}/>
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
              {values.emailError ? <label style={{color:"red"}}>Email is required</label> : ""}
              {values.emailCheck ? <label style={{color:"red"}}>Email is Invalid</label> : ""}
              <div className="row">
                <div className="col-12">
                    <button type="submit" className="btn btn-primary btn-block" onClick={validateForm}>Request new password</button>
                </div>
              </div>
              <div className="mt-3 mb-1">
              <Link to='/' className="mt-3 mb-1">
                <a href="">Login</a>
              </Link>
              </div>
          </div>
        </div>
      </div>
    </body>
    )
}

export default Forgotpassword;
