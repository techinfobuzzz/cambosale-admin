import { toast } from 'react-toastify';
import swal from 'sweetalert';

export function Success(message) {
  toast.success(message,{position: "top-right"});
}

export function Error(message) {
  toast.error(message,{position: "top-right"})
}

export function Loader() {
  swal({
     title: "",
     text: "Please wait.",
     icon: "../../spinner.gif",
     button: false
 });
}
