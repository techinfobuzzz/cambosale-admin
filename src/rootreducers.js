import { combineReducers } from 'redux';

import LanguageReducer from './reducers/languagereducer';
import CurrencyReducer from './reducers/currencyreducer';
import FaqReducer from './reducers/faqreducer';
import PageReducer from './reducers/pagereducer';
import NewsletterReducer from './reducers/newsletterreducer';
import SliderReducer from './reducers/sliderreducer';
import ShopReducer from './reducers/shopreducer';
import AdminsettingsReducer from './reducers/adminsettingsreducer';
import UserReducer from './reducers/userreducer';
import SellerReducer from './reducers/sellerreducer';
import CategoryReducer from './reducers/categoryreducer';
import ProductReducer from './reducers/productreducer';
import DistrictReducer from './reducers/districtreducer';
import BrandReducer from './reducers/brandreducer';
import BlogReducer from './reducers/blogreducer';

export default combineReducers({
  LanguageReducer,
  CurrencyReducer,
  FaqReducer,
  PageReducer,
  NewsletterReducer,
  SliderReducer,
  ShopReducer,
  UserReducer,
  SellerReducer,
  CategoryReducer,
  ProductReducer,
  AdminsettingsReducer,
  DistrictReducer,
  BrandReducer,
  BlogReducer
})
