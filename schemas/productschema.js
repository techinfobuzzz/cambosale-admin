var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var PRODUCTSCHEMA = {
    maincategory      : { type: Schema.Types.ObjectId, ref: 'maincategories' },
    subcategory       : { type: Schema.Types.ObjectId, ref: 'subcategories' },
    category          : { type: Schema.Types.ObjectId, ref: 'categories' },
    shop              : { type: Schema.Types.ObjectId, ref: 'shops' },
    user              : { type: Schema.Types.ObjectId, ref: 'users' },
    city              : { type: Schema.Types.ObjectId, ref: 'cities' },
    district          : { type: Schema.Types.ObjectId, ref: 'districts' },
    brand             : { type: Schema.Types.ObjectId, ref: 'brands' },
    locale            : String,
    title             : String,
    slug              : String,
    discount          : Number,
    discounttype      : String,
    price             : Number,
    discountprice     : Number,
    productcondition  : String,
    youtubeurl        : String,
    images            : [],
    description       : String,
    status            : String,
    productaddress    : String,
    listingfeature    : String,
    deliverystatus    : String,
    phonenumber       : String,
    minimumbid        : String,
    maximumbid        : String,
    bidstart          : String,
    bidend            : String,
    location     : {
        lat: Number,
        lon: Number
    },
};

module.exports = PRODUCTSCHEMA;
