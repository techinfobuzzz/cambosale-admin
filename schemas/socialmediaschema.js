var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var SOCIALMEDIASCHEMA = {
    iconname     : String,
    iconlink     : String,
    icon         : String,
    adminId      : { type: Schema.Types.ObjectId, ref: 'adminsettings' },
};

module.exports = SOCIALMEDIASCHEMA;
