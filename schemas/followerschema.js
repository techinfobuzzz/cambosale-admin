var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var FOLLOWERSCHEMA = {
    userfollower      : { type: Schema.Types.ObjectId, ref: 'users' },
    shopfollower      : { type: Schema.Types.ObjectId, ref: 'shops' },
    userfollowing     : { type: Schema.Types.ObjectId, ref: 'users' },
    shopfollowing     : { type: Schema.Types.ObjectId, ref: 'shops' },
};

module.exports = FOLLOWERSCHEMA;
