var DISTRICTSCHEMA = {
    state      : String,
    district   : String,
    slug       : String,
    status     : Boolean,
};

module.exports = DISTRICTSCHEMA;
