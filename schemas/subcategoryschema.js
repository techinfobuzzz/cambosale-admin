var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var SUBCATEGORYSCHEMA = {
    parentID   : { type: Schema.Types.ObjectId, ref: 'maincategories' },
    name       : String,
    icon       : String,
    image      : String,
    status     : Boolean,
};

module.exports = SUBCATEGORYSCHEMA;
