var SLIDERSCHEMA = {
    name        : String,
    description : String,
    image       : String,
    status      : Boolean,
};

module.exports = SLIDERSCHEMA;
