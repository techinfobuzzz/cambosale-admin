var MAINCATEGORYSCHEMA = {
    name       : String,
    icon       : String,
    image      : String,
    status     : Boolean,
};

module.exports = MAINCATEGORYSCHEMA;
