var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var CARTSCHEMA = {
    userid          : { type: Schema.Types.ObjectId, ref: 'users' },
    productid       : { type: Schema.Types.ObjectId, ref: 'products'},
    quantity        : Number,
    discountapplied : Boolean,
    price           : Number
  
};

module.exports = CARTSCHEMA;