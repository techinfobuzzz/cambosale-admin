var PAGESCHEMA = {
    name        : String,
    slug        : String,
    description : String,
    status      : Boolean,
};

module.exports = PAGESCHEMA;
