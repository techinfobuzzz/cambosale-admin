var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var LIKEDPRODUCTSSCHEMA = {
    userId      : { type: Schema.Types.ObjectId, ref: 'users' },
    productId   : { type: Schema.Types.ObjectId, ref: 'products' },
    productUser : { type: Schema.Types.ObjectId, ref: 'users' },
    productShop : { type: Schema.Types.ObjectId, ref: 'shops' },
};

module.exports = LIKEDPRODUCTSSCHEMA;
