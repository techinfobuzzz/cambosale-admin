var ADMINSETTINGSSCHEMA = {
  site           : {},
  socialmedia    : {},
  password       : String,
  email          : String
};

module.exports = ADMINSETTINGSSCHEMA;
