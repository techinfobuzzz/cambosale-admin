var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var CITYSCHEMA = {
    districtId   : { type: Schema.Types.ObjectId, ref: 'districts' },
    city         : String,
    slug         : String,
    status       : Boolean,
};

module.exports = CITYSCHEMA;
