var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var BRANDSCHEMA = {
    name          : String,
    image         : String,
    status        : Boolean,
};

module.exports = BRANDSCHEMA;
