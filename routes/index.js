const BaseUrl = '/api/v1/';
module.exports = function(app) {
  app.use(BaseUrl+"faqs", require("../controllers/admin/faq"));
  app.use(BaseUrl+"pages", require("../controllers/admin/page"));
  app.use(BaseUrl+"newsletters", require("../controllers/admin/newsletter"));
  app.use(BaseUrl+"adminsettings", require("../controllers/admin/adminsettings"));
  app.use(BaseUrl+"categories", require("../controllers/admin/category"));
  app.use(BaseUrl+"languages", require("../controllers/admin/language"));
  app.use(BaseUrl+"currencies", require("../controllers/admin/currency"));
  app.use(BaseUrl+"users", require("../controllers/admin/user"));
  app.use(BaseUrl+"sliders", require("../controllers/admin/slider"));
  app.use(BaseUrl+"shops", require("../controllers/admin/shop"));
  app.use(BaseUrl+"sellers", require("../controllers/admin/seller"));
  app.use(BaseUrl+"products", require("../controllers/admin/product"));
  app.use(BaseUrl+"districts", require("../controllers/admin/district"));
  app.use(BaseUrl+"brands", require("../controllers/admin/brand"));
  app.use(BaseUrl+"blogs", require("../controllers/admin/blog"));

  app.use(BaseUrl+"site/accounts", require("../controllers/site/account"));
  app.use(BaseUrl+"site/dashboard", require("../controllers/site/dashboard"));
  app.use(BaseUrl+"site/products", require("../controllers/site/products"));
  app.use(BaseUrl+"site/categories", require("../controllers/site/categories"));
  app.use(BaseUrl+"site/cart", require("../controllers/site/cart"));
  app.use(BaseUrl+"site/profile", require("../controllers/site/profile"));
  app.use(BaseUrl+"site/seller", require("../controllers/site/seller"));
  app.use(BaseUrl+"site/blogs", require("../controllers/site/blog"));

}
