const mongoose = require('mongoose');

// importing schemas to create model
const importedfaqSchema = require('../schemas/faqschema');
const importedPageSchema = require('../schemas/pageschema');
const importedNewsletterSchema = require('../schemas/newsletterschema');
const importedAdminSettingsSchema = require('../schemas/adminsettingsschema');
const importedMainCategorySchema = require('../schemas/maincategoryschema');
const importedSubCategorySchema = require('../schemas/subcategoryschema');
const importedCategorySchema = require('../schemas/categoryschema');
const importedLanguageSchema = require('../schemas/languageschema');
const importedCurrencySchema = require('../schemas/currencyschema');
const importedUserSchema = require('../schemas/userschema');
const importedSliderSchema = require('../schemas/sliderschema');
const importedShopSchema = require('../schemas/shopschema');
const importedSellerSchema = require('../schemas/sellerschema');
const importedProductSchema = require('../schemas/productschema');
const importedSocialMediaSchema = require('../schemas/socialmediaschema');
const importedDistrictSchema = require('../schemas/districtschema');
const importedCitySchema = require('../schemas/cityschema');
const importedCartSchema = require('../schemas/cartschema');
const importedBrandSchema = require('../schemas/brandschema');
const importedLikedProductsSchema = require('../schemas/likedproductsschema');
const importedFollowerSchema = require('../schemas/followerschema');
const importedNotificationSchema = require('../schemas/notificationschema');
const importedBidderSchema = require('../schemas/bidderschema');
const importedBlogCategorySchema = require('../schemas/blogcategoryschema');
const importedBlogSchema = require('../schemas/blogschema');

// Creating schema
const FaqSchema = mongoose.Schema(importedfaqSchema, { timestamps: true, versionKey: false });
const NewsletterSchema = mongoose.Schema(importedNewsletterSchema, { timestamps: true, versionKey: false });
const AdminSettingsSchema = mongoose.Schema(importedAdminSettingsSchema, { timestamps: true, versionKey: false });
const PageSchema = mongoose.Schema(importedPageSchema, { timestamps: true, versionKey: false });
const MainCategorySchema = mongoose.Schema(importedMainCategorySchema, { timestamps: true, versionKey: false });
const SubCategorySchema = mongoose.Schema(importedSubCategorySchema, { timestamps: true, versionKey: false });
const CategorySchema = mongoose.Schema(importedCategorySchema, { timestamps: true, versionKey: false });
const LanguageSchema = mongoose.Schema(importedLanguageSchema, { timestamps: true, versionKey: false });
const CurrencySchema = mongoose.Schema(importedCurrencySchema, { timestamps: true, versionKey: false });
const UserSchema = mongoose.Schema(importedUserSchema, { timestamps: true, versionKey: false });
const SliderSchema = mongoose.Schema(importedSliderSchema, { timestamps: true, versionKey: false });
const ShopSchema = mongoose.Schema(importedShopSchema, { timestamps: true, versionKey: false });
const SellerSchema = mongoose.Schema(importedSellerSchema, { timestamps: true, versionKey: false });
const ProductSchema = mongoose.Schema(importedProductSchema, { timestamps: true, versionKey: false });
const SocialMediaSchema = mongoose.Schema(importedSocialMediaSchema, { timestamps: true, versionKey: false });
const DistrictSchema = mongoose.Schema(importedDistrictSchema, { timestamps: true, versionKey: false });
const CitySchema = mongoose.Schema(importedCitySchema, { timestamps: true, versionKey: false });
const CartSchema = mongoose.Schema(importedCartSchema, { timestamps: true, versionKey: false });
const LikedProductsSchema = mongoose.Schema(importedLikedProductsSchema, { timestamps: true, versionKey: false });
const BrandSchema = mongoose.Schema(importedBrandSchema, { timestamps: true, versionKey: false });
const FollowerSchema = mongoose.Schema(importedFollowerSchema, { timestamps: true, versionKey: false });
const NotificationSchema = mongoose.Schema(importedNotificationSchema, { timestamps: true, versionKey: false });
const BidderSchema = mongoose.Schema(importedBidderSchema, { timestamps: true, versionKey: false });
const BlogCategorySchema = mongoose.Schema(importedBlogCategorySchema, { timestamps: true, versionKey: false });
const BlogSchema = mongoose.Schema(importedBlogSchema, { timestamps: true, versionKey: false });

// Creating models
const FaqModel = mongoose.model('faqs', FaqSchema);
const NewsletterModel = mongoose.model('newsletters', NewsletterSchema);
const AdminSettingsModel = mongoose.model('adminsettings', AdminSettingsSchema);
const PageModel = mongoose.model('pages', PageSchema);
const MainCategoryModel = mongoose.model('maincategories', MainCategorySchema);
const SubCategoryModel = mongoose.model('subcategories', SubCategorySchema);
const CategoryModel = mongoose.model('categories', CategorySchema);
const LanguageModel = mongoose.model('languages', LanguageSchema);
const CurrencyModel = mongoose.model('currencies', CurrencySchema);
const UserModel = mongoose.model('users', UserSchema);
const SliderModel = mongoose.model('sliders', SliderSchema);
const ShopModel = mongoose.model('shops', ShopSchema);
const SellerModel = mongoose.model('sellers', SellerSchema);
const ProductModel = mongoose.model('products', ProductSchema);
const SocialMediaModel = mongoose.model('socialmedias', SocialMediaSchema);
const DistrictModel = mongoose.model('districts', DistrictSchema);
const CityModel = mongoose.model('cities', CitySchema);
const CartModel = mongoose.model('carts', CartSchema);
const LikedProductsModel = mongoose.model('likedproducts', LikedProductsSchema);
const BrandModel = mongoose.model('brands', BrandSchema);
const FollowerModel = mongoose.model('followers', FollowerSchema);
const NotificationModel = mongoose.model('notifications', NotificationSchema);
const BidderModel = mongoose.model('bidders', BidderSchema);
const BlogCategoryModel = mongoose.model("blogcategories", BlogCategorySchema);
const BlogModel = mongoose.model("blogs", BlogSchema);

module.exports = {
  faqs: FaqModel,
  newsletters: NewsletterModel,
  adminsettings: AdminSettingsModel,
  pages: PageModel,
  maincategories: MainCategoryModel,
  subcategories: SubCategoryModel,
  categories: CategoryModel,
  languages: LanguageModel,
  currencies: CurrencyModel,
  users: UserModel,
  sliders: SliderModel,
  shops: ShopModel,
  sellers: SellerModel,
  products: ProductModel,
  socialmedias: SocialMediaModel,
  districts: DistrictModel,
  cities: CityModel,
  carts: CartModel,
  likedproducts: LikedProductsModel,
  brands: BrandModel,
  followers: FollowerModel,
  notifications: NotificationModel,
  bidders: BidderModel,
  blogcategories: BlogCategoryModel,
  blogs: BlogModel,
}
