const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const UPLOAD        = require('../../models/fileupload');
const HELPERFUNC    = require('../../models/commonfunctions');

Router.get('/listSliders',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('sliders', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewSlider',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('sliders', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/addUpdateSlider',UPLOAD.single('image'),function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  req.checkBody('description', 'description is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const name        = HELPERFUNC.Capitalize(req.body.name);
  const description = req.body.description;
  const status      = req.body.status;
  const formData = {
    name        : name,
    description : description,
    status      : status
  }
  if(req.file){
    formData.image  = req.file.filename
  } else {
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('sliders', {name : name}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('sliders', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Slider added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('sliders',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('sliders', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id            : result1._id,
                    name          : result1.name,
                    image         : result1.image,
                    description   : result1.description,
                    status        : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Slider updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteSlider',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('sliders', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('sliders', {}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Slider deleted successfully';
                  response.data    = result1;
                  response.count   = result1.length;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
