const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const UPLOAD        = require('../../models/fileupload');

Router.get('/listProducts',function(req,res) {
  const response = {
    status  : 0,
  }
  const options    = {};
  options.populate = 'maincategory subcategory category shop';
  DB.GetDocument('products', {}, {}, options, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewSubcategoriesList',function(req,res) {
  const response = {
    status  : 0,
  }
  if(req.body.type=="subcategories") {
    DB.GetDocument('subcategories', {parentID:req.body.id}, {}, {}, function(err, result) {
        if(err) {
            res.send(response);
        } else {
            response.status  = 1;
            response.data    = result;
            response.count   = result.length;
            res.send(response);
        }
    });
  } else if(req.body.type=="categories"){
    DB.GetDocument('categories', {subcategory:req.body.subcategory}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
  }
});

Router.post('/viewProduct',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('products', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          DB.GetDocument('subcategories', {parentID:result.maincategory}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
              DB.GetDocument('categories', {subcategory:result.subcategory}, {}, {}, function(err, result2) {
                if(err) {
                    res.send(response);
                } else {
                const data = {
                  _id                   : result._id,
                  maincategory          : result.maincategory,
                  subcategory           : result.subcategory,
                  category              : result.category,
                  shop                  : result.shop,
                  city                  : result.city,
                  brand                 : result.brand,
                  title                 : result.title,
                  discount              : result.discount?result.discount:0,
                  discounttype          : result.discounttype?result.discounttype:"",
                  price                 : result.price,
                  discountprice         : result.discountprice,
                  productcondition      : result.productcondition,
                  youtubeurl            : result.youtubeurl,
                  slug                  : result.slug,
                  productaddress        : result.productaddress,
                  productlat            : result.location.lat,
                  productlon            : result.location.lon,
                  deliverystatus        : result.deliverystatus,
                  listingfeature        : result.listingfeature,
                  description           : result.description,
                  status                : result.status,
                  images                : result.images
                }
                response.status        = 1;
                response.data          = data;
                response.subcategories = result1;
                response.categories    = result2;
                res.send(response);
            }
           })
          }
        });
      }
  });
});

Router.post('/addUpdateProduct',UPLOAD.array('images',5),function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('maincategory', 'maincategory is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const formData = {
    maincategory          : req.body.maincategory,
    subcategory           : req.body.subcategory,
    category              : req.body.category,
    title                 : req.body.title,
    discount              : req.body.discount,
    discounttype          : req.body.discounttype,
    price                 : req.body.price,
    discountprice         : req.body.discountprice,
    productcondition      : req.body.productcondition,
    youtubeurl            : req.body.youtubeurl,
    slug                  : req.body.slug,
    city                  : req.body.city,
    brand                 : req.body.brand,
    productaddress        : req.body.productaddress,
    listingfeature        : req.body.listingfeature,
    // location              : {
    //   lat : req.body.productlat,
    //   lon : req.body.productlon,
    // },
    deliverystatus        : req.body.deliverystatus,
    description           : req.body.description,
    status                : req.body.status
  }
  if(req.body.shop){
    formData.shop   = req.body.shop
  }
  if(req.files.length){
    var image = req.files;
    var images = [];
    for (var i = 0; i < image.length; i++) {
      images[i]    = image[i].filename;
    }
    formData.images = images;
  } else {
    formData.images = req.body.images;;
  } 
  if(req.body.id=="0"){
    DB.GetOneDocument('products', {slug:req.body.slug}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('products', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Product added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('products',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('products', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  DB.GetDocument('subcategories', {parentID:result1.maincategory}, {}, {}, function(err, result2) {
                    if(err) {
                        res.send(response);
                    } else {
                      DB.GetDocument('categories', {subcategory:result1.subcategory}, {}, {}, function(err, result3) {
                        if(err) {
                            res.send(response);
                        } else {
                        response.status        = 1;
                        response.data          = result1;
                        response.subcategories = result2;
                        response.categories    = result3;
                        response.message = 'Product updated successfully';
                        res.send(response);
                        }
                      })
                    }
                });
            }
        });
      }
    });
  }
})

Router.post('/deleteProduct',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('products', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        const options    = {};
        options.populate = 'maincategory subcategory category shop';
        DB.GetDocument('products', {}, {}, options, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Product deleted successfully';
                  response.data    = result1;
                  response.count   = result1.length;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
