const express       = require('express');
const Router        = express.Router();
const bcrypt        = require('bcryptjs');
var jwt             = require('jsonwebtoken');
const DB            = require('../../models/db');
const Functions     = require('../../models/commonfunctions');
const UPLOAD        = require('../../models/fileupload');
const MailSend      = require('../../models/mailer.js');
const { socialmedias } = require('../../models/schemaconnection');

var cpUpload = UPLOAD.fields([{ name: 'favicon', maxCount: 1 }, { name: 'logo', maxCount: 1 },{ name: 'icon', maxCount: 1 }])
Router.post('/updateAdminSettings',cpUpload,function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  const settingId  = req.body.id;
  const moduleName = req.body.module;
  var updateInfo = {};
  if(moduleName == 'Site') {
     updateInfo =  { site : {
       sitename          : req.body.sitename,
       copyrights        : req.body.copyrights,
     }};
     if(req.files['favicon']){
       updateInfo.site.favicon  = req.files['favicon'][0].filename
     } else {
       updateInfo.site.favicon  = req.body.favicon
     }
     if(req.files['logo']){
       updateInfo.site.logo  = req.files['logo'][0].filename
     } else {
       updateInfo.site.logo  = req.body.logo
     }
  }
  if(moduleName == 'Socialmedia') {
     updateInfo =  { socialmedia : {
       iconlink      : req.body.iconlink,
     }};
     if(req.files['icon']){
       updateInfo.socialmedia.icon  = req.files['icon'][0].filename
     } else {
       updateInfo.socialmedia.icon  = req.body.icon
     }
  }
  DB.UpdateDocument('adminsettings',{_id:settingId}, updateInfo, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('adminsettings', {_id:settingId}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                response.status  = 1;
                response.message = moduleName + ' settings updated successfully';
                response.data    = result;
                res.send(response);
            }
        });
      }
    });
});

Router.post('/viewAdminsettings',function(req,res) {
  const response  = {
                      status  : 0,
                    }
  if(((new Date().getTime() + 1)/1000)<=(jwt.decode(req.body.token)).exp){
      jwt.verify(req.body.token,'cambosale',(err,data)=>{
        var email = data.user.email;
        DB.GetOneDocument('adminsettings',{email:email}, {}, {}, function(err, result) {
          if(err) {
            res.send(response);
          } else {
                response.status  = 1;
                response.data    = result;
                res.send(response);
            }
        });
      });
  } else{
   response.status=403;
   response.message='Token expired';
   res.send(response);
  }

});

Router.post('/logIn',function(req,res) {
  const email      = req.body.email;
  const password   = req.body.password;
  const user       = {
                      email    : email,
                      password : password,
                     };
  const response = {
     status  : 0,
     message : 'Something went wrong in your code!'
  };
  DB.GetOneDocument('adminsettings', { email:email }, {}, {}, function(err, result) {
        if(err) {
            res.send(response);
          } else {
            if(result) {
              const passswordCheck = bcrypt.compareSync(password,result.password, null);
                if(passswordCheck && result.email == email) {
                 jwt.sign({user},'cambosale',{expiresIn:'10h'} ,(err,token)=>{
                   if(token) {
                     res.send({status:1,message:"Logged in successfully",token:token});
                   }
                 });
               } else {
                  res.send({status:0,message:"Invalid password"});
               }
             } else {
               res.send({status:0,message:"Invalid email"});
             }
      }
  });
});

Router.post('/deleteSocialMedia',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument("socialmedias", {_id:req.body.id}, function(err, result) {
    if(err) {
      res.send(response);
    } else {
      DB.GetDocument("socialmedias", {adminId:req.body.adminId}, {}, {}, function(err, result1) {
        response.status  = 1;
        response.message = 'Social Media deleted successfully';
        response.data    = result1;
        res.send(response);
      });
    }
  });
});

Router.post('/viewSocialMedia',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
    DB.GetDocument("socialmedias", {adminId:req.body.adminId}, {}, {}, function(err, result) {
    response.status  = 1;
    response.message = 'Sucess';
    response.data    = result;
    res.send(response);
  });
});

Router.post('/addEditSocialMedia',UPLOAD.single('icon'),function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  var updateInfo =  {
    adminId           : req.body.adminId,
    iconname          : req.body.iconname,
    iconlink          : req.body.iconlink,
  };
  if(req.file){
    updateInfo.icon  = req.file.filename
  } else {
    updateInfo.icon  = req.body.icon
  }
  if(req.body.id=="0"){
    DB.InsertDocument("socialmedias", updateInfo, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetDocument("socialmedias", {adminId:req.body.adminId}, {}, {}, function(err, result1) {
          response.status  = 1;
          response.message = 'Social Media added successfully';
          response.data    = result1;
          res.send(response);
        });
      }
    });
  } else {
        DB.FindUpdateDocument("socialmedias",{_id:req.body.id}, updateInfo, function(err, result) {
          if(err) {
            res.send(response);
          } else {
            DB.GetDocument("socialmedias", {adminId:req.body.adminId}, {}, {}, function(err, result1) {
              response.status  = 1;
              response.message = 'Social Media updated successfully';
              response.data    = result1;
              res.send(response);
            });
          }
        });
      }
});

Router.post('/changePassword',function(req,res) {
  const password     = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
  const response  = {
                      status  : 0,
                      message : 'Something went wrong in your code!'
                    }
  jwt.verify(req.body.token,'cambosale',(err,data)=>{
    var email = data.email;
    DB.UpdateDocument('adminsettings',{email:email}, {password:password}, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        if(result.n){
            response.status  = 1;
            response.message = 'Password changed successfully';
            res.send(response);
        }
        else {
          res.send(response);
        }
      }
    });
  });
});

Router.post('/forgotPassword',function(req,res) {
  let fromMail       = 'abarna@gmail.com';
  let toMail         = req.body.email;
  let subject        = "Change password";
  let text           = "";
  const email        = req.body.email;

  DB.GetOneDocument('adminsettings', {email:req.body.email}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        jwt.sign({email},'cambosale',{} ,(err,token)=>{
          if(token) {
            if(result){
                 var html = "<p>Hello user!</p><p>&nbsp; &nbsp; To change your password use this link&nbsp;<a href=\"http://localhost:3000/change-password?token="+token+ "\" target=\"_blank\">Reset Password</a><br></p>"
                 let mailOptions = {
                     from    : fromMail,
                     to      : toMail,
                     subject : subject,
                     html    : html
                 };
                 MailSend.MailSend(mailOptions,function(error,response){
                    if(error){
                      res.send({status: 0 , message : "Retry again"})
                    }
                    else {
                      res.send({status: 1 , message : "Check your email to chage password"})
                    }
                 });
            }
            else {
              res.send({status: 0 , message : "Email not registred"})
           }
          }
        });

     }
  });
});

module.exports = Router;
