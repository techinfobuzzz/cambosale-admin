const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const HELPERFUNC    = require('../../models/commonfunctions');

Router.get('/listUsers',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('users', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          // const data = []
          // for(var i=0;i<result.length;i++){
          //   const dataObj = {
          //     name     : result[i].name,
          //     email    : result[i].email,
          //     location : result[i].location
          //   };
          //   data.push(dataObj)
          // }
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewUser',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('users', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});


Router.post('/deleteUser',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('users', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('users', {}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'User deleted successfully';
                  response.data    = result1;
                  response.count   = result1.length;
                  res.send(response);
            }
        });
      }
  });
})
module.exports = Router;
