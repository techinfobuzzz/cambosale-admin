const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const HELPERFUNC    = require('../../models/commonfunctions');

Router.get('/listNewsletters',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('newsletters', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
            response.status  = 1;
            response.data    = result;
            response.count   = result.length;
            res.send(response);
      }
  });
});

Router.post('/viewNewsletter',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('newsletters', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
            response.status  = 1;
            response.data    = result;
            res.send(response);
      }
  });
});

Router.post('/addUpdateNewsletter',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('templatename', 'templatename is required.').notEmpty();
  req.checkBody('emailsubject', 'emailsubject is required.').notEmpty();
  req.checkBody('emailcontent', 'emailcontent is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const templatename  = req.body.templatename;
  const emailsubject  = req.body.emailsubject;
  const emailcontent  = req.body.emailcontent;
  const status        = req.body.status;
  const newsletterFormData = {
    templatename  : HELPERFUNC.Capitalize(templatename),
    emailsubject  : HELPERFUNC.Capitalize(emailsubject),
    emailcontent  : HELPERFUNC.Capitalize(emailcontent),
    status        : status
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('newsletters', {templatename : templatename}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('newsletters', newsletterFormData, function(err, result1) {
          if(err) {
            console.log("==",err);
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Newsletter added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('newsletters',{_id:req.body.id}, newsletterFormData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('newsletters', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const pagesData = {
                    id             : result1._id,
                    templatename   : result1.templatename,
                    emailsubject   : result1.emailsubject,
                    emailcontent   : result1.emailcontent,
                    status         : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Newsletter updated successfully';
                  response.data    = pagesData;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteNewsletter',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('newsletters', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('newsletters', {}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Newsletter deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
