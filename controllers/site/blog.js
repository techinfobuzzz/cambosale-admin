const express       = require('express');
const Router        = express.Router();
const async         = require("async");
const DB            = require('../../models/db');

Router.post('/viewBlog',function(req,res) {
  DB.GetOneDocument('blogs', {slug:req.body.slug}, {}, {}, function(err, result) {
    if(result){
      async.parallel([
        function(callback) {
          DB.GetDocument('blogs', {blogcategory:result.blogcategory}, {}, {}, function(err, blogs) {
                  callback(null,blogs);
          });
        },
      ],
      function(err, results) {
        const response = {
          relatedblogs   : results[0],
          blog           : result,
        }
        res.send({status:1,data:response})
      });
    }
  });
})

module.exports = Router;
