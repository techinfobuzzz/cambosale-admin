const express = require('express');
const Router = express.Router();
const async = require("async");
const DB = require('../../models/db');

Router.post('/dashboard', function (req, res) {
  async.parallel([
    function (callback) {
      DB.GetDocument('maincategories', { status: 1 }, {}, {}, function (err, categoriesresult) {
        callback(null, categoriesresult);
      })
    },
    function (callback) {
      var query = {}
      if (req.body.shopId) {
        query = { _id: { $ne: req.body.shopId } }
      }
      DB.GetDocument('shops', query, {}, {}, function (err, shops) {
        callback(null, shops);
      })
    },
    function (callback) {
      const options = {};
      options.populate = 'city shop user';
      const query1 = [{ listingfeature: "latest" }]
      if(req.body.id){
        query1.push({user: { $ne: req.body.id }})
      }
      DB.GetDocument('products', { $and: query1 }, {}, options, function (err, latest) {
        callback(null, latest);
      })
    },
    function (callback) {
      const options = {};
      options.populate = 'city shop user';
      const query2 = [{ listingfeature: "popular" }]
      if(req.body.id){
        query2.push({user: { $ne: req.body.id }})
      }
      DB.GetDocument('products', { $and: query2 }, {}, options, function (err, popular) {
        callback(null, popular);
      })
    },
    function (callback) {
      const options = {};
      options.populate = 'city shop user';
      const query3 = [{ listingfeature: "bidding" }]
      if(req.body.id){
        query3.push({user: { $ne: req.body.id }})
      }
      DB.GetDocument('products', { $and: query3 }, {}, options, function (err, bidding) {
        callback(null, bidding);
      })
    },
    function (callback) {
      var options = { populate: "shop city user" }
      const query4 = [{ listingfeature: "discount" }]
      if(req.body.id){
        query4.push({user: { $ne: req.body.id }})
      }
      DB.GetDocument('products', { $and: query4 }, {}, options, function (err, discount) {
        callback(null, discount);
      })
    },
    function (callback) {
      DB.GetDocument('cities', { status: 1 }, {}, {}, function (err, citiesresult) {
        callback(null, citiesresult);
      })
    },
    function (callback) {
      DB.GetDocument('districts', { status: 1 }, {}, {}, function (err, districtsresult) {
        callback(null, districtsresult);
      })
    },
    function (callback) {
      const Query = [
        { $match: { status: true } },
        { $sort: { sort: 1 } },
      ];
      DB.GetAggregation('categories', Query, {}, function (err, categories) {
        callback(null, categories);
      });
    },
    function (callback) {
      DB.GetDocument('categories', { status: 1 }, {}, {}, function (err, categoriesres) {
        callback(null, categoriesres);
      })
    },
    function (callback) {
      DB.GetDocument('brands', { status: 1 }, {}, {}, function (err, brands) {
        callback(null, brands);
      })
    },
    function (callback) {
      const options = {};
      options.populate = 'productid userid ownerid';
      DB.GetDocument('notifications', { userid: req.body.id }, {}, options, function (err, brands) {
        callback(null, brands);
      })
    },
    function (callback) {
      DB.GetDocument('sliders', { status: 1 }, {}, {}, function (err, sliders) {
        callback(null, sliders);
      })
    },
    function (callback) {
      DB.GetDocument('blogs', { status: 1 }, {}, {}, function (err, blogs) {
        callback(null, blogs);
      })
    },
    function (callback) {
      DB.GetDocument('blogs', { feature: "trending" }, {}, {}, function (err, trendingblogs) {
        callback(null, trendingblogs);
      })
    },
    function (callback) {
      DB.GetDocument('blogs', { feature: "popular" }, {}, {}, function (err, popularblogs) {
        callback(null, popularblogs);
      })
    },
    function (callback) {
      DB.GetRecentDocumet('blogs', { sort: -1 }, {}, {}, function (err, recentblogs) {
        callback(null, recentblogs);
      })
    },
  ],
    function (err, results) {
      const response = {
        maincategories: results[0],
        shops: results[1],
        latest: results[2],
        popular: results[3],
        bidding: results[4],
        discount: results[5],
        cities: results[6],
        districts: results[7],
        popularcategories: results[8],
        categories: results[9],
        brands: results[10],
        notifications: results[11],
        sliders: results[12],
        blogs: results[13],
        trendingblogs: results[14],
        popularblogs: results[15],
        recentblogs: results[16],
      }
      res.send({ status: 1, data: response })
    });
});

Router.post('/listCategoriesDashboard', function (req, res) {
  const response = {
    status: 0,
  }
  DB.GetDocument('categories', { $and: [{ maincategory: req.body.maincategory }, { status: true }] }, {}, {}, function (err, result) {
    if (err) {
      res.send(response);
    } else {
      response.status = 1;
      response.data = result;
      response.count = result.length;
      res.send(response);
    }
  });
});

Router.post('/productList', function (req, res) {
  const response = {
    status: 0,
  }
  // const options    = {};
  // options.populate = 'maincategory subcategory';
  DB.GetDocument('products', { title: { $regex: req.body.value, $options: "i" } }, {}, {}, function (err, result) {
    if (err) {
      res.send(response);
    } else {
      response.status = 1;
      response.data = result;
      response.count = result.length;
      res.send(response);
    }
  });
});

module.exports = Router;
