const express       = require('express');
const Router        = express.Router();
const bcrypt        = require('bcryptjs');
var jwt             = require('jsonwebtoken');
const DB            = require('../../models/db');

Router.post('/logIn',function(req,res) {
    req.checkBody('phonenumber', 'Phonenumber is required.').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        return res.status(422).json({ errors: errors});
    }
  const phonenumber = req.body.phonenumber;
  const password    = req.body.password;

  const response = {
     status  : 0,
     message : 'Something went wrong in your code!'
  };
  DB.GetOneDocument('users', { phonenumber:phonenumber }, {}, {}, function(err, result) {
        if(err) {
            res.send(response);
          } else {
            if(result) {
              const user = {
                userId      : result._id,
                phonenumber : phonenumber
              }
              const passswordCheck = bcrypt.compareSync(password,result.password, null);
                if(passswordCheck && result.phonenumber == phonenumber) {
                 jwt.sign(user,'cambosale',{} ,(err,token)=>{
                   if(token) {
                     res.send({status:1,message:"Logged in successfully",token:token,data:result});
                   }
                 });
               } else {
                  res.send({status:0,message:"Invalid password"});
               }
             } else {
               res.send({status:0,message:"Invalid Phone number"});
             }
      }
  });
});

Router.post('/registerAccount',function(req,res) {
    const response = {
      status  : 0,
      message : 'Something went wrong in your code!'
    }
    req.checkBody('phonenumber', 'Phonenumber is required.').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        return res.status(422).json({ errors: errors});
    }
    const password     = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    const formData ={
      phonenumber  : req.body.phonenumber,
      password     : password,
      email        : req.body.email,
      fullname     : req.body.fullname,
      verification : {}      
    }
  DB.GetOneDocument('users', {phonenumber:req.body.phonenumber}, {}, {}, function(err, result) {
    if(err) {
      res.send(response);
      } else {
        if(result){
          response.message="Phone number already registered";
          res.send(response);
        }
        else{
          DB.InsertDocument('users', formData, function(err, result) {
            if(err) {
              res.send(response);
            } else {
              const user = {
                userId      : result._id,
                phonenumber : req.body.phonenumber
              }
              jwt.sign(user,'cambosale',{} ,(err,token)=>{
                if(token) {
                  response.status  = 1;
                  response.message = 'Account created successfully';
                  response.token   = token;
                  response.data    = result;
                  res.send(response);
                }
              });
           }
        });
       }
      }
    });
});

module.exports = Router;
