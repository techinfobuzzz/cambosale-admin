const express = require('express');
const Router = express.Router();
var mongoose = require('mongoose');
const async = require("async");
const DB = require('../../models/db');
const UPLOAD = require('../../models/fileupload');

Router.post('/listProducts', function (req, res) {
  const response = {
    status: 0,
  }
  var queryval = []
  if(req.body.categories&&req.body.categories.length){
      var arr = []
      for (var i=0;i<req.body.categories.length;i++){
        arr.push({"category":new mongoose.Types.ObjectId(req.body.categories[i])})
      }
      queryval.push({$or:arr})
  }
  if(req.body.brands&&req.body.brands.length){
    var arr = []
    for (var i=0;i<req.body.brands.length;i++){
      arr.push({"brand":new mongoose.Types.ObjectId(req.body.brands[i])})
    }
      queryval.push({$or:arr})
  }
  if(req.body.cities&&req.body.cities.length){
    var arr = []
    for (var i=0;i<req.body.cities.length;i++){
      arr.push({"city":new mongoose.Types.ObjectId(req.body.cities[i])})
    }
      queryval.push({$or:arr})
  }
  if(req.body.productcondition&&req.body.productcondition.length){
    var arr = []
    for (var i=0;i<req.body.productcondition.length;i++){
      arr.push({"productcondition":req.body.productcondition[i]})
    }
      queryval.push({$or:arr})
  }
  if(req.body.deliverystatus){
      queryval.push({$or:[{"deliverystatus":"free"}]})
  }
  if(req.body.discount){
    queryval.push({$or:[{"discounttype":"%"},{"discounttype":"flat"}]})
  }
  if(req.body.maxprice){
    queryval.push({discountprice:{"$lte":parseInt(req.body.maxprice)}})
  }
  if(req.body.minprice){
    queryval.push({discountprice:{"$gte":parseInt(req.body.minprice)}})
  }
  var match = { user :{ $ne:  new mongoose.Types.ObjectId(req.body.user)},shop :{ $ne:  new mongoose.Types.ObjectId(req.body.shop)}}
  if(queryval.length){
    var match = { user :{ $ne:  new mongoose.Types.ObjectId(req.body.user)},shop :{ $ne:  new mongoose.Types.ObjectId(req.body.shop)},
    $and:queryval }
  } 
  const Query = [
    { $match: match},
  ];
  const options = {};
  options.populate = 'maincategory subcategory category shop user';
  DB.GetAggregation('products', Query,  options , function (err, result) {
    if (err) {
      res.send(response);
    } else {
      response.status = 1;
      response.data = result?result:[];
      res.send(response);
    }
  });
});

Router.post('/viewProduct', function (req, res) {
  const response = {
    status: 0,
  }
  const options = {};
  options.populate = 'maincategory subcategory category user shop brand';
  DB.GetOneDocument('products', { slug: req.body.slug }, {}, { options }, function (err, result) {
    // console.log('=---result---', result);
    if (err) {
      res.send(response);
    } else {
      if(result){
      var query = {}
      if (req.body.userId) {
        query = { $and: [{ productId: result._id }, { userId: mongoose.Types.ObjectId(req.body.userId) }] }
      }
      DB.GetOneDocument('likedproducts', query, {}, {}, function (err, result1) {
        if (err) {
          res.send(response);
        } else {
            response.status = 1;
            response.data   = result;
            if (req.body.userId) {
              response.liked = result1 ? result1 : {};
            } else {
              response.liked = {};
            }
            res.send(response);
        }
      })
     }
    }
  });
});

Router.post('/viewPostedAdd', function (req, res) {
  const response = {
    status: 0,
  }
  DB.GetOneDocument('products', { slug: req.body.slug }, {}, { }, function (err, result) {
    if (err) {
      res.send(response);
    } else {
        response.status = 1;
        response.data   = result;
        res.send(response);
    }
  });
});

Router.post('/viewProductDetails', function (req, res) {
  DB.GetOneDocument('products', { slug: req.body.slug }, {}, {}, function (err, result) {
    if (err) {
      res.send(response);
    } else {
      var query1 = {}
      var query2 = {}
      var query3 = {}
      if (result.shop) {
        query1 = { shop: result.shop }
        query2 = { shopfollowing: result.shop }
        query3 = { shopfollower: result.shop }
      } else {
        query1 = { user: result.user }
        query2 = { userfollowing: result.user }
        query3 = { userfollower: result.user }
      }
      async.parallel([
        function (callback) {
          DB.GetDocument('products', query1, {}, {}, function (err, productsresult) {
            callback(null, productsresult);
          });
        },
        function (callback) {
          DB.GetDocument('followers', query2, {}, {}, function (err, followers) {
            callback(null, followers);
          });
        },
        function (callback) {
          DB.GetDocument('followers', query3, {}, {}, function (err, following) {
            callback(null, following);
          });
        },
        function (callback) {
          const options={populate:"user"}
          DB.GetDocument('bidders', { product:result._id }, {}, options, function (err, bidders) {
            callback(null, bidders);
          });
        },
        function (callback) {
          if(req.body.user){
            // const options={populate:"user"}
            DB.GetOneDocument('bidders', { $and: [{ product:result._id },{user:req.body.user}]}, {}, {}, function (err, userbidding) {
              callback(null, userbidding);
            });
          } else {
            callback(null, {});
          }
        },
      ],
        function (err, results) {
          const response = {
            products: results[0].length,
            followers: results[1].length,
            following: results[2].length,
            bidders: results[3],
            userbidding: results[4],
          }
          res.send({ status: 1, data: response })
        });

    }
  });
});

Router.post('/relatedProducts', function (req, res) {
  const response = {
    status: 0,
  }
  const options = {};
  options.populate = 'maincategory subcategory category user shop brand city';
  DB.GetDocument('products', { $and: [{ category: req.body.category }, { _id: { $ne: req.body.productId } }] }, {}, { options }, function (err, result) {
    if (err) {
      res.send(response);
    } else {
      response.status = 1;
      response.data = result;
      res.send(response);
    }
  });
});

Router.post('/moreProducts', function (req, res) {
  const response = {
    status: 0,
  }
  const options = {};
  options.populate = 'maincategory subcategory category user shop brand city';
  DB.GetDocument('products', { $and: [{ shop: req.body.shop }, { _id: { $ne: req.body.productId } }] }, {}, { options }, function (err, result) {
    if (err) {
      res.send(response);
    } else {
      response.status = 1;
      response.data = result;
      res.send(response);
    }
  });
});

Router.post('/addLikedProduct', function (req, res) {
  const response = {
    status: 0,
    message: 'Something went wrong in your code!'
  }
  req.checkBody('userId', 'userId is required.').notEmpty();
  req.checkBody('productId', 'productId is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors });
  }
  const formData = {
    userId      : req.body.userId,
    productId   : req.body.productId,
    productUser : req.body.productUser,
  }
  if(req.body.productShop){
    formData.productShop =  req.body.productShop
  }
  DB.InsertDocument('likedproducts', formData, function (err, result1) {
    if (err) {
      res.send(response);
    } else {
      DB.GetOneDocument('products', { _id: req.body.productId }, {}, {}, function (err, result2) {
        if (err) {
          res.send(response);
        } else {
          response.status = 1;
          response.message = 'Added to liked products';
          response.data = result2;
          response.liked = result1;
          res.send(response);
        }
      })
    }
  });
})

Router.post('/addUpdateBidding', function (req, res) {
  const response = {
    status: 0,
    message: 'Something went wrong in your code!'
  }
  req.checkBody('user', 'user is required.').notEmpty();
  req.checkBody('product', 'product is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors });
  }
  const formData = {
    user           : req.body.user,
    product        : req.body.product,
    amount         : req.body.amount,
  }
  if(req.body.biddingid=="0"){
    DB.InsertDocument('bidders', formData, function (err, result1) {
      if (err) {
        res.send(response);
      } else {
        async.parallel([
          function (callback) {
            const options={populate:"user"}
            DB.GetDocument('bidders', { product:req.body.product }, {}, options, function (err, bidders) {
              callback(null, bidders);
            });
          },
          function (callback) {
              // const options={populate:"user"}
              DB.GetOneDocument('bidders', { $and: [{ product:req.body.product },{user:req.body.user}]}, {}, {}, function (err, userbidding) {
                callback(null, userbidding);
              });
          },
        ],
          function (err, results) {
            const response = {
              bidders: results[0],
              userbidding: results[1],
            }
            res.send({ status: 1, message:"Bidding Added",data: response })
          });
      }
    });
  } else {
    DB.FindUpdateDocument('bidders', {_id:req.body.biddingid},formData, function (err, result1) {
      if (err) {
        res.send(response);
      } else {
        async.parallel([
          function (callback) {
            const options={populate:"user"}
            DB.GetDocument('bidders', { product:req.body.product }, {}, options, function (err, bidders) {
              callback(null, bidders);
            });
          },
          function (callback) {
              // const options={populate:"user"}
              DB.GetOneDocument('bidders', { $and: [{ product:req.body.product },{user:req.body.user}]}, {}, {}, function (err, userbidding) {
                callback(null, userbidding);
              });
          },
        ],
          function (err, results) {
            const response = {
              bidders: results[0],
              userbidding: results[1],
            }
            res.send({ status: 1, message:"Bidding Updated",data: response })
          });
      }
    });
  }
})

Router.post('/deleteLikedProduct', function (req, res) {
  const response = {
    status: 0,
    message: 'Something went wrong in your code!'
  }
  DB.DeleteDocument('likedproducts', { _id: req.body.id }, function (err, result) {
    if (err) {
      res.send(response);
    } else {
      var options = {}
      options.populate = 'maincategory subcategory category user shop brand';
      DB.GetOneDocument('products', { _id: req.body.productId }, {}, options, function (err, result2) {
        if (err) {
          res.send(response);
        } else {
          response.status = 1;
          response.message = 'Removed from liked products';
          response.data = result2;
          response.liked = {};
          res.send(response);
        }
      })
    }
  });
})

Router.post('/addProduct', UPLOAD.array('images', 5), function (req, res) {
  const response = {
    status: 0,
    message: 'Something went wrong in your code!'
  }
  req.checkBody('maincategory', 'maincategory is required.').notEmpty();
  req.checkBody('title', 'title is required.').notEmpty();
  req.checkBody('user', 'User is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors });
  }
  const formData = {
    maincategory     : req.body.maincategory,
    subcategory      : req.body.subcategory,
    category         : req.body.category,
    title            : req.body.title,
    user             : req.body.user,
    discount         : req.body.discount,
    discounttype     : req.body.discounttype,
    price            : req.body.price,
    discountprice    : req.body.discountprice,
    productcondition : req.body.productcondition,
    youtubeurl       : req.body.youtubeurl,
    productaddress   : req.body.productaddress,
    listingfeature   : req.body.listingfeature,
    city             : req.body.city,
    district         : req.body.district,
    deliverystatus   : req.body.deliverystatus,
    description      : req.body.description,
    status           : req.body.status,
    brand            : req.body.brand,
    phonenumber      : req.body.phonenumber,
    minimumbid       : req.body.minimumbid,
    maximumbid       : req.body.maximumbid,
    bidstart         : req.body.bidstart,
    bidend           : req.body.bidend,
  }
  if (req.body.shop) {
    formData.shop = req.body.shop;
  }
  if (req.files.length) {
    var image = req.files;
    var images = [];
    for (var i = 0; i < image.length; i++) {
      images[i] = image[i].filename;
    }
    formData.images = images;
  } else {
    formData.images = req.body.images;;
  }
  var slug = req.body.title;
    slug = slug.replace(/[^\w\-]+/g, "-");
    slug = slug.toLowerCase();
    formData.slug = slug;
  DB.GetOneDocument('products', {title:req.body.title}, {}, {}, function(err, result) {
    if(result){
      response.status  = 0;
      response.message = 'Title you entered already exist!';
      res.send(response);
    } else {
        DB.InsertDocument('products', formData, function (err, result1) {
          if (err) {
            res.send(response);
          } else {
            response.status = 1;
            response.message = 'Product added successfully';
            response.id = result1._id;
            sendNotification(req.body.shop)
            res.send(response);
          }
        });
      }
  });
})

Router.post('/EditProduct', UPLOAD.array('images', 5), function (req, res) {
  const response = {
    status: 0,
    message: 'Something went wrong in your code!'
  }
  req.checkBody('maincategory', 'maincategory is required.').notEmpty();
  req.checkBody('title', 'title is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors });
  }
  const formData = {
    maincategory     : req.body.maincategory,
    subcategory      : req.body.subcategory,
    category         : req.body.category,
    title            : req.body.title,
    discount         : req.body.discount,
    discounttype     : req.body.discounttype,
    price            : req.body.price,
    discountprice    : req.body.discountprice,
    productcondition : req.body.productcondition,
    youtubeurl       : req.body.youtubeurl,
    productaddress   : req.body.productaddress,
    city             : req.body.city,
    district         : req.body.district,
    deliverystatus   : req.body.deliverystatus,
    description      : req.body.description,
    brand            : req.body.brand,
    phonenumber      : req.body.phonenumber,
    minimumbid       : req.body.minimumbid,
    maximumbid       : req.body.maximumbid,
    bidstart         : req.body.bidstart,
    bidend           : req.body.bidend,
  }
  
  if (req.files.length) {
    var image = req.files;
    var images = [];
    for (var i = 0; i < image.length; i++) {
      images[i] = image[i].filename;
    }
    formData.images = images;
  } else {
    formData.images = req.body.images;;
  }
  DB.FindUpdateDocument('products', {_id:req.body.productId},formData, function (err, result1) {
    if (err) {
      res.send(response);
    } else {
      response.status = 1;
      response.message = 'Product updated successfully';
      res.send(response);
    }
  });
})

function sendNotification(shopOrUserId, productid) {
  const response = {
    status: 0,
    message: 'Something went wrong in your code!'
  }
  // const productid = '60f95b40ee5ad30c2c67f816';
  // const shopOrUserId = req.body.shopOrUserId;
  DB.GetDocument('followers', { 'shopfollowing': shopOrUserId }, 'userfollower', {}, function (err, followers) {
    followers.map((follower, index) => {
      const formNotificationData = {
        userid: follower.userfollower,
        seen: false,
        productid: productid,
        ownerid: shopOrUserId
      }
      DB.InsertDocument('notifications', formNotificationData, function (err, result) {
      });
    });
  })
}

module.exports = Router;
