const express       = require('express');
const Router        = express.Router();
var jwt             = require('jsonwebtoken');
const async         = require("async");
const DB            = require('../../models/db');
const UPLOAD        = require('../../models/fileupload');

Router.post('/viewUserProfile',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
    DB.GetOneDocument('users', {_id:req.body.id}, {}, {}, function(err, result1) {
          if(err) {
              res.send(response);
          } else {
                var resultData = {}
                if(result1){
                  resultData = {
                    id            : result1._id,
                    fullname      : result1.fullname?result1.fullname:"",
                    description   : result1.description?result1.description:"",
                    phonenumber   : result1.phonenumber,
                    email         : result1.email?result1.email:"",
                    address       : result1.address?result1.address:"",
                    location      : result1.location?result1.location:"",
                    profileimage  : result1.profileimage?result1.profileimage:"",
                    coverpicture  : result1.coverpicture?result1.coverpicture:"",
                    verification  : result1.verification
                  }
                }
                response.status  = 1;
                response.message = 'user shown successfully';
                response.data    = resultData;
              res.send(response);
          }
      });
});

Router.post('/viewListings',function(req,res) {
  async.parallel([
    function(callback) {
      const options    = {};
      options.populate = 'maincategory subcategory category shop brand';
      DB.GetDocument('products', {user:req.body.id}, {}, options, function(err, productsresult) {
              callback(null,productsresult);
      });
    },
    function(callback) {
      const options = { populate : "productId productUser productShop" }
      DB.GetDocument('likedproducts', { userId : req.body.id }, {}, options , function(err, likedproducts) {
        callback(null, likedproducts);
      });
    },
    function(callback) {
      const options = { populate : "shopfollower userfollower" }
      DB.GetDocument('followers', { userfollowing : req.body.id }, {}, options , function(err, followers) {
        callback(null, followers);
      });
    },
    function(callback) {
      const options = { populate : "shopfollowing userfollowing" }
      DB.GetDocument('followers', { userfollower : req.body.id }, {}, options , function(err, following) {
        callback(null, following);
      });
    },
  ],
  function(err, results) {
    const response = {
      products     : results[0],
      liked        : results[1],
      followers    : results[2],
      following    : results[3],
    }
    res.send({status:1,data:response})
  });

});

Router.post('/deleteLikedProduct',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('likedproducts', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          const options = { populate : "productId productUser productShop " }
          DB.GetDocument('likedproducts', {userId:req.body.userId}, {}, options, function(err, result2) {
              if(err) {
                  res.send(response);
              } else {
                response.status        = 1;
                response.message       = 'Removed from liked products';
                response.data          = result2;
                res.send(response);
              }
            })
      }
  });
})

Router.post('/deleteListing',function(req,res) {
    const response = {
      status  : 0,
      message : 'Something went wrong in your code!'
    }
    DB.DeleteDocument('products', {_id:req.body.id}, function(err, result) {
        if(err) {
            res.send(response);
        } else {
          const options    = {};
          options.populate = 'maincategory subcategory category shop';
          DB.GetDocument('products', {user:req.body.user}, {}, options, function(err, result1) {
              if(err) {
                  res.send(response);
              } else {
                    response.status  = 1;
                    response.message = 'Product deleted successfully';
                    response.data    = result1;
                    res.send(response);
              }
          });
        }
    });
})

Router.post('/deleteShopListing',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('products', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        const options    = {};
        options.populate = 'maincategory subcategory category shop';
        DB.GetDocument('products', {shop:req.body.shop}, {}, options, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Product deleted successfully';
                  response.data    = result1;
                  res.send(response);
            }
        });
      }
  });
})

Router.post('/viewShopListing',function(req,res) {
  async.parallel([
    function(callback) {
      const options    = {};
      options.populate = 'maincategory subcategory category shop brand';
      DB.GetDocument('products', {shop:req.body.shop}, {}, options, function(err, productsresult) {
              callback(null,productsresult);
      });
    },
    function(callback) {
      const options = { populate : "shopfollower userfollower" }
      DB.GetDocument('followers', { shopfollowing : req.body.shop }, {}, options , function(err, followers) {
        callback(null, followers);
      });
    },
    function(callback) {
      const options = { populate : "shopfollowing userfollowing" }
      DB.GetDocument('followers', { shopfollower : req.body.shop }, {}, options , function(err, following) {
        callback(null, following);
      });
    },
  ],
  function(err, results) {
    const response = {
      products     : results[0],
      followers    : results[1],
      following    : results[2],
    }
    res.send({status:1,data:response})
  });
})

Router.post('/viewUserShop',function(req,res) {
  const response = {
    status  : 0,
  }
  jwt.verify(req.body.token,'cambosale',(err,data)=>{
    var phonenumber = data.phonenumber;
    DB.GetOneDocument('shops', {contactnumber:phonenumber}, {}, {}, function(err, result1) {
          if(err) {
              res.send(response);
          } else {
                response.status  = 1;
                response.data    = result1?result1:{};
                res.send(response);
          }
      });
  });
});

Router.post('/editUserProfile',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  const data = {}
  if(req.body.fullname){
    data.fullname = req.body.fullname
  }
  if(req.body.description){
    data.description = req.body.description
  }
  if(req.body.address){
    data.address = req.body.address
  }
  if(req.body.email){
    data.email = req.body.email
  }
  if(req.body.location){
    data.location     = {
      lat : req.body.location.lat,
      lon : req.body.location.lng,
    }
    data.verification = {}
    if(req.body.verification.emailVerification == true || req.body.verification.emailVerification == false){
      data.verification.emailVerification = req.body.verification.emailVerification
    }
    if(req.body.verification.facebookVerification == true || req.body.verification.facebookVerification == false ){
      data.verification.facebookVerification = req.body.verification.facebookVerification
    }
    if(req.body.verification.phonenumberVerification == true || req.body.verification.phonenumberVerification == false){
      data.verification.phonenumberVerification = req.body.verification.phonenumberVerification
    }

  }
  DB.FindUpdateDocument('users',{_id:req.body.id}, data, function(err, result) {
    if(err) {
      res.send(response);
    } else {
      DB.GetOneDocument('users', {_id:req.body.id}, {}, {}, function(err, result1) {
          if(err) {
              res.send(response);
          } else {
                const resultData = {
                  id            : result1._id,
                  fullname      : result1.fullname?result1.fullname:"",
                  description   : result1.description?result1.description:"",
                  phonenumber   : result1.phonenumber,
                  email         : result1.email?result1.email:"",
                  address       : result1.address?result1.address:"",
                  location      : result1.location?result1.location:"",
                  verification  : result1.verification

                }
                response.status  = 1;
                response.message = 'User updated successfully';
                response.data    = resultData;
              res.send(response);
          }
      });
    }
  });
})

Router.post('/editShopProfile',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  const data = {}
  if(req.body.description){
    data.description = req.body.description
  }
  DB.FindUpdateDocument('shops',{_id:req.body.id}, data, function(err, result) {
      if(err) {
        res.send(response);
      } else {
          response.status  = 1;
          response.message = 'Shop updated successfully';
          res.send(response);
      }
  });
})

var cpUpload = UPLOAD.fields([{ name: 'userprofileimage', maxCount: 1 }, { name: 'usercoverpicture', maxCount: 1 },
                              { name: 'shopprofileimage', maxCount: 1 }, { name: 'shopcoverpicture', maxCount: 1 }])
Router.post('/updateProfileImages',cpUpload,function(req,res) {
  const response   = {
    status  : 0,
    message : 'Something went wrong in your code!'
   };
  var updateInfo = {};
  if(req.body.modulename == 'userprofile') {
     updateInfo =  { profileimage : req.files['userprofileimage'][0].filename };
  } else if (req.body.modulename == 'usercoverpicture'){
    updateInfo =  { coverpicture : req.files['usercoverpicture'][0].filename };
  } else if (req.body.modulename == 'shopcoverpicture'){
    updateInfo =  { coverpicture : req.files['shopcoverpicture'][0].filename };
  } else if (req.body.modulename == 'shopprofile'){
    updateInfo =  { image : req.files['shopprofileimage'][0].filename };
  }

  if(req.body.modulename == 'userprofile'||req.body.modulename == 'usercoverpicture'){
      DB.UpdateDocument('users',{_id:req.body.userId}, updateInfo, function(err, result) {
          if(err) {
            res.send(response);
          } else {
            DB.GetOneDocument('users', {_id:req.body.userId}, {}, {}, function(err, result1) {
              if(err) {
                  res.send(response);
              } else {
                    var resultData = {}
                    if(result1){
                      resultData = {
                        id            : result1._id,
                        fullname      : result1.fullname?result1.fullname:"",
                        description   : result1.description?result1.description:"",
                        phonenumber   : result1.phonenumber,
                        email         : result1.email?result1.email:"",
                        address       : result1.address?result1.address:"",
                        location      : result1.location?result1.location:"",
                        profileimage  : result1.profileimage?result1.profileimage:"",
                        coverpicture  : result1.coverpicture?result1.coverpicture:""
                      }
                    }
                    response.status  = 1;
                    response.message = 'Images updated successfully';
                    response.data    = resultData;
                  res.send(response);
              }
          });
          }
        });
      } else {
        DB.UpdateDocument('shops',{_id:req.body.shopId}, updateInfo, function(err, result) {
          if(err) {
            res.send(response);
          } else {
              DB.GetOneDocument('shops', {_id:req.body.shopId}, {}, {}, function(err, result1) {
                    if(err) {
                        res.send(response);
                    } else {
                          response.status  = 1;
                          response.data    = result1;
                          res.send(response);
                    }
                });
          }
        });
      }
});


module.exports = Router;
