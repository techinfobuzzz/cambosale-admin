const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');

Router.post('/IncrementCount',function(req,res) {
    const response = {
      status  : 0,
      message : 'Something went wrong in your code!'
    }
    
    const userid              = req.body.userid;
    const productid           = req.body.productid;
    const discountapplied     = req.body.discountapplied;
    var quantity              = req.body.quantity;
    var price                 = req.body.price;
    const cartid              = req.body.cartid;

     const CartData = {
        userid: userid,
        productid: productid,
        price    :price,
        discountapplied: discountapplied,
        quantity : quantity
      }
        DB.FindUpdateDocument('carts',{_id:cartid}, CartData, function(err, result) {
           if(err) {
             res.send(response);
           } else {
            const options    = {};
            options.populate = 'productid';
              DB.GetDocument('carts', {userid:userid}, {}, {options}, function(err, result1) {
                  if(err) {
                      res.send(response);
                  } else {
                    if(result1.length == 0) {
                        response.status  = 2;
                        response.message = 'no data found';
                      } else {
                        const cartoutput=[];
                        for(var i=0;i<result1.length;i++){
                            const cartid          = result1[i]._id;
                            const productid       = result1[i].productid._id;
                            const title           = result1[i].productid.title;
                            const description     = result1[i].productid.description;
                            const quantity        = result1[i].quantity;
                            const discountapplied = result1[i].discountapplied;
                            const price           = (result1[i].price);
                            const image           =  result1[i].productid.images[(result1[i].productid.images.length)-1];
            
                            const productData    = {
                                cartid          : cartid,
                                productid       : productid,
                                title           : title,
                                description     : description,
                                quantity        : quantity,
                                discountapplied : discountapplied,
                                price           : price,
                                image           : image
                            };
            
                        cartoutput.push(productData);
                        }
                        response.status  = 1;
                        response.message = 'Product incremented successfully';
                        response.data    = cartoutput;
                        response.count   = cartoutput.length;
                        res.send(response);
                    }
                  }
             });

        }
     })
       
});

Router.post('/DecrementCount',function(req,res) {
    const response = {
      status  : 0,
      message : 'Something went wrong in your code!'
    }
    
    const userid              = req.body.userid;
    const productid           = req.body.productid;
    const discountapplied     = req.body.discountapplied;
    var quantity              = req.body.quantity;
    const price               = req.body.price;
    const cartid              = req.body.cartid;
    
    const CartData = {
        userid: userid,
        productid: productid,
        price    : price,
        discountapplied: discountapplied,
        quantity : quantity
      }
     
        DB.FindUpdateDocument('carts',{_id:cartid,userid:userid,productid:productid}, CartData, function(err, result) {
           if(err) {
             res.send(response);
           } else {
            const options    = {};
            options.populate = 'productid';
              DB.GetDocument('carts', {userid:userid}, {}, {options}, function(err, result1) {
                  if(err) {
                      res.send(response);
                  } else {
                    if(result1.length == 0) {
                        response.status  = 2;
                        response.message = 'no data found';
                      } else {
                        const cartoutput=[];
                        for(var i=0;i<result1.length;i++){
                            const cartid          = result1[i]._id;
                            const productid       = result1[i].productid._id;
                            const title           = result1[i].productid.title;
                            const description     = result1[i].productid.description;
                            const quantity        = result1[i].quantity;
                            const discountapplied = result1[i].discountapplied;
                            const price           = (result1[i].price);
                            const image           =  result1[i].productid.images[(result1[i].productid.images.length)-1];
            
                            const productData    = {
                                cartid          : cartid,
                                productid       : productid,
                                title           : title,
                                description     : description,
                                quantity        : quantity,
                                discountapplied : discountapplied,
                                price           : price,
                                image           : image
                            };
            
                        cartoutput.push(productData);
            
                        }
                        response.status  = 1;
                        response.message = 'Product decremented successfully';
                        response.data    = cartoutput;
                        response.count   = cartoutput.length;
                        res.send(response);
                    }
                  }
             });

        }
     })
       
});

Router.post('/listCart',function(req,res) {
    const response = {
      status  : 0,
    }
      const userid     = req.body.user;
      const options    = {};
      options.populate = 'productid';
    DB.GetDocument('carts', {userid:userid}, {}, options , function(err, result) {
        if(err) {
            res.send(response);
        } else {
            const cartoutput=[];
            for(var i=0;i<result.length;i++){
                const cartid          = result[i]._id;
                const productid       = result[i].productid._id;
                const title           = result[i].productid.title;
                const description     = result[i].productid.description;
                const quantity        = result[i].quantity;
                const discountapplied = result[i].discountapplied;
                const price           = (result[i].price);
                const image           = result[i].productid.images[(result[i].productid.images.length)-1];

                const productData    = {
                    cartid          : cartid,
                    productid       : productid,
                    title           : title,
                    description     : description,
                    quantity        : quantity,
                    discountapplied : discountapplied,
                    price           : price,
                    image           : image
                };

            cartoutput.push(productData);

            }
            response.status  = 1;
            response.data    = cartoutput;
            response.count   = cartoutput.length;
            res.send(response);
        }
    });
  });

Router.post('/deleteCart',function(req,res) {
    const userid     = req.body.userid;
    const cartid     = req.body.cartid;
    const response = {
        status  : 0,
      }
      DB.DeleteDocument('carts',{_id:cartid},function(err,result){
        if(err){
            res.send(response);
        }
        else{
          const options    = {};
          options.populate = 'productid';
        DB.GetDocument('carts', {userid:userid}, {}, options , function(err, result) {
            if(err) {
                res.send(response);
            } else {
                const cartoutput=[];
                for(var i=0;i<result.length;i++){
                    const cartid          = result[i]._id;
                    const productid       = result[i].productid._id;
                    const title           = result[i].productid.title;
                    const description     = result[i].productid.description;
                    const quantity        = result[i].quantity;
                    const discountapplied = result[i].discountapplied;
                    const price           = (result[i].price);
                    const image           = result[i].productid.images[(result[i].productid.images.length)-1];
    
                    const productData    = {
                        cartid          : cartid,
                        productid       : productid,
                        title           : title,
                        description     : description,
                        quantity        : quantity,
                        discountapplied : discountapplied,
                        price           : price,
                        image           : image
                    };
    
                cartoutput.push(productData);
    
                }
                response.status  = 1;
                response.message = "Product removed from cart";
                response.data    = cartoutput;
                response.count   = cartoutput.length;
                res.send(response);
            }
        });
        }
      })
});

Router.post('/addCart',function(req,res) {
  const userid    = req.body.userid;
  const productid = req.body.productid;
  const quantity  = 1;
  const discountapplied=true;
  const price         = req.body.price;

  const response = {
    status  : 0,
  }
  const productData    = {
      userid          : userid,
      productid       : productid,
      quantity        : quantity,
      discountapplied : discountapplied,
      price           : price
  };

  DB.InsertDocument('carts',productData,function(err,result){
      if(err){
          res.send(response);
      }
      else{
        const options    = {};
        options.populate = 'productid';
        DB.GetDocument('carts',{userid:userid}, {}, options , function(err,result){
          if(err){
              res.send(response);
          }
          else{
              const cartoutput=[];
              for(var i=0;i<result.length;i++){
                  const cartid          = result[i]._id;
                  const productid       = result[i].productid._id;
                  const title           = result[i].productid.title;
                  const description     = result[i].productid.description;
                  const quantity        = result[i].quantity;
                  const discountapplied = result[i].discountapplied;
                  const price           = (result[i].price);
                  const image           = result[i].productid.images[0];

                  const productData    = {
                      cartid          : cartid,
                      productid       : productid,
                      title           : title,
                      description     : description,
                      quantity        : quantity,
                      discountapplied : discountapplied,
                      price           : price,
                      image           : image
                  };

                  cartoutput.push(productData);
              }
              response.status     = 1;
              response.message    = "Product added to cart";
              response.data       = cartoutput;
              response.count      = cartoutput.length;
              res.send(response);
          }
      })
    }
  })
});


module.exports = Router;
